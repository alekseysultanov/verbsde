//
//  StructFirebase.swift
//  prototypeGV
//
//  Created by  Artem Valeryevich on 21.01.2020.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct  StructFirebase {

    let defaultRealm: String?
    let databaseXlsx: String?
    let defaultRealmLock: String?
    let usersPhoto: String?
    let ref : DatabaseReference?
    
    init(_defaultRealm: String, _databaseXlsx: String, _defaultRealmLock: String, _usersPhoto: String) {
        self.defaultRealm = _defaultRealm
        self.databaseXlsx = _databaseXlsx
        self.defaultRealmLock = _defaultRealmLock
        self.usersPhoto = _usersPhoto
        
        self.ref = nil
    }
    
    init(snapshot: DataSnapshot) {
      
        let snapshotValue = snapshot.value as? NSDictionary
        databaseXlsx = snapshotValue?["database-xlsx"] as? String
        defaultRealm = snapshotValue?["default-realm"] as? String
        defaultRealmLock = snapshotValue?["default-realm-lock"] as? String
        usersPhoto = snapshotValue?["usersPhoto"] as? String
        ref = snapshot.ref
    }
    
}
