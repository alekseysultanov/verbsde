//
//  UserInfo.swift
//  prototypeGV
//
//  Created by admin on 05.01.20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import RealmSwift
import FirebaseDatabase

class UserInfo: Object {
    @objc dynamic var email = ""
    @objc dynamic var name = ""
    @objc dynamic var verbFixVersion = 0.0
    @objc dynamic var imageData: Data? = nil
    var themes = List<Theme>()
    let verbFix = List<Verb>()
    let verbUser = List<Verb>()
    
    convenience init(name: String, email: String, imageData: Data?, verbFixVersion: Double?) {
        self.init()
        self.name = name
        self.email = email
        self.imageData = imageData
        self.verbFixVersion = verbFixVersion ?? -1
    }
}

class UserInfoFirebase {
    let email: String
    let name: String
    let verbFixVersion: Double
    let imageData: Data?
    var verbFix: [Verb]
    var verbUser: [Verb]
    var themes: [Theme]
    
    init(snapshot: DataSnapshot) {
        let snapshotValue = snapshot.value as! [String: AnyObject]
        email = snapshotValue["email"] as? String ?? ""
        name = snapshotValue["name"] as? String ?? ""
        verbFixVersion = snapshotValue["verbFixVersion"] as? Double ?? 0.0
        imageData = DataManager.convertBase64ToImage(imageString: (snapshotValue["imageData"] as? String) ?? "")
        
        verbFix = []
        verbUser = []
        themes = []
        
        let arrayThemes = snapshotValue["themes"] as? [String: AnyObject]
        arrayThemes?.forEach({ key, value in
            if let fixed = value["fixed"] as? Bool,
                let name = value["name"] as? String {
                
                let date = value["date"] as? String
                let oldCount = value["oldCount"] as? Int
                let learnCount = value["learnCount"] as? Int
                
                themes.append(Theme(name: name,
                                    fixed: fixed,
                                    date: date ?? "",
                                    oldCount: oldCount ?? 0,
                                    learnCount: learnCount ?? 0))
            }
        })
        
        verbFix = self.parseVerbs(verb: snapshotValue["verbFix"] as? [String: AnyObject])
        verbUser = self.parseVerbs(verb: snapshotValue["verbUser"] as? [String: AnyObject])
    }
    
    func convertToUserInfoRealm() -> UserInfo {
        let userInfo = UserInfo(name: name,
                                email: email,
                                imageData: imageData,
                                verbFixVersion: verbFixVersion)
        verbFix.forEach { verb in
            userInfo.verbFix.append(verb)
        }
        
        verbUser.forEach { verb in
            userInfo.verbUser.append(verb)
        }
        
        themes.forEach { theme in
            userInfo.themes.append(theme)
        }
        
        return userInfo
    }
    
    private func parseVerbs(verb: [String: AnyObject]?) -> [Verb] {
        var verbTemp: [Verb] = []
        verb?.forEach({ key, value in
            let casus = value["casus"] as? String ?? ""
            let hilfsVerb = value["hilfsVerb"] as? String ?? ""
            let infinitiv = value["infinitiv"] as? String ?? ""
            let perfect = value["perfect"] as? String ?? ""
            let prateritum = value["prateritum"] as? String ?? ""
            let preposition = value["preposition"] as? String ?? ""
            let theme = value["theme"] as? String ?? ""
            
//            var translates: [String] = []
//            let arrayTranslate = value["translate"] as? [String: AnyObject]
//            arrayTranslate?.forEach({ key, value in
//                if let value = value as? String {
//                    translates.append(value)
//                }
//            })
            
            var translatesT: [Translates] = []
            let arraytranslates = value["translate"] as? [String: [String: AnyObject]]
            arraytranslates?.forEach({ locale, translates in
                var translatesString: [String] = []
                translates.forEach { (_, value) in
                    if let value = value as? String {
                        translatesString.append(value)
                    }
                }
                translatesT.append(Translates(locale: locale, translates: translatesString))
            })
            
            var history: [HistoryVerb] = []
            let arrayHistory = value["historyVerb"] as? [String: AnyObject]
            arrayHistory?.forEach({ ket, value in
                if let date = value["date"] as? String,
                    let answer = value["answer"] as? Bool {
                    history.append(HistoryVerb(date: date, answer: answer))
                }
            })
            
            let newVerb = Verb(infinitiv: infinitiv,
                               translate: translatesT,
                               prateritum: prateritum,
                               hilfsVerb: hilfsVerb,
                               perfect: perfect,
                               casus: casus,
                               preposition: preposition,
                               theme: theme)
            history.forEach { item in
                newVerb.historyVerb.append(item)
            }
            verbTemp.append(newVerb)
        })
        
        return verbTemp
    }
}
