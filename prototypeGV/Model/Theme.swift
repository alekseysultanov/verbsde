//
//  Theme.swift
//  prototypGV
//
//  Created by Max Polinkovsky on 02.05.2019.
//  Copyright © 2019 Max Polinkovsky. All rights reserved.
//

import RealmSwift
import FirebaseDatabase

class Theme: Object {
    private let refFirebase = Database.database().reference()
    
    @objc dynamic var name = ""
    @objc dynamic var fixed = false
    @objc dynamic var oldCount = 0
    @objc dynamic var learnCount = 0
    @objc dynamic var date = ""
    
    
    convenience init(name: String, fixed: Bool, date: String, oldCount: Int, learnCount: Int) {
        self.init()
        self.name = name
        self.fixed = fixed
        self.oldCount = oldCount
        self.learnCount = learnCount
        self.date = date
    }
    
    func getProgress() -> Float {
        var progress: Float = 0
        var rightCount = 0
        
        for verb in getVerbsInCurrentTheme() {
            if verb.getProgress() >= 1 {
                rightCount += 1
            }
        }
        
        progress = Float(rightCount) / Float(getVerbCount())
        
        return progress
    }
    
    func getRightVerb() -> [Verb] {
        var rightVeb: [Verb] = []
        
        for verb in getVerbsInCurrentTheme() {
            if verb.getProgress() >= 0.6 {
                rightVeb.append(verb)
            }
        }
        
        return rightVeb
    }
    
    func getVerbLess(dayCount: Int) -> [Verb] {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM.dd.yyyy HH:mm"
        let date = Date()
        
        var rightVeb: [Verb] = []
        
        for verb in getVerbsInCurrentTheme() {
            
            let historyVerb = verb.historyVerb
                .filter
                {
                    Calendar.current.dateComponents([.day], from: date, to: dateFormatter.date(from: $0.date) ?? Date()).day ?? 0 <= dayCount
            }
            .compactMap { $0 }
            
            guard historyVerb.count > 0 else { continue }
            
            let historyAnswerTrue = historyVerb.filter { $0.answer == true }
            let historyAnswerFalse = historyVerb.filter { $0.answer == false }
            
            var progress = Float(historyAnswerTrue.count - historyAnswerFalse.count) * 0.2
            
            if progress < 0 {
                progress = 0
            } else if progress > 1 {
                progress = 1
            }
            
            if progress >= 0.6 {
                rightVeb.append(verb)
            }
        }
        
        return rightVeb
    }
    
    func getVerbRightFor(date: String) -> [Verb] {
        var rightVeb: [Verb] = []
        
        for verb in getVerbsInCurrentTheme() {
            
            let historyVerb = verb.historyVerb.filter(NSPredicate(format: "date = %@", date))
            
            let historyAnswerTrue = historyVerb.filter(NSPredicate(format: "answer = %d", true))
            let historyAnswerFalse = historyVerb.filter(NSPredicate(format: "answer = %d", false))
            
            var progress = Float(historyAnswerTrue.count - historyAnswerFalse.count) * 0.2
            
            if progress < 0 {
                progress = 0
            } else if progress > 1 {
                progress = 1
            }
            
            if progress >= 1 {
                rightVeb.append(verb)
            }
        }
        
        return rightVeb
    }
    
    func getVerbLess(equalDay: Int) -> [Verb] {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        var rightVeb: [Verb] = []
        
        for verb in getVerbsInCurrentTheme() {
            
            var historyVerb: [HistoryVerb] = []
            
            for index in 0..<verb.historyVerb.count {
                historyVerb.append(verb.historyVerb[index])
            }
            
            historyVerb = historyVerb
                .filter
                {
                    dateFormatter.date(from: dateFormatter.string(from: Calendar.current.date(byAdding: .day, value: -equalDay, to: Date()) ?? Date())) == dateFormatter.date(from: dateFormatter.string(from: dateFormatter.date(from: $0.date) ?? Date()))
            }
            
            guard historyVerb.count > 0 else { return [] }
            
            let historyAnswerTrue = historyVerb.filter { $0.answer == true }
            let historyAnswerFalse = historyVerb.filter { $0.answer == false }
            
            var progress = Float(historyAnswerTrue.count - historyAnswerFalse.count) * 0.2
            
            if progress < 0 {
                progress = 0
            } else if progress > 1 {
                progress = 1
            }
            
            if progress >= 0.6 {
                rightVeb.append(verb)
            }
        }
        
        return rightVeb
    }
    
    func getRightVerbCount() -> Int {
        var rightCount = 0
        for verb in getVerbsInCurrentTheme() {
            if verb.getProgress() >= 1 {
                rightCount += 1
            }
        }
        return rightCount
    }
    
    func getVerbCount() -> Int {
        return getVerbsInCurrentTheme().count
    }
    
    /// Получить все слова по текущей теме
    func getVerbsInCurrentTheme() -> [Verb] {
        guard let userInfo = realm?.objects(UserInfo.self).toArray().first else { return [] }
        
        let verbsFix = userInfo.verbFix.filter(NSPredicate(format: "theme = %@", self.name))
        let verbsUser = userInfo.verbUser.filter(NSPredicate(format: "theme = %@", self.name))
        
        var allVerb: [Verb] = []
        
        for index in 0..<verbsFix.count {
            allVerb.append(verbsFix[index])
        }
        
        for index in 0..<verbsUser.count {
            allVerb.append(verbsUser[index])
        }
        
        return allVerb
    }
    
    func saveToFirebase(to fixedPath: String) {
        guard let userUid = defaults.object(forKey: "userUid") as? String else { return }
        refFirebase.child("users").child("\(userUid)").child("userInfo").child("themes").child(fixedPath).updateChildValues([
            "name": name,
            "fixed": fixed,
            "oldCount": oldCount,
            "learnCount": learnCount,
            "date": date
        ])
    }
    
//    func getScore() -> Int {
//        return learnCount - oldCount
//    }
    
    func removeFromFirebase() {
        guard let userUid = defaults.object(forKey: "userUid") as? String else { return }
        refFirebase.child("users").child("\(userUid)").child("userInfo").child("themes").child(name).removeValue()
    }
    
    func getThemePath() -> String {
        return fixed ? "verbFix" : "verbUser"
    }
}
