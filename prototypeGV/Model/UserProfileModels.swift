//
//  UserProfileModels.swift
//  prototypeGV
//
//  Created by Vladimir Ereskin on 3/7/20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

struct DaysActivityCellModel {
    let numberDay: String
    let countWord: Int
}

struct ThemesLearned {
    let theme: Theme
    var countLearnedVerbs: Int
}

struct UserProfileModel {
    let email: String
    var dayActivity: [DaysActivityCellModel]
    var themes: [ThemesLearned]
}
