//
//  Verb.swift
//  prototypGV
//
//  Created by Max Polinkovsky on 30.04.2019.
//  Copyright © 2019 Max Polinkovsky. All rights reserved.
//

import RealmSwift
import FirebaseDatabase

class HistoryVerb: Object {
    private let refFirebase = Database.database().reference()
    
    @objc dynamic var date = ""
    @objc dynamic var answer = false
    
    convenience init(date: String, answer: Bool) {
        self.init()
        self.date = date
        self.answer = answer
    }
    
    func saveToFirebaseStep(to verb: Verb, fixed: String) {
       guard let userUid = defaults.object(forKey: "userUid") as? String else { return }
        refFirebase.child("users").child("\(userUid)").child("userInfo").child(fixed).child(verb.infinitiv).child("historyVerb").child("\(UUID().uuidString)").updateChildValues([
            "date": date,
            "answer": answer
        ])
    }
}

class Translates: Object {
    @objc dynamic var locale = ""
    var translates = List<String>()
    
    convenience init(locale: String, translates: [String]) {
        self.init()
        self.locale = locale
        
        let translatesList = List<String>()
        translates.forEach { value in
            translatesList.append(value)
        }
        
        self.translates = translatesList
    }
}

class Verb: Object {
    private let refFirebase = Database.database().reference()
    
    @objc dynamic var infinitiv = ""
    @objc dynamic var prateritum = ""
    @objc dynamic var perfect = ""
    @objc dynamic var hilfsVerb = ""
    @objc dynamic var casus = ""
    @objc dynamic var preposition = ""
    @objc dynamic var theme = ""
    var historyVerb = List<HistoryVerb>()
    var translate = List<Translates>()
    
    convenience init(infinitiv: String, translate: [Translates], prateritum: String, hilfsVerb: String, perfect: String, casus: String, preposition: String, theme: String) {
        self.init()
        let translateList = List<Translates>()
        for verb in translate {
//            print(verb.locale)
//            print(verb.translates)
            translateList.append(verb)
        }
        self.infinitiv = infinitiv
        self.translate = translateList
        self.casus = casus
        self.preposition = preposition
        self.prateritum = prateritum
        self.perfect = perfect
        self.hilfsVerb = hilfsVerb
        self.theme = theme
    }
    
    func getProgress() -> Float {
        var progress: Float = 0
        
        let rightVerbArray = historyVerb.filter { $0.answer == true }
        let wrongVerbArray = historyVerb.filter { $0.answer == false }
        
        progress = Float(rightVerbArray.count - wrongVerbArray.count) * 0.2
        
        if progress < 0 {
            return 0
        } else if progress > 1 {
            return 1
        } else {
            return progress
        }
    }

    func saveToFirebaseStep(fixed: String) {
        guard let userUid = defaults.object(forKey: "userUid") as? String else { return }
        refFirebase.child("users").child("\(userUid)").child("userInfo").child(fixed).child(infinitiv).updateChildValues([
            "infinitiv": infinitiv,
            "prateritum": prateritum,
            "perfect": perfect,
            "hilfsVerb": hilfsVerb,
            "casus": casus,
            "preposition": preposition,
            "theme": theme
        ])
        
        saveTranslate(fixed: fixed)
    }
    
    func removeFromFirebaseStep(fixed: String) {
            guard let userUid = defaults.object(forKey: "userUid") as? String else { return }
        refFirebase.child("users").child("\(userUid)").child("userInfo").child(fixed).child(infinitiv).removeValue()
        }
    
    private func saveTranslate(fixed: String) {
        guard let userUid = defaults.object(forKey: "userUid") as? String else { return }

        translate.forEach { item in
            
            item.translates.forEach { translate in
                refFirebase.child("users").child("\(userUid)").child("userInfo").child(fixed).child(infinitiv).child("translate").child("\(item.locale)").updateChildValues([
                    translate: translate
                ])
            }
        }
    }
    
    func getTranslate() -> List<String> {
        let currentLocaleCode = getLocale()
        if let findLocaleTranslates = translate.first(where: { $0.locale == currentLocaleCode }) {
            return findLocaleTranslates.translates
        } else {
            if let findLocaleTranslates = translate.first(where: { $0.locale == "ru" }) {
                return findLocaleTranslates.translates
            }
            return List<String>()
        }
    }
    
    func getLocale() -> String {
        let locale = defaults.object(forKey: "Locale") as? String
        return locale ?? "ru"
    }
    
    func appendTranslate(value: String) {
        let translates = getTranslate()
        translates.append(value)
        
        if let index = translate.firstIndex(where: { $0.locale == getLocale() }) {
            translate.replace(index: index, object: Translates(locale: getLocale(), translates: Array(translates)))
        }
    }
    
    func appendTranslateTo(locale: String, value: String) {
        let translates = getTranslate()
        translates.append(value)
        
        if let index = translate.firstIndex(where: { $0.locale == locale }) {
            translate.replace(index: index, object: Translates(locale: locale, translates: Array(translates)))
        }
    }
}
