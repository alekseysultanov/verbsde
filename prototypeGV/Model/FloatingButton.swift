//
//  FloatingButton.swift
//  prototypeGV
//
//  Created by admin on 01.11.19.
//  Copyright © 2019 Max Polinkovsky. All rights reserved.
//

import UIKit

class FloatingButton: UIButton {
    
    var floatingButton = UIButton(type: .custom)
    
    func createFloatingButton() {
        floatingButton.translatesAutoresizingMaskIntoConstraints = false
        floatingButton.backgroundColor = .none
        // Make sure you replace the name of the image:
        floatingButton.setImage(UIImage(named:"btnAction"), for: .normal)
        // We're manipulating the UI, must be on the main thread:
        DispatchQueue.main.async {
            if let keyWindow = UIApplication.shared.keyWindow {
                keyWindow.addSubview(self.floatingButton)
                NSLayoutConstraint.activate([
                    keyWindow.trailingAnchor.constraint(equalTo: self.floatingButton.trailingAnchor, constant: 40),
                    keyWindow.bottomAnchor.constraint(equalTo: self.floatingButton.bottomAnchor, constant: 140),
                    self.floatingButton.widthAnchor.constraint(equalToConstant: 56),
                    self.floatingButton.heightAnchor.constraint(equalToConstant: 56)])
            }
            // Make the button round:
            self.floatingButton.layer.cornerRadius = 28
            // Add a black shadow:
            self.floatingButton.layer.shadowColor = UIColor.black.cgColor
            self.floatingButton.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
            self.floatingButton.layer.masksToBounds = false
            self.floatingButton.layer.shadowRadius = 2.0
            self.floatingButton.layer.shadowOpacity = 0.5
            // Add a pulsing animation to draw attention to button:
//            let scaleAnimation: CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
//            scaleAnimation.duration = 0.4
//            scaleAnimation.repeatCount = .greatestFiniteMagnitude
//            scaleAnimation.autoreverses = true
//            scaleAnimation.fromValue = 1.0;
//            scaleAnimation.toValue = 1.05;
//            self.floatingButton.layer.add(scaleAnimation, forKey: "scale")
        }
    }
}
