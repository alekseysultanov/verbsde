//
//  ProgressColor.swift
//  prototypeGV
//
//  Created by Max Polinkovsky on 26.05.2019.
//  Copyright © 2019 Max Polinkovsky. All rights reserved.
//

import UIKit

class ProgressColor {
    private init() {}
    enum PalletteColor: Int { case one, two }
    
    static func by(progress: Float, withPalette palette: PalletteColor = .one) -> UIColor {
        switch progress {
        case   0..<0.3:
            let color = vermillion
            switch palette {
            case .one: return color
            case .two: return color.add(UIColor.white.withAlphaComponent(0.5))
            }
        case 0.3..<0.7:
            let color = yellowOrange
            switch palette {
            case .one: return color
            case .two: return color.add(UIColor.white.withAlphaComponent(0.5))
            }
        case 0.7..<1.0:
            let color = #colorLiteral(red: 0.5009082556, green: 0.8771516681, blue: 0.255045563, alpha: 1)
            switch palette {
            case .one: return color
            case .two: return color.add(UIColor.white.withAlphaComponent(0.5))
            }
        case 1.0:
            let color = darkLimeGreen
            switch palette {
            case .one: return color
            case .two: return color.add(UIColor.white.withAlphaComponent(0.5))
            }
        default:
            let color = veryLightPinkTwo
            switch palette {
            case .one: return color
            case .two: return color.add(UIColor.white.withAlphaComponent(0.5))
            }
        }
    }
}
