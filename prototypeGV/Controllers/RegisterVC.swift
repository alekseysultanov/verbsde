//
//  RegisterVC.swift
//  prototypeGV
//
//  Created by admin on 03.01.20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit
import FirebaseAuth

class RegisterVC: UIViewController {
    
    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "back"), for: .normal)
        button.addTarget(self, action: #selector(cancelClick), for: .touchUpInside)
        return button
    }()
    
    lazy var registerLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 16)
        label.textColor = black
        label.textAlignment = .center
        label.text = "Register"
        return label
    }()
    
    lazy var loginTextField: UITextField = {
        let textField = UITextField()
        textField.delegate = self
        textField.setLeftView(image: UIImage.init(named: "icnEmail")!)
        textField.textColor = black
        textField.placeholder = "Email address"
        textField.background = UIImage(contentsOfFile: "")
        textField.backgroundColor = gray
        textField.textAlignment = .left
        textField.borderStyle = .none
        textField.returnKeyType = .done
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        textField.smartInsertDeleteType = .no
        textField.spellCheckingType = .no
        textField.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        return textField
    }()
    
    lazy var passTextField: UITextField = {
        let textField = UITextField()
        textField.delegate = self
        textField.setLeftView(image: UIImage.init(named: "icnPass")!)
        textField.isSecureTextEntry = true
        textField.textColor = black
        textField.placeholder = "Password"
        textField.background = UIImage(contentsOfFile: "")
        textField.backgroundColor = gray
        textField.textAlignment = .left
        textField.borderStyle = .none
        textField.returnKeyType = .done
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        textField.smartInsertDeleteType = .no
        textField.spellCheckingType = .no
        textField.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        return textField
    }()
    
    lazy var registerButton: UIButton = {
        let button = UIButton()
        button.setTitle("Register", for: .normal)
        button.layer.cornerRadius = 12
        button.backgroundColor = blue
        button.setTitleColor(white, for: .normal)
        button.titleLabel?.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        button.addTarget(self, action: #selector(register), for: .touchUpInside)
        return button
    }()
    
    lazy var idLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 16)
        label.textColor = black
        label.text = "Already registered?"
        return label
    }()
    
    lazy var loginButton: UIButton = {
        let button = UIButton()
        button.setTitle("Login", for: .normal)
        button.setTitleColor(blue, for: .normal)
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont(name: "TeXGyreAdventor-Bold", size: 16)
        button.addTarget(self, action: #selector(login), for: .touchUpInside)
        return button
    }()
    
    lazy var stackTextField: UIStackView = {
        let stack = UIStackView()
        stack.spacing = 10
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.alignment = .fill
        stack.contentMode = .scaleToFill
        return stack
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        navigationController?.setNavigationBarHidden(false, animated: animated)
        setup()
    }
    
    func createStack() {
        stackTextField.addArrangedSubview(loginTextField)
        stackTextField.addArrangedSubview(passTextField)
    }
    
    @objc func login() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func register() {
        guard loginTextField.text != "" else {return}
        guard passTextField.text != "" else {return}
        
        Auth.auth().createUser(withEmail: loginTextField.text!, password: passTextField.text!) { (result, error) in
//            print(error!.localizedDescription)
            if error == nil {
                guard let dvc = ThemeTVC.storyboardInstance() else { return }
                self.navigationController?.pushViewController(dvc, animated: true)
            } else {
                
                print(error?.localizedDescription)
            }
        }
    }
    
    @objc func cancelClick() {
        dismiss(animated: true, completion: nil)
    }
}

extension RegisterVC: UserPresentable {
    func designView() {
        view.backgroundColor = .white
        loginTextField.layer.cornerRadius = 12
        passTextField.layer.cornerRadius = 12
    }
    
    func addSubView() {
        view.addSubview(registerLabel)
        createStack()
        view.addSubview(cancelButton)
        view.addSubview(stackTextField)
        view.addSubview(loginButton)
        view.addSubview(idLabel)
        view.addSubview(registerButton)
    }
    
    func makeConstrains() {
        registerLabel.translatesAutoresizingMaskIntoConstraints = false
        stackTextField.translatesAutoresizingMaskIntoConstraints = false
        idLabel.translatesAutoresizingMaskIntoConstraints = false
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        registerButton.translatesAutoresizingMaskIntoConstraints = false
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        
        cancelButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 47).isActive = true
        cancelButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        cancelButton.heightAnchor.constraint(equalToConstant: 32).isActive = true
        cancelButton.widthAnchor.constraint(equalToConstant: 32).isActive = true
        
        registerLabel.centerYAnchor.constraint(equalTo: cancelButton.centerYAnchor).isActive = true
        registerLabel.widthAnchor.constraint(equalToConstant: 220).isActive = true
        registerLabel.heightAnchor.constraint(equalToConstant: 22).isActive = true
        registerLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        stackTextField.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        stackTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8).isActive = true
        stackTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
        stackTextField.heightAnchor.constraint(equalToConstant: 122).isActive = true
        
        registerButton.topAnchor.constraint(equalTo: stackTextField.bottomAnchor, constant: 30).isActive = true
        registerButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8).isActive = true
        registerButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
        registerButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        registerButton.heightAnchor.constraint(equalToConstant: 56).isActive = true
        
        loginButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -70).isActive = true
        loginButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loginButton.widthAnchor.constraint(equalToConstant: 62).isActive = true
        loginButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        idLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        idLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
        idLabel.bottomAnchor.constraint(equalTo: loginButton.topAnchor, constant: -10).isActive = true
    }
}

extension RegisterVC: UITextFieldDelegate {
    //hidden keyboard with Done tapped
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
