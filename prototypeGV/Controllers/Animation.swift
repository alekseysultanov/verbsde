//
//  Animation.swift
//  prototypeGV
//
//  Created by admin on 04.03.20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import Foundation

struct Animation {
    
    static let duration02 = 0.2
    static let duration03 = 0.3
    static let duration04 = 0.4
    static let duration07 = 0.7
    static let duration08 = 0.8
    static let duration12 = 1.2
    static let delay03  = 0.3
    static let delay04  = 0.4
    static let delay05  = 0.5
}
