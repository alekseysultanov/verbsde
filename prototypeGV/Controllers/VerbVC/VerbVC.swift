//
//  VerbTVC.swift
//  prototypGV
//
//  Created by Max Polinkovsky on 30.04.2019.
//  Copyright © 2019 Max Polinkovsky. All rights reserved.
//

import UIKit
import RealmSwift
import ExpandingMenu

class VerbVC: UIViewController {
   
    var selectedTheme: Theme!
    var currentVerb: [Verb] = []
    var filtredVerbs: [Verb] = []
    var textSearch = ""
    var userInfo = UserInfo()
    var tableBottomTrueConstrains: NSLayoutConstraint!
    var tableBottomFalseConstrains: NSLayoutConstraint!
    let menuButton = ExpandingMenuButton(frame: CGRect(origin: CGPoint.zero,
                                                               size: CGSize(width: 56.0, height: 56.0)),
                                                 image: UIImage(named: "btnAction")!, rotatedImage: UIImage(named: "closeMenu")!)
    
    var lastContentOffset: CGFloat = 0
    var searchController = UISearchController(searchResultsController: nil)
    private var searchBarIsEmpty: Bool {
        guard let text = searchController.searchBar.text else { return false }
        return text.isEmpty
    }
    
    var isFilting: Bool {
        return searchController.isActive && !searchBarIsEmpty
    }
    
    lazy var noVerbs: UILabel = {
        let label = UILabel()
        label.text = thereAreNotVerbs
        label.font = UIFont(name:"TeXGyreAdventor-Bold", size: 20.0)
        label.textAlignment = .center
        label.numberOfLines = 2
        label.isHidden = true
        return label
    }()
    
    lazy var addOneButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.contentHorizontalAlignment = .right
        button.semanticContentAttribute = .forceLeftToRight
        button.contentMode = .right
        button.backgroundColor = .clear
        button.setTitle(addOne, for: .normal)
        button.setTitleColor(azure, for: .normal)
        button.setImage(UIImage(named: "add"), for: .normal)
        button.setImage(UIImage(named: "add"), for: .highlighted)
        button.imageEdgeInsets.right = 10
        button.titleLabel?.font = UIFont(name: "TeXGyreAdventor-Bold", size: 16)
        button.alpha = 0
        button.addTarget(self, action: #selector(addNewVerb(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var alertButton: UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 28
        button.setImage(UIImage(named: "btnAction"), for: .normal)
        button.layer.shadowColor = black.cgColor
        button.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        button.layer.masksToBounds = false
        button.layer.shadowRadius = 2.0
        button.layer.shadowOpacity = 0.5
        button.addTarget(self, action: #selector(setAlert), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    lazy var tableView: UITableView = {
        let table = UITableView()
        table.separatorStyle = .none
        table.delegate = self
        table.dataSource = self
        table.backgroundColor = veryLightPinkTwo15
        table.register(VerbCell.self, forCellReuseIdentifier: cellIdentifier)
        return table
    }()
    
    lazy var segmentedControl: UISegmentedControl = {
        let control = UISegmentedControl()
        control.translatesAutoresizingMaskIntoConstraints = false
        control.insertSegment(withTitle: infinitiv, at: 0, animated: false)
        control.insertSegment(withTitle: translate, at: 1, animated: false)
        control.insertSegment(withTitle: progress, at: 2, animated: false)
        control.selectedSegmentIndex = 0
        control.addTarget(self, action: #selector(sortSelection), for: .valueChanged)
        
        return control
    }()
    
    lazy var cancelButton: UIBarButtonItem = {
        let button = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(cancel))
        return button
    }()
    
    /// Кнопка добавления нового слова
    lazy var addNewVerbButton: UIBarButtonItem = {
        let button = UIBarButtonItem(image: UIImage(named: "add"),
                                     style: .plain,
                                     target: self,
                                     action: #selector(addNewVerb(_:)))
        button.tintColor = .clear
        return button
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.addSubview(menuButton)
        if !selectedTheme.fixed && currentVerb.count != 0 {
            addNewVerbButton.isEnabled = true
            addOneButton.alpha = 0
            addNewVerbButton.tintColor = azure
        } else {
            addNewVerbButton.isEnabled = false
            addOneButton.alpha = 1
        }
        
        DataManager.getUserInfo { [ weak self ] info in
            guard let user = info else { return }
            self?.userInfo = user
            convertVerb()
            choiseButtonTap()
            sortSelection()
        }
        loadDarkTheme()
        isOtherIphone()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.menuButton.isHidden = false
            self.menuButton.removeFromSuperview()
        }
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        // old
        setup()
        convertVerb()
        choiseButtonTap()
        configureTestChoice()
        initSerachBar()
        tableView.tableFooterView = UIView() //no separators of rows upper full rows
        // new
        makeNavigationBar()
    }
    
    private func makeNavigationBar() {
        navigationItem.rightBarButtonItem = addNewVerbButton
        navigationItem.leftBarButtonItem = cancelButton
        title = selectedTheme.name
    }
    
    private func convertVerb() {
        currentVerb = selectedTheme.getVerbsInCurrentTheme()
        tableView.reloadData()
    }
   
    private func isOtherIphone() {
           var heightScreen: CGFloat = 0
           var widthScreen: CGFloat = 0
            let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
           let bounds = UIScreen.main.bounds
           heightScreen = bounds.size.height //get the height of screen
           widthScreen = bounds.size.width //get the width of screen
           
           if heightScreen == 568.0 && widthScreen == 320.0 {
            NSLayoutConstraint.activate([
                segmentedControl.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
                segmentedControl.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)
            ])
            if #available(iOS 13.0, *) {
                return segmentedControl.setTitleTextAttributes(userCurretChoiceIs ? [NSAttributedString.Key.foregroundColor: black] : [NSAttributedString.Key.foregroundColor: black85], for: .selected)
            } else {
                return segmentedControl.setTitleTextAttributes(userCurretChoiceIs ? [NSAttributedString.Key.foregroundColor: black] : [NSAttributedString.Key.foregroundColor: white], for: .selected)
                
            }
           } else {
            segmentedControl.setTitleTextAttributes(userCurretChoiceIs ? [NSAttributedString.Key.foregroundColor: black] : [NSAttributedString.Key.foregroundColor: black85], for: .selected)
            NSLayoutConstraint.activate([
                segmentedControl.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 38),
                segmentedControl.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -38)
            ])
           }
       }
   
    func loadDarkTheme(){
        let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
        let titleTextAttributesSelected = userCurretChoiceIs ? [NSAttributedString.Key.foregroundColor: black] : [NSAttributedString.Key.foregroundColor: black]
        let titleTextAttributesNormal = userCurretChoiceIs ? [NSAttributedString.Key.foregroundColor: white] : [NSAttributedString.Key.foregroundColor: black]

        searchController.searchBar.barStyle = userCurretChoiceIs ? .black : .default
        view.backgroundColor = userCurretChoiceIs ? darkGrey : white
        segmentedControl.backgroundColor = userCurretChoiceIs ? black : veryLightPinkTwo15
        segmentedControl.tintColor = userCurretChoiceIs ? white : black85
        segmentedControl.setTitleTextAttributes(titleTextAttributesSelected, for: .selected)
        segmentedControl.setTitleTextAttributes(titleTextAttributesNormal, for: .normal)
        noVerbs.textColor = userCurretChoiceIs ? white : black85
        cancelButton.tintColor = userCurretChoiceIs ? white : black
        tableView.backgroundColor = userCurretChoiceIs ? black : veryLightPinkTwo15
    }
    
    @objc func sortSelection() {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            if isFilting {
                filtredVerbs = filtredVerbs.sorted(by: { $0.infinitiv < $1.infinitiv })
            } else {
                currentVerb = currentVerb.sorted(by: { $0.infinitiv < $1.infinitiv })
            }
        case 1:
            if isFilting {
                filtredVerbs = filtredVerbs.sorted(by: { ($0.getTranslate().first ?? "") < ($1.getTranslate().first ?? "") })
            } else {
                currentVerb = currentVerb.sorted(by: { ($0.getTranslate().first ?? "") < ($1.getTranslate().first ?? "") })
            }
        case 2:
            if isFilting {
                filtredVerbs = filtredVerbs.sorted(by: { ($0.getProgress()) > ($1.getProgress()) })
            } else {
                currentVerb = currentVerb.sorted(by: { $0.getProgress() > $1.getProgress() })
            }
        default:
            break
        }
        tableView.reloadData()
    }
    
    @objc func cancel() {
        navigationController?.popViewController(animated: true)
    }
}
