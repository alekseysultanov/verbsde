//
//  ExtensionsVerbVC.swift
//  prototypGV
//
//  Created by Max Polinkovsky on 23.05.2019.
//  Copyright © 2019 Max Polinkovsky. All rights reserved.
//

import UIKit
protocol CancelKeyTestDelegate {
    func cancelKeyTest()
}

extension VerbVC: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContent(searchController.searchBar.text!.lowercased())
    }
    
    func filterContent(_ searchText: String) {
        filtredVerbs = currentVerb.filter { $0.infinitiv.contains(searchText) || ($0.getTranslate().reduce(0) { (result, verb) -> Int in
            result + (verb.contains(searchText) ? 1 : 0)
        } >= 1) }
        tableView.reloadData()
    }
}
