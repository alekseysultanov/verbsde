//
//  VerbCell.swift
//  prototypGV
//
//  Created by Max Polinkovsky on 30.04.2019.
//  Copyright © 2019 Max Polinkovsky. All rights reserved.
//

import UIKit

class VerbCell: UITableViewCell {
    
    lazy var infinitiv: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 20)
        return label
    }()
    
    lazy var translate: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.alpha = 0.5
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 14)
        return label
    }()
    
    lazy var progress: UIProgressView = {
        let progress = UIProgressView()
        progress.translatesAutoresizingMaskIntoConstraints = false
        progress.progress = 0.0
        progress.backgroundColor = veryLightPinkTwo
        progress.contentMode = .scaleToFill
        return progress
    }()
    
     lazy var learned: UIImageView = {
        let image = UIImageView(image: UIImage(named: "done"))
        image.translatesAutoresizingMaskIntoConstraints = false
        image.alpha = 0
        return image
    }()
    
    let separatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .gray
        view.alpha = 0.2
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
          super.init(style: .default, reuseIdentifier: cellIdentifier)
          setup()
      }
      
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    func loadDarkTheme() {
        let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
        infinitiv.textColor = userCurretChoiceIs ? white : black85
        translate.textColor = userCurretChoiceIs ? white : black85
        separatorView.backgroundColor = userCurretChoiceIs ? black : .gray
        separatorView.alpha = userCurretChoiceIs ? 1.0 : 0.2
    }
}

extension VerbCell: UserPresentable {
    
    func addSubView() {
        contentView.addSubview(infinitiv)
        contentView.addSubview(translate)
        contentView.addSubview(learned)
        contentView.addSubview(progress)
        contentView.addSubview(separatorView)
    }
    
    func makeConstraints() {
        NSLayoutConstraint.activate([
            infinitiv.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            infinitiv.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 27),
            infinitiv.heightAnchor.constraint(equalToConstant: 31),
            
            learned.leadingAnchor.constraint(equalTo: infinitiv.trailingAnchor, constant: 10),
            learned.heightAnchor.constraint(equalToConstant: 22),
            learned.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15),
            
            translate.topAnchor.constraint(equalTo: infinitiv.bottomAnchor, constant: 2),
            translate.heightAnchor.constraint(equalToConstant: 21),
            translate.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 27),
            
            progress.topAnchor.constraint(equalTo: translate.bottomAnchor, constant: 15),
            progress.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 27),
            progress.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -27),
            progress.heightAnchor.constraint(equalToConstant: 6),
            progress.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -15),

            separatorView.heightAnchor.constraint(equalToConstant: 1),
            separatorView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            separatorView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            separatorView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
}
