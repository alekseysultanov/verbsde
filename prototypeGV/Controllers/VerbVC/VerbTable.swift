//
//  Table.swift
//  prototypeGV
//
//  Created by admin on 28.06.20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

extension VerbVC: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFilting {
            return filtredVerbs.count
        }
        return currentVerb.isEmpty ? 0 : currentVerb.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! VerbCell
        var verb = Verb()
        
        if isFilting {
            verb = filtredVerbs[indexPath.row]
        } else {
            verb = currentVerb[indexPath.row]
        }
        cell.selectionStyle = .none
        let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
        let progress = verb.getProgress()
        
        cell.infinitiv.text = verb.infinitiv
        cell.translate.text = verb.getTranslate().first
        cell.progress.setProgressBar(progress: cell.progress)
        cell.progress.tintColor = ProgressColor.by(progress: progress)
        cell.progress.setProgress(0, animated: false)
        cell.loadDarkTheme()
        cell.backgroundColor = userCurretChoiceIs ? darkGrey : white
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            UIView.animate(withDuration: Animation.duration04) {
                cell.progress.setProgress(progress, animated: true)
            }
            if progress == 1 {
                UIView.animate(withDuration: Animation.duration04) {
                    cell.learned.alpha = 1
                }
            } else {
                cell.learned.alpha = 0
            }
        }
        return cell
    }
    
    // MARK: - Table view Delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dvc = DetailsVC()
        guard let indexPath = tableView.indexPathForSelectedRow else { return }
        var verb = Verb()
        if isFilting {
            verb = filtredVerbs[indexPath.row]
        } else {
            verb = self.currentVerb[indexPath.row]
        }
        dvc.currentVerb = verb
        navigationController?.pushViewController(dvc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        guard !selectedTheme.fixed else { return nil }
        
        let verb = self.isFilting ? filtredVerbs[indexPath.row] : currentVerb[indexPath.row]
        
        let verbFixArray = Array(userInfo.verbFix)
        let verbSearch = verbFixArray.first { $0.infinitiv == verb.infinitiv }
        
        guard verbSearch == nil else { return nil }
       
        let editAction = UIContextualAction(style: .normal, title:  "Edit".localized(), handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            tableView.setEditing(false, animated: true)
            
            var verb = Verb()
            if self.isFilting {
                verb = self.filtredVerbs[indexPath.row]
            } else {
                verb = self.currentVerb[indexPath.row]
            }
            let dvc = NewVerbVC()
            dvc.selectedVerb = verb
            dvc.currentTheme = self.selectedTheme
            self.navigationController?.pushViewController(dvc, animated: true)
        })
        editAction.backgroundColor = brightOrange
        
        let deleteAction = UIContextualAction(style: .normal, title:  "Delete".localized(), handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            DataManager.delete(verb, fixed: self.selectedTheme.getThemePath())
            if self.isFilting {
                self.filtredVerbs.remove(at: indexPath.row)
            } else {
                self.currentVerb.remove(at: indexPath.row)
            }
            tableView.deleteRows(at: [indexPath], with: .automatic)
            self.choiseButtonTap()
        })
        deleteAction.backgroundColor = vermillion
        
        return UISwipeActionsConfiguration(actions: [editAction, deleteAction])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
