//
//  TestChoise.swift
//  prototypeGV
//
//  Created by admin on 28.06.20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

extension VerbVC {
    
    func choiseButtonTap() {
        switch currentVerb.count {
        case 0:
            menuButton.isHidden = true
            alertButton.isHidden = true
            noVerbs.isHidden = false
            segmentedControl.isHidden = true
            tableBottomTrueConstrains.isActive = true
            tableBottomFalseConstrains.isActive = false
        case 1..<4:
            menuButton.isHidden = true
            alertButton.isHidden = false
            noVerbs.isHidden = true
            segmentedControl.isHidden = true
            tableBottomTrueConstrains.isActive = true
            tableBottomFalseConstrains.isActive = false
        default:
            menuButton.isHidden = false
            alertButton.isHidden = true
            noVerbs.isHidden = true
            segmentedControl.isHidden = false
            tableBottomTrueConstrains.isActive = false
            tableBottomFalseConstrains.isActive = true
        }
        
        if !selectedTheme.fixed && currentVerb.count > 0 {
            addNewVerbButton.isEnabled = true
            addNewVerbButton.tintColor = azure
            addOneButton.backgroundColor = .clear
            addOneButton.alpha = 0
            addOneButton.isEnabled = false
        } else if !selectedTheme.fixed && currentVerb.count == 0 {
            addNewVerbButton.isEnabled = false
            addNewVerbButton.tintColor = .clear
            addOneButton.alpha = 1
            addOneButton.isEnabled = true
        } else {
            addNewVerbButton.isEnabled = false
            addNewVerbButton.tintColor = .clear
            addOneButton.isEnabled = false
            addOneButton.alpha = 0
        }
    }
}
