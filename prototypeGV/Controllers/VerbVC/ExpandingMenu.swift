//
//  ExpandingMenu.swift
//  prototypeGV
//
//  Created by admin on 28.06.20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit
import ExpandingMenu

extension VerbVC {
    
    func configureTestChoice() {
        let menuButtonSize: CGSize = CGSize(width: 56.0, height: 56.0)
        self.menuButton.center = CGPoint(x: self.view.bounds.width - 70.0, y: self.view.bounds.height - 170.0)
        // Make the button round:
        self.menuButton.layer.cornerRadius = 28
        // Add a black shadow:
        self.menuButton.layer.shadowColor = black.cgColor
        self.menuButton.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        self.menuButton.layer.masksToBounds = false
        self.menuButton.layer.shadowRadius = 2.0
        self.menuButton.layer.shadowOpacity = 0.5
        self.menuButton.playSound = false
        
        let optionTest = ExpandingMenuItem(size: menuButtonSize, title: option, image: UIImage(named: "icnOptionColor")!, highlightedImage: UIImage(named: "icnOptionColor")!, backgroundImage: UIImage(named: "chooser-moment-button"), backgroundHighlightedImage: UIImage(named: "chooser-moment-button-highlighted")) { () -> Void in
            let testVC = TestVC(verbs: self.currentVerb, type: .option, theme: self.selectedTheme)
            self.navigationController?.pushViewController(testVC, animated: true)
        }
        
        let translateTest = ExpandingMenuItem(size: menuButtonSize, title: translate, image: UIImage(named: "icnTranslateColor")!, highlightedImage: UIImage(named: "icnTranslateColor")!, backgroundImage: UIImage(named: "chooser-moment-button"), backgroundHighlightedImage: UIImage(named: "chooser-moment-button-highlighted")) { () -> Void in
            let testVC = TestVC(verbs: self.currentVerb, type: .translate, theme: self.selectedTheme)
            self.navigationController?.pushViewController(testVC, animated: true)
        }
        
        let keysTest = ExpandingMenuItem(size: menuButtonSize, title: keys, image: UIImage(named: "icnKeys")!, highlightedImage: UIImage(named: "icnKeys")!, backgroundImage: UIImage(named: "chooser-moment-button"), backgroundHighlightedImage: UIImage(named: "chooser-moment-button-highlighted")) { () -> Void in
            let testVC = TestVC(verbs: self.currentVerb, type: .keys, theme: self.selectedTheme)
            self.navigationController?.pushViewController(testVC, animated: true)
        }
        
        let brainstormTest = ExpandingMenuItem(size: menuButtonSize, title: brainstorm, image: UIImage(named: "icnBrainColor")!, highlightedImage: UIImage(named: "icnBrainColor")!, backgroundImage: UIImage(named: "chooser-moment-button"), backgroundHighlightedImage: UIImage(named: "chooser-moment-button-highlighted")) { () -> Void in
            let testVC = TestVC(verbs: self.currentVerb, type: .brainstorm, theme: self.selectedTheme)
            self.navigationController?.pushViewController(testVC, animated: true)
        }
        
        optionTest.titleColor = white
        translateTest.titleColor = white
        keysTest.titleColor = white
        menuButton.foldingAnimations = .menuButtonRotate
        menuButton.bottomViewAlpha = 0.2
        menuButton.addMenuItems([optionTest, translateTest, keysTest, brainstormTest])
    }
    
    func initSerachBar() {
        searchController.searchResultsUpdater = self as UISearchResultsUpdating
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = search
        searchController.searchBar.autocapitalizationType = .none
        navigationItem.searchController = searchController
    }
    
    @objc func setAlert() {
        let alert = UIAlertController(title: nil, message: lessThen4Verbs, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .destructive, handler: nil)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func addNewVerb(_ sender: UIButton) {
        if !selectedTheme.fixed {
            let dvc = NewVerbVC()
            dvc.currentTheme = self.selectedTheme
            dvc.delegate = self
            navigationController?.pushViewController(dvc, animated: true)
        }
    }
}
