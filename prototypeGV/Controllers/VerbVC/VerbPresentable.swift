//
//  Presentable.swift
//  prototypeGV
//
//  Created by admin on 28.06.20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

    extension VerbVC: UserPresentable {
        
        func addSubView() {
            view.addSubview(tableView)
            view.addSubview(noVerbs)
            view.addSubview(alertButton)
            view.addSubview(addOneButton)
            view.addSubview(segmentedControl)
        }
        
        func makeConstraints() {
            noVerbs.translatesAutoresizingMaskIntoConstraints = false
            alertButton.translatesAutoresizingMaskIntoConstraints = false
            addOneButton.translatesAutoresizingMaskIntoConstraints = false
            tableView.translatesAutoresizingMaskIntoConstraints = false
            segmentedControl.translatesAutoresizingMaskIntoConstraints = false
            
            NSLayoutConstraint.activate([
                
                noVerbs.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                noVerbs.centerYAnchor.constraint(equalTo: view.centerYAnchor),
                noVerbs.heightAnchor.constraint(equalToConstant: 62),
                noVerbs.widthAnchor.constraint(equalToConstant: 188),
                
                addOneButton.topAnchor.constraint(equalTo: noVerbs.bottomAnchor, constant: 30),
                addOneButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                addOneButton.heightAnchor.constraint(equalToConstant: 32),
                
                alertButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -33),
                alertButton.bottomAnchor.constraint(equalTo: segmentedControl.topAnchor, constant:  -54),
                alertButton.heightAnchor.constraint(equalToConstant: 56),
                alertButton.widthAnchor.constraint(equalToConstant: 56),
                
                segmentedControl.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50),
                segmentedControl.centerXAnchor.constraint(equalTo: view.centerXAnchor),

                tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
                tableView.rightAnchor.constraint(equalTo: view.rightAnchor),
                
            ])
            tableBottomFalseConstrains =  tableView.bottomAnchor.constraint(equalTo: segmentedControl.topAnchor, constant: -20)
            tableBottomTrueConstrains = tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        }
    }

