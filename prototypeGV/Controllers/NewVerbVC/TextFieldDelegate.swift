//
//  ExtensionsNewVerbVC.swift
//  prototypeGV
//
//  Created by Max Polinkovsky on 28.05.2019.
//  Copyright © 2019 Max Polinkovsky. All rights reserved.
//

import UIKit
import PopMenu

extension NewVerbVC: UITextFieldDelegate {
    //hidden keyboard with Done tapped
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func textFieldChanged() {
        if infinitivTextField.text!.isEmpty == true || perfectTextField.text!.isEmpty == true || preteriteTextField.text!.isEmpty == true {
            saveButton.isEnabled = false
        } else {
            saveButton.isEnabled = true
        }
        return
    }
}

extension NewVerbVC {
    
    @objc func setCasus(_ sender: UIButton) {
        let actions = [
            PopMenuDefaultAction(title: Casus.empty.description, didSelect: { (PopMenuAction) in
                self.casusButton.setTitle(Casus.empty.description, for: .normal)
            }),
            PopMenuDefaultAction(title: Casus.akkusativ.description, didSelect: { (PopMenuAction) in
                self.casusButton.setTitle(Casus.akkusativ.description, for: .normal)
            }),
            PopMenuDefaultAction(title: Casus.dativ.description, didSelect: { (PopMenuAction) in
                self.casusButton.setTitle(Casus.dativ.description, for: .normal)
            })
        ]
        
        let menu = PopMenuViewController(sourceView: sender, actions: actions)
        menu.appearance.popMenuBackgroundStyle = .none()
        menu.appearance.popMenuColor.backgroundColor = .solid(fill: white)
        menu.appearance.popMenuFont = UIFont(name: "TeXGyreAdventor-Bold", size: 14)!
        menu.appearance.popMenuColor.actionColor = .tint(black85)
        menu.appearance.popMenuCornerRadius = 14
        menu.appearance.popMenuItemSeparator = .fill(black10, height: 1)
        present(menu, animated: true, completion: nil)
    }
    
    @objc func hilfsVerbAction(_ sender: UIButton) {
        let actions = [
            PopMenuDefaultAction(title:HilfsVerb.hat.description, didSelect: { (PopMenuAction) in
                self.hilfsVerbButton.setTitle(HilfsVerb.hat.description, for: .normal)
            }),
            PopMenuDefaultAction(title: HilfsVerb.ist.description, didSelect: { (PopMenuAction) in
                self.hilfsVerbButton.setTitle(HilfsVerb.ist.description, for: .normal)
            })
        ]
        
        let menu = PopMenuViewController(sourceView: sender, actions: actions)
        menu.appearance.popMenuBackgroundStyle = .none()
        menu.appearance.popMenuColor.backgroundColor = .solid(fill: white)
        menu.appearance.popMenuFont = UIFont(name: "TeXGyreAdventor-Bold", size: 14)!
        menu.appearance.popMenuColor.actionColor = .tint(black85)
        menu.appearance.popMenuCornerRadius = 14
        menu.appearance.popMenuItemSeparator = .fill(black10, height: 1)
        present(menu, animated: true, completion: nil)
    }
    
    @objc func setPreposition(_ sender: UIButton) {
        let actions = [
            PopMenuDefaultAction(title: Preposition.empty.description, didSelect: { (PopMenuAction) in
                self.prepositionButton.setTitle(Preposition.empty.description, for: .normal)
            }),
            PopMenuDefaultAction(title: Preposition.als.description, didSelect: { (PopMenuAction) in
                self.prepositionButton.setTitle(Preposition.an.description, for: .normal)
            }),
            PopMenuDefaultAction(title: Preposition.an.description, didSelect: { (PopMenuAction) in
                self.prepositionButton.setTitle(Preposition.an.description, for: .normal)
            }),
            PopMenuDefaultAction(title: Preposition.auf.description, didSelect: { (PopMenuAction) in
                self.prepositionButton.setTitle(Preposition.auf.description, for: .normal)
            }),
            PopMenuDefaultAction(title: Preposition.aus.description, didSelect: { (PopMenuAction) in
                self.prepositionButton.setTitle(Preposition.aus.description, for: .normal)
            }),
            PopMenuDefaultAction(title: Preposition.bei.description, didSelect: { (PopMenuAction) in
                self.prepositionButton.setTitle(Preposition.bei.description, for: .normal)
            }),
            PopMenuDefaultAction(title: Preposition.für.description, didSelect: { (PopMenuAction) in
                self.prepositionButton.setTitle(Preposition.für.description, for: .normal)
            }),
            PopMenuDefaultAction(title: Preposition.gegen.description, didSelect: { (PopMenuAction) in
                self.prepositionButton.setTitle(Preposition.gegen.description, for: .normal)
            }),
            PopMenuDefaultAction(title: Preposition.hinter.description, didSelect: { (PopMenuAction) in
                self.prepositionButton.setTitle(Preposition.hinter.description, for: .normal)
            }),
            PopMenuDefaultAction(title: Preposition.in_.description, didSelect: { (PopMenuAction) in
                self.prepositionButton.setTitle(Preposition.in_.description, for: .normal)
            }),
            PopMenuDefaultAction(title: Preposition.mit.description, didSelect: { (PopMenuAction) in
                self.prepositionButton.setTitle(Preposition.mit.description, for: .normal)
            }),
            PopMenuDefaultAction(title: Preposition.nach.description, didSelect: { (PopMenuAction) in
                self.prepositionButton.setTitle(Preposition.nach.description, for: .normal)
            }),
            PopMenuDefaultAction(title: Preposition.neben.description, didSelect: { (PopMenuAction) in
                self.prepositionButton.setTitle(Preposition.neben.description, for: .normal)
            }),
            PopMenuDefaultAction(title: Preposition.über.description, didSelect: { (PopMenuAction) in
                self.prepositionButton.setTitle(Preposition.über.description, for: .normal)
            }),
            PopMenuDefaultAction(title: Preposition.um.description, didSelect: { (PopMenuAction) in
                self.prepositionButton.setTitle(Preposition.um.description, for: .normal)
            }),
            PopMenuDefaultAction(title: Preposition.unter.description, didSelect: { (PopMenuAction) in
                self.prepositionButton.setTitle(Preposition.unter.description, for: .normal)
            }),
            PopMenuDefaultAction(title: Preposition.von.description, didSelect: { (PopMenuAction) in
                self.prepositionButton.setTitle(Preposition.von.description, for: .normal)
            }),
            PopMenuDefaultAction(title: Preposition.zu.description, didSelect: { (PopMenuAction) in
                self.prepositionButton.setTitle(Preposition.zu.description, for: .normal)
            }),
            PopMenuDefaultAction(title: Preposition.zwischen.description, didSelect: { (PopMenuAction) in
                self.prepositionButton.setTitle(Preposition.zwischen.description, for: .normal)
            })
        ]
        
        let menu = PopMenuViewController(sourceView: sender, actions: actions)
        menu.appearance.popMenuBackgroundStyle = .none()
        menu.appearance.popMenuColor.backgroundColor = .solid(fill: white)
        menu.appearance.popMenuFont = UIFont(name: "TeXGyreAdventor-Bold", size: 14)!
        menu.appearance.popMenuColor.actionColor = .tint(black85)
        menu.appearance.popMenuCornerRadius = 14
        menu.appearance.popMenuItemSeparator = .fill(black10, height: 1)
        menu.appearance.popMenuActionCountForScrollable = 10 // default 6
        menu.appearance.popMenuScrollIndicatorHidden = false // default false
        menu.appearance.popMenuScrollIndicatorStyle = .black // default .white
        present(menu, animated: true, completion: nil)
    }
}
