//
//  TranslateCellProtocol.swift
//  prototypeGV
//
//  Created by admin on 28.06.20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import Foundation

extension NewVerbVC: TranslateCellProtocol {
    func cellTextDidChange(text: String, index: Int) {
        translatesArray[index] = text
    }
}
