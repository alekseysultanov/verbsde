//
//  NewVerbPresentable.swift
//  prototypeGV
//
//  Created by admin on 28.06.20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

extension NewVerbVC: UserPresentable {
    
    func addSubView() {
        createStack()
        view.addSubview(scrollView)
        scrollView.addSubview(commonStack)
        scrollView.addSubview(tableView)
    }
    
    func makeConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            hilfsVerbButton.widthAnchor.constraint(equalToConstant: 54),
            
            commonStack.topAnchor.constraint(equalTo: scrollView.topAnchor),
            commonStack.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            commonStack.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            commonStack.heightAnchor.constraint(equalToConstant: 298),
            commonStack.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
            
            tableView.topAnchor.constraint(equalTo: commonStack.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            tableView.heightAnchor.constraint(equalToConstant: 500),
            tableView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor)
        ])
    }
    
    @objc func addTranslateVerb() {
       
        let isEmpty = translatesArray.first { $0.isEmpty }
        
        if let isEmpty = isEmpty, isEmpty.isEmpty {
            showAlert(with: fillEmptyFields)
        } else {
            translatesArray.append("")
            if translatesArray.count == 0 {
                tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .bottom)
            } else {
                tableView.insertRows(at: [IndexPath(row: self.translatesArray.count - 1, section: 0)], with: .bottom)
            }
        }
    }
    
    func showAlert(with text: String) {
        let alert = UIAlertController(title: nil, message: text, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
}
