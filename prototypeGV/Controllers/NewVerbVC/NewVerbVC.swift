//
//  NewVerbVC.swift
//  prototypeGV
//
//  Created by Max Polinkovsky on 28.05.2019.
//  Copyright © 2019 Max Polinkovsky. All rights reserved.
//

import UIKit
import RealmSwift

protocol NewVerbDelegate {
    func choiseButton()
}

class NewVerbVC: UIViewController {
    private let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
    var delegate: NewVerbDelegate?
    var selectedVerb: Verb?
    var translatesArray: [String] = []
    var currentTheme: Theme!
    var casus: String {
        get { return casusButton.title(for: .normal)! }
        set {
            if newValue == Casus.akkusativ.description {
                casusButton.setTitle(newValue, for: .normal)
            }
        }
    }
    
    var preposition: String {
        get { return prepositionButton.title(for: .normal)! }
        set {
            if newValue == Preposition.als.description {
                prepositionButton.setTitle(newValue, for: .normal)
            }
        }
    }
    
    var hilfsVerb: String {
        get { return hilfsVerbButton.title(for: .normal)! }
        set {
            if newValue == HilfsVerb.hat.description {
                hilfsVerbButton.setTitle(newValue, for: .normal)
            }
        }
    }
    
    lazy var saveButton: UIBarButtonItem = {
        let button = UIBarButtonItem(image: UIImage(named: "add"),
                                     style: .plain,
                                     target: self,
                                     action: #selector(saveVerb(_:)))
        button.isEnabled = false
        return button
    }()
    
    lazy var cancelButton: UIBarButtonItem = {
        let button = UIBarButtonItem(image: UIImage(named: "close"), style: .plain, target: self, action: #selector(cancelAction(_:)))
        return button
    }()
    
    lazy var infinitivLine: UIImageView = {
        let image = UIImageView(image: UIImage(named: "line"))
        image.translatesAutoresizingMaskIntoConstraints = false
        image.heightAnchor.constraint(equalToConstant: 1).isActive = true
        return image
    }()
    
    lazy var preteriteLine: UIImageView = {
        let image = UIImageView(image: UIImage(named: "line"))
        image.translatesAutoresizingMaskIntoConstraints = false
        image.heightAnchor.constraint(equalToConstant: 1).isActive = true
        return image
    }()
    
    lazy var perfectLine: UIImageView = {
        let image = UIImageView(image: UIImage(named: "line"))
        image.translatesAutoresizingMaskIntoConstraints = false
        image.heightAnchor.constraint(equalToConstant: 1).isActive = true
        return image
    }()
    
    lazy var casusLine: UIImageView = {
        let image = UIImageView(image: UIImage(named: "line"))
        image.translatesAutoresizingMaskIntoConstraints = false
        image.heightAnchor.constraint(equalToConstant: 1).isActive = true
        return image
    }()
    
    lazy var infinitivLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 14)
        label.alpha = 0.5
        label.textAlignment = .left
        label.text = infinitiv
        return label
    }()
    
    lazy var infinitivTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textAlignment = .left
        textField.contentVerticalAlignment = .bottom
        textField.borderStyle = .none
        textField.autocorrectionType = .yes
        textField.autocapitalizationType = .none
        textField.smartInsertDeleteType = .yes
        textField.spellCheckingType = .yes
        textField.font = UIFont(name: "TeXGyreAdventor-Bold", size: 20)
        textField.layer.masksToBounds = true
        textField.text = ""
        textField.placeholder = "add word".localized()
        textField.addTarget(self, action: #selector(textFieldChanged), for: .editingChanged)
        return textField
    }()
    
    lazy var perfectLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 14)
        label.alpha = 0.5
        label.textAlignment = .left
        label.text = perfect
        return label
    }()
    
    lazy var hilfsVerbButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(hilfsVerbAction(_:)), for: .touchUpInside)
        button.contentMode = .left
        button.contentHorizontalAlignment = .right
        button.titleLabel!.font = UIFont(name: "TeXGyreAdventor-Bold", size: 20)
        button.setTitle("hat", for: .normal)
        button.setImage(UIImage(named: "selection"), for: .normal)
        button.semanticContentAttribute = .forceRightToLeft
        return button
    }()
    
    lazy var perfectTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textAlignment = .left
        textField.contentVerticalAlignment = .bottom
        textField.borderStyle = .none
        textField.autocorrectionType = .yes
        textField.autocapitalizationType = .none
        textField.smartInsertDeleteType = .yes
        textField.spellCheckingType = .yes
        textField.font = UIFont(name: "TeXGyreAdventor-Bold", size: 20)
        textField.layer.masksToBounds = true
        textField.text = ""
        textField.placeholder = "add word".localized()
        textField.addTarget(self, action: #selector(textFieldChanged), for: .editingChanged)
        return textField
    }()
    
    lazy var preteriteLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 14)
        label.alpha = 0.5
        label.textAlignment = .left
        label.text = preterite
        return label
    }()
    
    lazy var preteriteTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textAlignment = .left
        textField.contentVerticalAlignment = .bottom
        textField.borderStyle = .none
        textField.autocorrectionType = .yes
        textField.autocapitalizationType = .none
        textField.smartInsertDeleteType = .yes
        textField.spellCheckingType = .yes
        textField.font = UIFont(name: "TeXGyreAdventor-Bold", size: 20)
        textField.layer.masksToBounds = true
        textField.text = ""
        textField.placeholder = "add word".localized()
        textField.addTarget(self, action: #selector(textFieldChanged), for: .editingChanged)
        return textField
    }()
    
    lazy var casusLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 14)
        label.alpha = 0.5
        label.textAlignment = .left
        label.text = "Casus".localized()
        return label
    }()
    
    lazy var casusButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(setCasus(_:)), for: .touchUpInside)
        button.contentMode = .left
        button.contentHorizontalAlignment = .left
        button.titleLabel!.font = UIFont(name: "TeXGyreAdventor-Bold", size: 20)
        button.setTitle("akk", for: .normal)
        button.setImage(UIImage(named: "selection"), for: .normal)
        button.semanticContentAttribute = .forceRightToLeft
        return button
    }()
    
    lazy var prepositionButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(setPreposition(_:)), for: .touchUpInside)
        button.contentMode = .left
        button.contentHorizontalAlignment = .left
        button.titleLabel!.font = UIFont(name: "TeXGyreAdventor-Bold", size: 20)
        button.setTitle("als", for: .normal)
        button.setImage(UIImage(named: "selection"), for: .normal)
        button.semanticContentAttribute = .forceRightToLeft
        return button
    }()
    
    lazy var stackInfinitiv: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .fill
        stack.distribution = .fillEqually
        stack.spacing = 0
        stack.contentMode = .center
        stack.layoutMargins = UIEdgeInsets(top: 0, left: 27, bottom: 0, right: 27)
        stack.isLayoutMarginsRelativeArrangement = true
        stack.heightAnchor.constraint(equalToConstant: 74).isActive = true
        return stack
    }()
    
    lazy var pefectHorizontalStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.alignment = .fill
        stack.distribution = .fillProportionally
        stack.spacing = 20
        return stack
    }()
    
    lazy var stackPerfect: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.alignment = .fill
        stack.distribution = .fillEqually
        stack.spacing = 0
        stack.contentMode = .center
        stack.layoutMargins = UIEdgeInsets(top: 0, left: 27, bottom: 0, right: 27)
        stack.isLayoutMarginsRelativeArrangement = true
        stack.heightAnchor.constraint(equalToConstant: 74).isActive = true
        return stack
    }()
   
    lazy var stackPreterite: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.alignment = .fill
        stack.distribution = .fillEqually
        stack.spacing = 0
        stack.contentMode = .center
        stack.layoutMargins = UIEdgeInsets(top: 0, left: 27, bottom: 0, right: 27)
        stack.isLayoutMarginsRelativeArrangement = true
        stack.heightAnchor.constraint(equalToConstant: 74).isActive = true
        return stack
    }()
    
    lazy var stackCasusLabel: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.alignment = .leading
        stack.distribution = .fill
        stack.spacing = 0
        stack.contentMode = .left
        stack.layoutMargins = UIEdgeInsets(top: 0, left: 27, bottom: 0, right: 27)
        stack.isLayoutMarginsRelativeArrangement = true
        return stack
    }()
    
    lazy var stackCasus: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .horizontal
        stack.alignment = .fill
        stack.distribution = .fillProportionally
        stack.spacing = 30
        stack.contentMode = .left
        return stack
    }()
    
    lazy var commonStack: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.alignment = .fill
        stack.distribution = .fill
        stack.spacing = 0
        stack.contentMode = .scaleAspectFill
        return stack
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: UITableView.Style.plain)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.separatorStyle = .singleLine
        tableView.showsVerticalScrollIndicator = false
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(TranslateCell.self, forCellReuseIdentifier: String(describing: TranslateCell.self))
        return tableView
    }()
    
    lazy var addTranslateButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "add"), for: .normal)
        button.addTarget(self, action: #selector(addTranslateVerb), for: .touchUpInside)
        return button
    }()
    
    lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    
    private var isShownKeyboard = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setName()
        editVerb()
        makeNavigationBar()
        makeTableHeaderView()
        setup()
        tableView.tableFooterView = UIView()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil);
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: UIResponder.keyboardWillHideNotification, object: nil);
        view.backgroundColor = white
        view.addTapGestureToHideKeyboard()
        loadDarkTheme()
    }
    
    private func makeNavigationBar() {
        navigationItem.rightBarButtonItem = saveButton
        navigationItem.leftBarButtonItem = cancelButton
    }
    
    func createStack() {
        
        commonStack.addArrangedSubview(stackInfinitiv)
        commonStack.addArrangedSubview(infinitivLine)
        commonStack.addArrangedSubview(stackPreterite)
        commonStack.addArrangedSubview(preteriteLine)
        commonStack.addArrangedSubview(stackPerfect)
        commonStack.addArrangedSubview(perfectLine)
        commonStack.addArrangedSubview(stackCasusLabel)
        commonStack.addArrangedSubview(casusLine)
        
        stackInfinitiv.addArrangedSubview(infinitivTextField)
        stackInfinitiv.addArrangedSubview(infinitivLabel)
        
        stackPreterite.addArrangedSubview(preteriteTextField)
        stackPreterite.addArrangedSubview(preteriteLabel)
        
        pefectHorizontalStack.addArrangedSubview(perfectTextField)
        pefectHorizontalStack.addArrangedSubview(hilfsVerbButton)
        stackPerfect.addArrangedSubview(pefectHorizontalStack)
        stackPerfect.addArrangedSubview(perfectLabel)
        
        stackCasus.addArrangedSubview(casusButton)
        stackCasus.addArrangedSubview(prepositionButton)
        
        stackCasusLabel.addArrangedSubview(stackCasus)
        stackCasusLabel.addArrangedSubview(casusLabel)
    }
    
    private func saveVerb() -> Bool {
        var value = false
        
        translatesArray = translatesArray.compactMap({ $0.lowercased().delSpaces() })
        let dups = Dictionary(grouping: translatesArray, by: {$0}).filter { $1.count > 1 }.keys
        
        if dups.count > 0 {
            showAlert(with: duplicatesInTranslations)
        } else {
            let newVerb = Verb(infinitiv: infinitivTextField.text?.delSpaces().lowercased() ?? "",
                               translate: [Translates(locale: Verb().getLocale(), translates: translatesArray)],
                               prateritum: preteriteTextField.text?.delSpaces().lowercased() ?? "",
                               hilfsVerb: hilfsVerb,
                               perfect: perfectTextField.text?.delSpaces().lowercased() ?? "",
                               casus: casus,
                               preposition: preposition,
                               theme: currentTheme.name)
            
            if selectedVerb != nil {
                try! realm.write ({
                    selectedVerb?.infinitiv = newVerb.infinitiv.delSpaces().lowercased()
                    
                    for translate in self.translatesArray {
                        guard let contains = selectedVerb?.getTranslate().contains(translate.delSpaces().lowercased()) else { return }
                        if !contains {
                            selectedVerb?.appendTranslate(value: translate.delSpaces().lowercased())
                        }
                    }
                    
                    selectedVerb?.perfect = newVerb.perfect.delSpaces().lowercased()
                    selectedVerb?.prateritum = newVerb.prateritum.delSpaces().lowercased()
                    selectedVerb?.casus = newVerb.casus
                    selectedVerb?.preposition = newVerb.preposition
                    selectedVerb?.hilfsVerb = newVerb.hilfsVerb
                    
                    value = true
                    
                    selectedVerb?.saveToFirebaseStep(fixed: currentTheme.getThemePath())
                })
            } else {
                DataManager.add(newVerb)
                newVerb.saveToFirebaseStep(fixed: currentTheme.getThemePath())
                value = true
            }
        }
        
        return value
    }
    
    private func editVerb() {
        if selectedVerb != nil {
            infinitivTextField.text = selectedVerb?.infinitiv.delSpaces()
            perfectTextField.text = selectedVerb?.perfect.delSpaces()
            preteriteTextField.text = selectedVerb?.prateritum.delSpaces()
            translatesArray = Array(selectedVerb?.getTranslate() ?? List<String>())
            casus = selectedVerb?.casus ?? ""
            preposition = selectedVerb?.preposition ?? ""
            hilfsVerb = selectedVerb?.hilfsVerb ?? ""
        } 
    }
    
    private func setName() {
        title = selectedVerb?.infinitiv ?? addNewVerb
        if selectedVerb != nil {
            cancelButton.image = UIImage(named: "back")
            saveButton.image = nil
            saveButton.tintColor = azure
            saveButton.title = edit
        } else {
            cancelButton.image = UIImage(named: "close")
            saveButton.image = UIImage(named: "save")
        }
    }
    
    @objc func saveVerb(_ sender: Any) {
        let isEmpty = translatesArray.first { $0.isEmpty }
        
        if isEmpty != nil || translatesArray.isEmpty {
            showAlert(with: fillTranslate)
        } else {
            if realm.objects(Verb.self).filter("infinitiv = %@",infinitivTextField.text?.lowercased().delSpaces() ?? "").first != nil && selectedVerb == nil {
                showAlert(with: wordAlreadyExists)
            } else {
                if saveVerb() {
                    delegate?.choiseButton()
                    navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    @objc func cancelAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func keyboardWillShow(sender: NSNotification) {
        guard !infinitivTextField.isFirstResponder && !perfectTextField.isFirstResponder && !preteriteTextField.isFirstResponder && (isShownKeyboard == false) else { return }
        scrollView.contentSize.height += 150
        isShownKeyboard = true
    }
    
    @objc private func keyboardWillHide(sender: NSNotification) {
        guard isShownKeyboard else { return }
        scrollView.contentSize.height -= 150
        isShownKeyboard = false
    }
    
    private func makeTableHeaderView() {
        let headerTableView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 60))
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.alpha = 0.5
        label.text = translates
        label.textColor = userCurretChoiceIs ? white : black85
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 14)
        tableView.tableHeaderView = headerTableView
        headerTableView.addSubview(label)
        headerTableView.addSubview(addTranslateButton)
        headerTableView.backgroundColor = userCurretChoiceIs ? black : veryLightPinkTwo15
        label.heightAnchor.constraint(equalToConstant: 22).isActive = true
        label.centerYAnchor.constraint(equalTo: headerTableView.centerYAnchor).isActive = true
        label.leftAnchor.constraint(equalTo: headerTableView.leftAnchor, constant: 27).isActive = true
        addTranslateButton.leftAnchor.constraint(equalTo: label.rightAnchor, constant: 10).isActive = true
        addTranslateButton.centerYAnchor.constraint(equalTo: label.centerYAnchor, constant: 0).isActive = true
        tableView.tableHeaderView = headerTableView
        tableView.rowHeight = 54
    }
    
    func loadDarkTheme(){
        tableView.backgroundColor = userCurretChoiceIs ? black : veryLightPinkTwo15
        tableView.separatorColor  = userCurretChoiceIs ? black : .gray
        view.backgroundColor = userCurretChoiceIs ? darkGrey : white
        cancelButton.tintColor = userCurretChoiceIs ? white : black
        infinitivLabel.textColor = userCurretChoiceIs ? white : black85
        infinitivTextField.textColor = userCurretChoiceIs ? white : black85
        infinitivTextField.attributedPlaceholder = NSAttributedString(string:"add word".localized(), attributes: [NSAttributedString.Key.foregroundColor: userCurretChoiceIs ? white : black85])
        infinitivTextField.tintColor = userCurretChoiceIs ? white : black85
        perfectTextField.textColor = userCurretChoiceIs ? white : black85
        perfectTextField.attributedPlaceholder = NSAttributedString(string:"add word".localized(), attributes: [NSAttributedString.Key.foregroundColor: userCurretChoiceIs ? white : black85])
        perfectLabel.textColor = userCurretChoiceIs ? white : black85
        preteriteLabel.textColor = userCurretChoiceIs ? white : black85
        preteriteTextField.textColor = userCurretChoiceIs ? white : black85
        preteriteTextField.attributedPlaceholder = NSAttributedString(string:"add word".localized(), attributes: [NSAttributedString.Key.foregroundColor: userCurretChoiceIs ? white : black85])
        casusLabel.textColor = userCurretChoiceIs ? white : black85
        casusButton.setTitleColor(userCurretChoiceIs ? white : black85, for: .normal)
        prepositionButton.setTitleColor(userCurretChoiceIs ? white : black85, for: .normal)
        hilfsVerbButton.setTitleColor(userCurretChoiceIs ? white : black85, for: .normal)
        infinitivLine.backgroundColor = userCurretChoiceIs ? black : black10
        perfectLine.backgroundColor = userCurretChoiceIs ? black : black10
        preteriteLine.backgroundColor = userCurretChoiceIs ? black : black10
        casusLine.backgroundColor = userCurretChoiceIs ? black : black10
    }
}
