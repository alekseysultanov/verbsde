//
//  NewVerbTable.swift
//  prototypeGV
//
//  Created by admin on 28.06.20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

extension NewVerbVC: UITableViewDelegate {
}

extension NewVerbVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .normal, title:  "Delete".localized(), handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            self.translatesArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        })
        deleteAction.backgroundColor = vermillion
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return translatesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TranslateCell.self), for: indexPath) as? TranslateCell else { return UITableViewCell() }
        cell.selectionStyle = .none
        cell.backgroundColor = userCurretChoiceIs ? darkGrey : white
        cell.loadDarkTheme()
        cell.delegate = self
        cell.update(with: translatesArray[indexPath.row].delSpaces(), index: indexPath.row)
        return cell
    }
}
