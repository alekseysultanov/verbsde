//
//  UserPresentable.swift
//  prototypeGV
//
//  Created by admin on 07.12.19.
//  Copyright © 2019 Max Polinkovsky. All rights reserved.
//

import UIKit

protocol UserPresentable: class {
    
    func addSubView()
    func makeConstraints()
}

extension UserPresentable {
    
    func setup() {
        addSubView()
        makeConstraints()
    }
}
