//
//  ResultsPresentable.swift
//  prototypeGV
//
//  Created by admin on 28.06.20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

extension ResultsVC: UserPresentable {
    
    func addSubView() {
        view.addSubview(resultLabel)
        view.addSubview(repeatLabel)
        view.addSubview(tableView)
        view.addSubview(closeButton)
        view.addSubview(nextButton)
    }
    
    func makeConstraints() {
        NSLayoutConstraint.activate([
            resultLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 80),
            resultLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            repeatLabel.topAnchor.constraint(equalTo: resultLabel.bottomAnchor, constant: 60),
            repeatLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 27),
            repeatLabel.widthAnchor.constraint(equalToConstant: 150),
            repeatLabel.heightAnchor.constraint(equalToConstant: 21),
            
            tableView.topAnchor.constraint(equalTo: repeatLabel.bottomAnchor, constant: 10),
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 27),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -27),
            tableView.bottomAnchor.constraint(equalTo: nextButton.topAnchor, constant: 0),
            
            closeButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            closeButton.heightAnchor.constraint(equalToConstant: 32),
            closeButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -59),
            
            nextButton.bottomAnchor.constraint(equalTo: closeButton.topAnchor, constant: -30),
            nextButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 8),
            nextButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -8),
            nextButton.heightAnchor.constraint(equalToConstant: 56)
        ])
    }
}
