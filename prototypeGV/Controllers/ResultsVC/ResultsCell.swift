//
//  ResultsCell.swift
//  prototypeGV
//
//  Created by admin on 19.04.20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

class ResultsCell: UITableViewCell {
    
    lazy var infinitiv: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = black
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 20)
        return label
    }()
    
    lazy var translate: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = black
        label.alpha = 0.4
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 14)
        return label
    }()
    
    let separatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .gray
        view.alpha = 0.2
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: cellIdentifier)
        setup()
    }
    
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}

extension ResultsCell: UserPresentable {
    
    func addSubView() {
        contentView.addSubview(infinitiv)
        contentView.addSubview(translate)
        contentView.addSubview(separatorView)
    }
    
    func makeConstraints() {
        NSLayoutConstraint.activate([
            infinitiv.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            infinitiv.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            infinitiv.heightAnchor.constraint(equalToConstant: 31),
            
            translate.topAnchor.constraint(equalTo: infinitiv.bottomAnchor, constant: 2),
            translate.heightAnchor.constraint(equalToConstant: 21),
            translate.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            
            separatorView.heightAnchor.constraint(equalToConstant: 1),
            separatorView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            separatorView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            separatorView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
}
