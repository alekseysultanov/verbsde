//
//  ResultsVC.swift
//  prototypeGV
//
//  Created by  Artem Valeryevich on 09.12.2019.
//  Copyright © 2019 Max Polinkovsky. All rights reserved.
//

import UIKit

class ResultsVC: UIViewController {
    weak var delegate: TestVC?
    
    var continueTestShow = true
    
    /// Массив неправильных слов
    var resultsWrongArray: [Verb] = []
    private let goodJob = "Good job!".localized()
    private let doCanBetter = "You can do better!".localized()
    
    /// Результат, количество верно отвеченных из общего количества слов
    lazy var resultLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
//        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 32)
        label.textColor = white
        label.textAlignment = .center
        label.numberOfLines = 2
        return label
    }()
    
    lazy var repeatLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 14)
        label.textColor = white
        label.text = repeatVerbs
        return label
    }()
    
    lazy var tableView: UITableView = {
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        table.delegate = self
        table.dataSource = self
        table.backgroundColor = .clear
        table.register(ResultsCell.self, forCellReuseIdentifier: cellIdentifier)
        return table
    }()
    
    lazy var nextButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Next!".localized(), for: .normal)
        button.backgroundColor = white
        button.layer.cornerRadius = 12
        button.setTitleColor(black, for: .normal)
        button.titleLabel?.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        button.addTarget(self, action: #selector(continueTest), for: .touchUpInside)
        return button
    }()
    
    lazy var closeButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.contentHorizontalAlignment = .right
        button.semanticContentAttribute = .forceLeftToRight
        button.contentMode = .right
        button.backgroundColor = .clear
        button.setTitle(close, for: .normal)
        button.titleLabel!.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        button.setTitleColor(white, for: .normal)
//        button.setImage(UIImage(named: "closeWhite"), for: .normal)
//        button.setImage(UIImage(named: "closeWhite"), for: .highlighted)
//        button.imageEdgeInsets.right = -11
        button.addTarget(self, action: #selector(closeVC), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        tableView.tableFooterView = UIView()
        setup()
        isIphoneSE()
        updateDataView()
    }
    
    /// Обновление данных на Вью
    private func updateDataView() {
        guard let wrongArray = delegate?.getWrongArray() else { return }
        resultsWrongArray = wrongArray
        updateLabelText()
        tableView.reloadData()
    }
    
    private func isIphoneSE() {
        var heightScreen: CGFloat = 0
        var widthScreen: CGFloat = 0
                   
        let bounds = UIScreen.main.bounds
        heightScreen = bounds.size.height //get the height of screen
        widthScreen = bounds.size.width //get the width of screen
        
        if heightScreen == 568.0 && widthScreen == 320.0 {
        resultLabel.font = UIFont(name: "TeXGyreAdventor-Bold", size: 28)
        } else {
        resultLabel.font = UIFont(name: "TeXGyreAdventor-Bold", size: 32)
        }
    }
    
    
    
    private func updateLabelText() {
        guard let rightCount = delegate?.getRightArray().count,
            let allProgressCount = delegate?.getAllCountForResult() else { return }
        
        if Float(rightCount) >= Float(allProgressCount) * 0.6 {
            resultLabel.text = "\(goodJob) \n \(rightCount)/\(allProgressCount)"
            view.backgroundColor = darkLimeGreen
        } else {
            resultLabel.text = "\(doCanBetter) \n \(rightCount)/\(allProgressCount)"
            view.backgroundColor = brightOrange
        }
        
        repeatLabel.isHidden = rightCount == allProgressCount
        nextButton.alpha = continueTestShow ? 1 : 0
    }
    
    @objc func closeVC() {
        saveLearnedCount()
        dismiss(animated: true, completion: nil)
        delegate?.navigationController?.popViewController(animated: false)
    }
    
    @objc func continueTest() {
        dismiss(animated: true)
    }
    
    private func saveLearnedCount() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        let dateString = dateFormatter.string(from: Date())
        
        guard let oldCount = delegate?.selectedTheme.learnCount,
            let learnCount = delegate?.selectedTheme.getRightVerbCount(),
            let oldScore = delegate?.selectedTheme.learnCount,
            let saveDate = delegate?.selectedTheme.date,
            let themeName = delegate?.selectedTheme.name else { return }
        
        var score = 0
        if saveDate == dateString {
            score = oldScore + learnCount - oldCount
        } else {
            score = learnCount - oldCount
        }
        
        try! realm.write {
            delegate?.selectedTheme.oldCount = learnCount
            delegate?.selectedTheme.learnCount = score
            delegate?.selectedTheme.date = dateString
        }
        
        DataManager.saveLernThemeToFirebase(date: dateString, oldCount: learnCount, learnCount: score, theme: themeName)
    }
}
