//
//  ActivityController.swift
//  prototypeGV
//
//  Created by  Artem Valeryevich on 21.01.2020.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//
import RealmSwift
import UIKit

protocol DownloadCloudDelegate {
    func fileCloud(array: Results<Theme>)
}

class ActivityController: UIViewController {
    
    let rotationImage = UIImageView()
    var lock: Bool = false
    let imageLock = UIImageView()
    
    lazy var dotsStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.spacing = 7
        return stack
    }()
    
    lazy var imageDots: [UIImageView] = (1...3).map{ _ in UIImageView() }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.black
        view.alpha = 0
        rotationImageConfigure()
        view.isOpaque = false
        
        if lock {
            imageLockConfigure()
            setupDotsStackView()
            configureDotsStackView()
        }
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func setupDotsStackView() {
        for (_, img) in imageDots.enumerated() {
            img.image = UIImage(named: "dots_indicator")
            img.alpha = 0
            img.contentMode = .scaleAspectFit
        }
        view.addSubview(dotsStackView)
        imageDots.forEach {
            self.dotsStackView.addArrangedSubview($0)
        }
    }
    
    func configureDotsStackView() {
        dotsStackView.translatesAutoresizingMaskIntoConstraints = false
        
        dotsStackView.topAnchor.constraint(equalTo: imageLock.bottomAnchor, constant: 20).isActive = true
        dotsStackView.bottomAnchor.constraint(equalTo: rotationImage.topAnchor, constant: -20).isActive = true
        dotsStackView.widthAnchor.constraint(equalToConstant: 19).isActive = true
        dotsStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    func imageLockConfigure() {
        imageLock.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(imageLock)
        imageLock.image = UIImage(named: "lock")
        imageLock.alpha = 0
        imageLock.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageLock.bottomAnchor.constraint(equalTo: rotationImage.topAnchor, constant: -115).isActive = true
        imageLock.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.1).isActive = true
        imageLock.heightAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.1).isActive = true
    }
    
    func rotationImageConfigure() {
        rotationImage.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(rotationImage)
        rotationImage.image = UIImage(named: "ref")
        rotationImage.alpha = 0
        rotationImage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        rotationImage.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -8).isActive = true
        rotationImage.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.15).isActive = true
        rotationImage.heightAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.15).isActive = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        animationIn()
    }
    
    func animationIn() {
        UIView.animate(withDuration: 0.5, animations: {
            self.view.alpha = 0.6
            
        }, completion: { (_) in
            
            UIView.animate(withDuration: 1, animations: {
                self.rotationImage.alpha = 0.9
                self.rotationImage.rotate()
                
            }) { (_) in
                UIView.animate(withDuration: 0.6, animations: {
                    self.imageLock.alpha = 0.9
                }, completion: { (_) in
                    self.animateDots()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 4.1) {
                        self.animationOut()
                    }
                })
            }
        })
    }
    
    func animateDots() {
        
        for (index, image) in imageDots.enumerated() {
            let delay = Double(index) * 0.2
            UIView.animate(withDuration: 0.7, delay: delay, options: [.repeat, .autoreverse], animations: {
                image.alpha = 0.6
            }) { (_) in
            }
        }
    }
    
    func animationOut() {
        UIView.animate(withDuration: 0.2, animations: {
            self.imageLock.alpha = 0
        }) { (_) in
            UIView.animate(withDuration: 0.2, animations: {
                self.dotsStackView.alpha = 0
            }, completion: { (_) in
                UIView.animate(withDuration: 0.3, animations: {
                    self.rotationImage.alpha = 0
                }, completion: { (_) in
                    UIView.animate(withDuration: 0.4, animations: {
                        self.view.alpha = 0
                    }) { (_) in
                        self.dismiss(animated: false, completion: nil)
                        
                    }
                  
                })
            })
        }
        
        
    }
    
}

extension UIImageView {
    private static let kRotationAnimationKey = "rotationanimationkey"
    
    func rotate(duration: Double = 3) {
        if layer.animation(forKey: UIImageView.kRotationAnimationKey) == nil {
            let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
            rotationAnimation.fromValue = 0.0
            rotationAnimation.toValue = Float.pi * 2.0
            rotationAnimation.duration = duration
            rotationAnimation.repeatCount = Float.infinity
            
            layer.add(rotationAnimation, forKey: UIImageView.kRotationAnimationKey)
        }
    }
    
    func stopRotating() {
        if layer.animation(forKey: UIImageView.kRotationAnimationKey) != nil {
            layer.removeAnimation(forKey: UIImageView.kRotationAnimationKey)
            
        }
    }
}


