//
//  IAPManager.swift
//  prototypeGV
//
//  Created by Vladimir Ereskin on 8/14/20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit
import StoreKit

public typealias SuccessBlock = () -> Void
public typealias FailureBlock = (Error?) -> Void
let IAP_PRODUCTS_DID_LOAD_NOTIFICATION = Notification.Name("IAP_PRODUCTS_DID_LOAD_NOTIFICATION")

class IAPManager : NSObject {
    
    private var sharedSecret = "033e6e8cf7f04e95b7553c1d8aaa9bd8"
    @objc static let shared = IAPManager()
    @objc var products : Array<SKProduct>?
    var productsArray: [Product] = []
    
    private override init(){}
    private var productIds : Set<String> = []
    
    private var successBlock : SuccessBlock?
    private var failureBlock : FailureBlock?
    
    private var refreshSubscriptionSuccessBlock : SuccessBlock?
    private var refreshSubscriptionFailureBlock : FailureBlock?
    
    // MARK:- Main methods
    
    @objc func startWith(arrayOfIds : Set<String>!){
        SKPaymentQueue.default().add(self)
        self.productIds = arrayOfIds
        loadProducts()
    }
    
    func expirationDateFor(_ identifier : String) -> Date?{
        return defaults.object(forKey: identifier) as? Date
    }
    
    func purchaseProduct(product : SKProduct, success: @escaping SuccessBlock, failure: @escaping FailureBlock){
        
        guard SKPaymentQueue.canMakePayments() else {
            return
        }
        guard SKPaymentQueue.default().transactions.last?.transactionState != .purchasing else {
            return
        }
        self.successBlock = success
        self.failureBlock = failure
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }
    
    func restorePurchases(success: @escaping SuccessBlock, failure: @escaping FailureBlock){
        self.successBlock = success
        self.failureBlock = failure
        SKPaymentQueue.default().restoreCompletedTransactions()
    }

    func refreshSubscriptionsStatus(callback : @escaping SuccessBlock, failure : @escaping FailureBlock){
        
        self.refreshSubscriptionSuccessBlock = callback
        self.refreshSubscriptionFailureBlock = failure
        
        guard let receiptUrl = Bundle.main.appStoreReceiptURL else {
            refreshReceipt()
            return
        }
        
        #if DEBUG
        let urlString = "https://sandbox.itunes.apple.com/verifyReceipt"
        #else
        let urlString = "https://buy.itunes.apple.com/verifyReceipt"
        #endif
        let receiptData = try? Data(contentsOf: receiptUrl).base64EncodedString()
        let requestData = ["receipt-data" : receiptData ?? "", "password" : self.sharedSecret, "exclude-old-transactions" : true] as [String : Any]
        var request = URLRequest(url: URL(string: urlString)!)
        request.httpMethod = "POST"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        let httpBody = try? JSONSerialization.data(withJSONObject: requestData, options: [])
        request.httpBody = httpBody
        
        URLSession.shared.dataTask(with: request)  { (data, response, error) in
            DispatchQueue.main.async {
                if data != nil {
                    if let json = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments){
                        self.parseReceipt(json as! Dictionary<String, Any>)
                        return
                    }
                } else {
                    print("error validating receipt: \(error?.localizedDescription ?? "")")
                }
                self.refreshSubscriptionFailureBlock?(error)
                self.cleanUpRefeshReceiptBlocks()
            }
            }.resume()
    }
    
    /// Проверь что в этом методе данные записываются в UserDefaults
    private func parseReceipt(_ json : Dictionary<String, Any>) {
        guard let receipts_array = json["latest_receipt_info"] as? [Dictionary<String, Any>] else {
            self.refreshSubscriptionFailureBlock?(nil)
            self.cleanUpRefeshReceiptBlocks()
            return
        }
        for receipt in receipts_array {
            let productID = receipt["product_id"] as! String
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
            if let date = formatter.date(from: receipt["expires_date"] as! String) {
                // вернуть все на date вместо newDate
                let newDate = makeDateExpiration(for: productID, date: date)
                if newDate > Date() {
                    defaults.set(newDate, forKey: productID)
                }
            } // интернета нет на телефоне
        }
        self.refreshSubscriptionSuccessBlock?()
        self.cleanUpRefeshReceiptBlocks()
    }
    
    private func makeDateExpiration(for id: String, date: Date) -> Date {
        switch id {
        case Products.oneMonth.rawValue:
            return Calendar.current.date(byAdding: .month, value: 1, to: date)!
        case Products.twelveMonths.rawValue:
            return Calendar.current.date(byAdding: .year, value: 1, to: date)!
        case Products.oneYear.rawValue:
            return Calendar.current.date(byAdding: .year, value: 1, to: date)!
        default:
            return Date()
        }
    }
    
    private func refreshReceipt(){
        let request = SKReceiptRefreshRequest(receiptProperties: nil)
        request.delegate = self
        request.start()
    }
    
    private func loadProducts(){
        let request = SKProductsRequest.init(productIdentifiers: productIds)
        request.delegate = self
        request.start()
    }
    
    private func cleanUpRefeshReceiptBlocks(){
        self.refreshSubscriptionSuccessBlock = nil
        self.refreshSubscriptionFailureBlock = nil
    }
}

// MARK:- SKReceipt Refresh Request Delegate
extension IAPManager : SKRequestDelegate {
    
    func requestDidFinish(_ request: SKRequest) {
        if request is SKReceiptRefreshRequest {
            refreshSubscriptionsStatus(callback: self.successBlock ?? {}, failure: self.failureBlock ?? {_ in})
        }
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error){
        if request is SKReceiptRefreshRequest {
            self.refreshSubscriptionFailureBlock?(error)
            self.cleanUpRefeshReceiptBlocks()
        }
        print("error: \(error.localizedDescription)")
    }
}

// MARK:- SKProducts Request Delegate
extension IAPManager: SKProductsRequestDelegate {
    
    public func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        products = response.products
        makeCurrentProduct(products: products)
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: IAP_PRODUCTS_DID_LOAD_NOTIFICATION, object: nil)
        }
    }
    
    private func makeCurrentProduct(products: [SKProduct]?) {
        guard let products = products else { return }
        for item in products {
            
            productsArray.append(Product(
                periodText: getPeriodDescription(for: Int(item.subscriptionPeriod?.unit.rawValue ?? 100)),
                periodValue: item.subscriptionPeriod?.numberOfUnits ?? 0,
                priceValue: Double(truncating: item.price),
                priceSimbol: item.priceLocale.currencySymbol ?? "",
                trialPeriod: "7 days trial",
                productId: item.productIdentifier))
        }
        
        productsArray = productsArray.sorted(by: { (product1, product2) -> Bool in
            return product1.priceValue < product2.priceValue
        })
    }
    
    private func getPeriodDescription(for unit: Int) -> String {
        switch unit {
        case 0:
            return "day"
        case 1:
            return monthPeriod
        case 2:
            return monthsPeriod
        case 3:
            return yearPeriod
        default:
            return ""
        }
    }
}

// MARK:- SKPayment Transaction Observer
extension IAPManager: SKPaymentTransactionObserver {
    
    public func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        for transaction in transactions {
            switch (transaction.transactionState) {
            case .purchased:
                SKPaymentQueue.default().finishTransaction(transaction)
                notifyIsPurchased(transaction: transaction)
                break
            case .failed:
                SKPaymentQueue.default().finishTransaction(transaction)
                print("purchase error : \(transaction.error?.localizedDescription ?? "")")
                self.failureBlock?(transaction.error)
                cleanUp()
                break
            case .restored:
                SKPaymentQueue.default().finishTransaction(transaction)
                notifyIsPurchased(transaction: transaction)
                break
            case .deferred, .purchasing:
                break
            default:
                break
            }
        }
    }
    
    private func notifyIsPurchased(transaction: SKPaymentTransaction) {
        refreshSubscriptionsStatus(callback: {
            self.successBlock?()
            self.cleanUp()
        }) { (error) in
            self.failureBlock?(error)
            self.cleanUp()
        }
    }
    
    func cleanUp(){
        self.successBlock = nil
        self.failureBlock = nil
    }
}

extension IAPManager {
    func getSubscriptionStatus() -> Bool {
        let month1 = defaults.object(forKey: Products.oneMonth.rawValue)
        let months12 = defaults.object(forKey: Products.twelveMonths.rawValue)
        let year1 = defaults.object(forKey: Products.oneYear.rawValue)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
        
        var parameter = false
        
        if let value = month1 as? Date {
            if value > Date() {
                parameter = true
            }
        }
        
        if let value = months12 as? Date {
            if value > Date() {
                parameter = true
            }
        }
        
        if let value = year1 as? Date {
            if value > Date() {
                parameter = true
            }
        }
        
        return parameter
    }
}
