//
//  SubscriptionView.swift
//  prototypeGV
//
//  Created by Vladimir Ereskin on 8/13/20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

final class SubscriptionView: UIControl {
    
    weak var delegate: SubscriptionsView?
    
    var productId = ""
    var isActive = false
    
    let periodValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 2
        label.text = "6"
        label.textAlignment = .center
        return label
    }()
    
    let periodTextLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 2
        label.text = monthsPeriod
        label.textAlignment = .center
        return label
    }()
    
    let priceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 2
        label.textAlignment = .center
        return label
    }()
    
    let trialLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 3
        label.font = UIFont(name: "TeXGyreAdventor-Regular", size: 14)
        label.textColor = black85
        label.alpha = 0.5
        label.textAlignment = .center
        return label
    }()
    let backgroundLabelView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.alpha = 1
        view.layer.cornerRadius = 13
        return view
    }()
    
    let topLabelView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.alpha = 1
        view.layer.cornerRadius = 13
        return view
    }()
    
    let topLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = white
        label.backgroundColor = .clear
        label.text = "Qwerty"
        label.alpha = 1
        return label
    }()
 
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateView(isActive value: Bool) {
        let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
        isActive = value
        if value {
            backgroundColor = userCurretChoiceIs ? white : .clear
            periodValueLabel.textColor = userCurretChoiceIs ? white : black
            periodValueLabel.textColor = black85
            periodValueLabel.alpha = 1
            periodTextLabel.textColor = black85
            periodTextLabel.alpha = 1
            priceLabel.textColor = black85
            priceLabel.font = UIFont(name: "TeXGyreAdventor-Bold", size: 16)
            priceLabel.alpha = 1
            backgroundLabelView.backgroundColor = azure
            topLabelView.backgroundColor = azure
            topLabel.backgroundColor = azure
            layer.borderColor = azure.cgColor
        } else {
            backgroundColor = userCurretChoiceIs ? charcoalGrey : black10
            periodValueLabel.textColor = userCurretChoiceIs ? white50 : black85
            periodValueLabel.alpha = 0.5
            periodTextLabel.textColor = userCurretChoiceIs ? white50 : black85
            periodTextLabel.alpha = 0.5
            priceLabel.textColor = userCurretChoiceIs ? white50 : black85
            priceLabel.font = UIFont(name: "TeXGyreAdventor-Regular", size: 16)
            priceLabel.alpha = 0.5
            backgroundLabelView.backgroundColor = greyishBrown
            topLabelView.backgroundColor = greyishBrown
            topLabel.backgroundColor = greyishBrown
            layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    private func setupView() {
        translatesAutoresizingMaskIntoConstraints = false
        layer.borderWidth = 3
        layer.borderColor = azure.cgColor
        layer.cornerRadius = 14
        
        addTarget(self, action: #selector(tappedView), for: .touchUpInside)
        addSubview(periodValueLabel)
        addSubview(periodTextLabel)
        addSubview(priceLabel)
        addSubview(trialLabel)
        addSubview(backgroundLabelView)
        backgroundLabelView.addSubview(topLabelView)
//        addSubview(topLabelView)
        topLabelView.addSubview(topLabel)
    }
    
    private func makeConstraints() {
        var heightScreen: CGFloat = 0
        var widthScreen: CGFloat = 0
                   
        let bounds = UIScreen.main.bounds
        heightScreen = bounds.size.height //get the height of screen
        widthScreen = bounds.size.width //get the width of screen
        
        if heightScreen == 568.0 && widthScreen == 320.0 {
            periodValueLabel.font = UIFont(name: "TeXGyreAdventor-Bold", size: 28)
            periodTextLabel.font = UIFont(name: "TeXGyreAdventor-Bold", size: 14)
            priceLabel.font = UIFont(name: "TeXGyreAdventor-Regular", size: 12)
            trialLabel.font = UIFont(name: "TeXGyreAdventor-Bold", size: 8) //10
         NSLayoutConstraint.activate([
            periodValueLabel.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            trialLabel.topAnchor.constraint(equalTo: priceLabel.bottomAnchor, constant: 12)
         ])
        } else if heightScreen == 667.0 && widthScreen == 375.0 {
               periodValueLabel.font = UIFont(name: "TeXGyreAdventor-Bold", size: 36)
                  periodTextLabel.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
                  priceLabel.font = UIFont(name: "TeXGyreAdventor-Regular", size: 16)
                  trialLabel.font = UIFont(name: "TeXGyreAdventor-Bold", size: 10) //12
               NSLayoutConstraint.activate([
                  periodValueLabel.topAnchor.constraint(equalTo: topAnchor, constant: 30),
                  trialLabel.topAnchor.constraint(equalTo: priceLabel.bottomAnchor, constant: 18)
               ])
        } else {
            periodValueLabel.font = UIFont(name: "TeXGyreAdventor-Bold", size: 36)
            periodTextLabel.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
            priceLabel.font = UIFont(name: "TeXGyreAdventor-Regular", size: 16)
            trialLabel.font = UIFont(name: "TeXGyreAdventor-Bold", size: 11) //12
         NSLayoutConstraint.activate([
//            priceLabel.heightAnchor.constraint(equalToConstant: 48),
            periodValueLabel.topAnchor.constraint(equalTo: topAnchor, constant: 30),
            trialLabel.topAnchor.constraint(equalTo: priceLabel.bottomAnchor, constant: 30)
         ])
        }
        
        NSLayoutConstraint.activate([
            
            periodValueLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            periodValueLabel.heightAnchor.constraint(equalToConstant: 36),
            periodValueLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            periodTextLabel.topAnchor.constraint(equalTo: periodValueLabel.bottomAnchor),
            periodTextLabel.heightAnchor.constraint(equalToConstant: 18),
            periodTextLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            periodTextLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            priceLabel.topAnchor.constraint(equalTo: periodTextLabel.bottomAnchor, constant: 7),
            priceLabel.heightAnchor.constraint(equalToConstant: 48),
            priceLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            priceLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            
            trialLabel.topAnchor.constraint(equalTo: priceLabel.bottomAnchor, constant: -23),
            trialLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            trialLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            trialLabel.bottomAnchor.constraint(equalTo: bottomAnchor),//, constant: -20),
            
            backgroundLabelView.topAnchor.constraint(equalTo: topAnchor, constant: -13),
            backgroundLabelView.heightAnchor.constraint(equalToConstant: 26),
            backgroundLabelView.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
            backgroundLabelView.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
            
            topLabelView.topAnchor.constraint(equalTo: backgroundLabelView.topAnchor),
            topLabelView.heightAnchor.constraint(equalTo: backgroundLabelView.heightAnchor),
            topLabelView.leftAnchor.constraint(equalTo: backgroundLabelView.leftAnchor),
            topLabelView.rightAnchor.constraint(equalTo: backgroundLabelView.rightAnchor),
            
            topLabel.centerXAnchor.constraint(equalTo: topLabelView.centerXAnchor),
            topLabel.centerYAnchor.constraint(equalTo: topLabelView.centerYAnchor)
        ])
    }
    
    @objc private func tappedView() {
        delegate?.updateView(with: self)
    }
}
