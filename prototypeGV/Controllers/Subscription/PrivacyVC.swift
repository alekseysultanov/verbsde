//
//  PrivacyVC.swift
//  verbsde
//
//  Created by admin on 31.08.20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

class PrivacyVC: UIViewController {
    
    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "close"), for: .normal)
        button.addTarget(self, action: #selector(cancelAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var textView: UITextView = {
        let label = UITextView()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = privacy.localized()
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 14)
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadDarkTheme()
    }
    
    @objc func cancelAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    private func loadDarkTheme() {
        let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
        view.backgroundColor = userCurretChoiceIs ? darkGrey : white
        textView.backgroundColor = userCurretChoiceIs ? darkGrey : white
        textView.textColor = userCurretChoiceIs ? white : black
        closeButton.setImage(UIImage(named: userCurretChoiceIs ? "closeWhite" : "close"), for: .normal)
        closeButton.tintColor = userCurretChoiceIs ? white : black
    }
}

extension PrivacyVC: UserPresentable {
    
    func addSubView() {
        view.addSubview(closeButton)
        view.addSubview(textView)
    }
    
    func makeConstraints() {
        NSLayoutConstraint.activate([
            closeButton.heightAnchor.constraint(equalToConstant: 32),
            closeButton.widthAnchor.constraint(equalToConstant: 32),
            closeButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20),
            closeButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 47),
            
            textView.topAnchor.constraint(equalTo: closeButton.bottomAnchor, constant: 10),
            textView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20),
            textView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20),
            textView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}
