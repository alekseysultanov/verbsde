//
//  SubscriptionVC.swift
//  prototypeGV
//
//  Created by Vladimir Ereskin on 8/13/20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

enum Products: String {
    case oneMonth = "de.mahus.easy.oneMonth"
    case twelveMonths = "de.mahus.easy.twelveMonth"
    case oneYear = "de.mahus.easy.oneYear"
}

final class SubscriptionVC: UIViewController {
    
    private let iconView: UIImageView = {
        let view = UIImageView(image: UIImage(named: "icnEasy"))
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let closeButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "close"), for: .normal)
        button.addTarget(self, action: #selector(closeVC), for: .touchUpInside)
        return button
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 2
        label.text = get3Themes
        label.textAlignment = .center
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 16)
        return label
    }()
    
    private let subscriptionsView = SubscriptionsView()
    
    private let confirmationButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(confirmation, for: .normal)
        button.setTitleColor(.gray, for: .highlighted)
        button.backgroundColor = azure
        button.titleLabel?.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        button.layer.cornerRadius = 14
        button.addTarget(self, action: #selector(subscriptionAdd), for: .touchUpInside)
        return button
    }()
    
    let activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.hidesWhenStopped = true
        return indicator
    }()
    
    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = cancelAnytime
        label.textAlignment = .center
        return label
    }()
    
    private let privacyButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(terms, for: .normal)
        button.setTitleColor(azure, for: .normal)
        button.titleLabel?.font = UIFont(name: "TeXGyreAdventor-Bold", size: 16)
        button.addTarget(self, action: #selector(readPrivacy), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        makeConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isOtherIphone()
        loadDarkTheme()
        
        subscriptionsView.setupDataForViews(products: IAPManager.shared.productsArray)
        if  IAPManager.shared.getSubscriptionStatus() {
            subscriptionsView.deactivateView(is: false)
            subscriptionsView.isUserInteractionEnabled = false
            confirmationButton.isHidden = true
        } else {
            subscriptionsView.deactivateView(is: true)
            subscriptionsView.isUserInteractionEnabled = true
            confirmationButton.isHidden = false
        }
    }
    private func setupViews() {
        view.backgroundColor = .white
        view.addSubview(iconView)
        view.addSubview(closeButton)
        view.addSubview(titleLabel)
        view.addSubview(subscriptionsView)
        view.addSubview(confirmationButton)
        view.addSubview(descriptionLabel)
        view.addSubview(privacyButton)
        view.addSubview(activityIndicator)
    }
    
    private func isOtherIphone() {
        var heightScreen: CGFloat = 0
        var widthScreen: CGFloat = 0
        let bounds = UIScreen.main.bounds
        heightScreen = bounds.size.height //get the height of screen
        widthScreen = bounds.size.width //get the width of screen
        
        if heightScreen == 568.0 && widthScreen == 320.0 {
            titleLabel.font = UIFont(name: "TeXGyreAdventor-Bold", size: 14)
            descriptionLabel.font = UIFont(name: "TeXGyreAdventor-Bold", size: 12)
            iconView.layer.cornerRadius = iconView.frame.size.width / 4
            NSLayoutConstraint.activate([
                iconView.heightAnchor.constraint(equalToConstant: 60),
                iconView.widthAnchor.constraint(equalToConstant: 60),
                titleLabel.heightAnchor.constraint(equalToConstant: 60),
                titleLabel.widthAnchor.constraint(equalToConstant: 160),
                subscriptionsView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20),
                confirmationButton.topAnchor.constraint(equalTo: subscriptionsView.bottomAnchor, constant: 20)
            ])
        } else if heightScreen == 667.0 && widthScreen == 375.0 {
            titleLabel.font = UIFont(name: "TeXGyreAdventor-Bold", size: 16)
            descriptionLabel.font = UIFont(name: "TeXGyreAdventor-Bold", size: 14)
            iconView.layer.cornerRadius = iconView.frame.size.width / 3
            NSLayoutConstraint.activate([
                iconView.heightAnchor.constraint(equalToConstant: 80),
                iconView.widthAnchor.constraint(equalToConstant: 80),
                titleLabel.heightAnchor.constraint(equalToConstant: 80),
                titleLabel.widthAnchor.constraint(equalToConstant: 180),
                subscriptionsView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 30),
                confirmationButton.topAnchor.constraint(equalTo: subscriptionsView.bottomAnchor, constant: 30)
            ])
        } else {
            titleLabel.font = UIFont(name: "TeXGyreAdventor-Bold", size: 32)
            descriptionLabel.font = UIFont(name: "TeXGyreAdventor-Bold", size: 14)
            iconView.layer.cornerRadius = iconView.frame.size.width / 2
            NSLayoutConstraint.activate([
                iconView.heightAnchor.constraint(equalToConstant: 110),
                iconView.widthAnchor.constraint(equalToConstant: 110),
                titleLabel.heightAnchor.constraint(equalToConstant: 100),
                titleLabel.widthAnchor.constraint(equalToConstant: 280),
                subscriptionsView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 60),
                subscriptionsView.heightAnchor.constraint(equalToConstant: 210),
            ])
        }
    }
    
    private func loadDarkTheme() {
        let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
        view.backgroundColor = userCurretChoiceIs ? darkGrey : white
        iconView.backgroundColor = userCurretChoiceIs ? darkGrey : .clear
        closeButton.setImage(UIImage(named: userCurretChoiceIs ? "closeGray" : "closeWhiteGray"), for: .normal)
        titleLabel.textColor = userCurretChoiceIs ? white : black
        descriptionLabel.textColor = userCurretChoiceIs ? white : black
        closeButton.setTitleColor(userCurretChoiceIs ? white20 : black, for: .normal)
        activityIndicator.color = userCurretChoiceIs ? azure : black85
    }
    
    private func makeConstraints() {
        NSLayoutConstraint.activate([
            
            iconView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            iconView.topAnchor.constraint(equalTo: view.topAnchor, constant: 47),
            
            closeButton.heightAnchor.constraint(equalToConstant: 32),
            closeButton.widthAnchor.constraint(equalToConstant: 32),
            closeButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            closeButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 47),
            
            titleLabel.topAnchor.constraint(equalTo: iconView.bottomAnchor),
            titleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            subscriptionsView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            subscriptionsView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            
            confirmationButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            confirmationButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            confirmationButton.heightAnchor.constraint(equalToConstant: 56),
            
            descriptionLabel.topAnchor.constraint(equalTo: confirmationButton.bottomAnchor, constant: 19),
            descriptionLabel.heightAnchor.constraint(equalToConstant: 25),
            descriptionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            descriptionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            
            privacyButton.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 10),
            privacyButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            privacyButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            privacyButton.heightAnchor.constraint(equalToConstant: 25),
            privacyButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -36),
            
            activityIndicator.widthAnchor.constraint(equalToConstant: 56),
            activityIndicator.heightAnchor.constraint(equalToConstant: 56),
            activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    
    @objc private func readPrivacy() {
        let dvc = PrivacyVC()
        dvc.modalPresentationStyle = .fullScreen
        present(dvc, animated: true, completion: nil)
    }
    
    @objc private func closeVC() {
        dismiss(animated: true)
    }
    
    @objc private func subscriptionAdd() {
        
        let product = IAPManager.shared.products?.first { $0.productIdentifier == subscriptionsView.currentProductId }
        
        guard let productValue = product else { return }
        
        //Сюда можно добавить старт активитиИндикатора
        
        self.activityIndicator.startAnimating()
        
        IAPManager.shared.purchaseProduct(product: productValue, success: {
            //Здесь останавливаем активити

            self.activityIndicator.stopAnimating()
            self.dismiss(animated: true, completion: nil)
            
            let alert = UIAlertController(title: nil, message: subscriptionsDone, preferredStyle: .alert)
            UIView.animate(withDuration: Animation.duration07, delay: Animation.delay05, animations: {
                self.confirmationButton.isHidden = true
                self.subscriptionsView.isUserInteractionEnabled = false
            })
            let okAction = UIAlertAction(title: "ОК", style: .destructive, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
            print("Успешно")
        }) { error in
            //Здесь останавливаем активити
            self.activityIndicator.stopAnimating()
            self.dismiss(animated: true, completion: nil)
            
            let alert = UIAlertController(title: nil, message: "Error: \(String(describing: error?.localizedDescription))".localized(), preferredStyle: .alert)
            let okAction = UIAlertAction(title: "ОК", style: .destructive, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
}
