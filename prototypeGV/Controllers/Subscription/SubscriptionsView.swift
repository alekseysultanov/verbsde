//
//  SubscriptionsView.swift
//  prototypeGV
//
//  Created by Vladimir Ereskin on 8/13/20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

struct Product {
    let periodText: String
    let periodValue: Int
    let priceValue: Double
    let priceSimbol: String
    let trialPeriod: String
    let productId: String
}

final class SubscriptionsView: UIView {
    
    var currentProductId = ""
    
    private let month1View = SubscriptionView()
    private let month12View = SubscriptionView()
    private let year1View = SubscriptionView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupDataForViews(products: [Product]) {
        for index in 0..<products.count {
            let price = products[index].priceValue
            let value = (String(format: "%.2f", price))
            switch index {
            case 0:
                month1View.periodValueLabel.text = "\(products[index].periodValue)"
                month1View.periodTextLabel.text = products[index].periodText.localized()
                month1View.priceLabel.text = "\(value)" + " " + products[index].priceSimbol
                month1View.trialLabel.text = "\(products[index].priceSimbol)" + "" + "2.99 billed\nmonthly"//products[index].trialPeriod.localized()
                month1View.productId = products[index].productId
                month1View.topLabelView.alpha = 0
                month1View.backgroundLabelView.alpha = 0
            case 1:
                month12View.periodValueLabel.text = "12"//"\(products[index].periodValue)"
                month12View.periodTextLabel.text = monthsPeriod//products[index].periodText.localized()
                month12View.priceLabel.text =  "\(products[index].priceSimbol)" + "" + "1.25/mo"//"\(value)" + " " + products[index].priceSimbol
                month12View.trialLabel.text = "\(products[index].priceSimbol)" + "" + "14.99 billed\nannually after\n7-day trial" //products[index].trialPeriod.localized()
                month12View.productId = products[index].productId
                month12View.topLabel.text = freeTrial
            case 2:
                year1View.periodValueLabel.text = "\(products[index].periodValue)"
                year1View.periodTextLabel.text = products[index].periodText.localized()
                year1View.priceLabel.text = "\(products[index].priceSimbol)" + "" + "0.84/mo"
                year1View.trialLabel.text = "\(products[index].priceSimbol)" + "" + "9.99 billed\nannually" //products[index].trialPeriod.localized()
                year1View.productId = products[index].productId
                year1View.topLabel.text = "Save 80%"
        
            default:
                print("Лишний продукт )")
            }
        }
    }
    
    func updateView(with view: SubscriptionView) {
        currentProductId = view.productId
        if view === month1View {
            view.updateView(isActive: true)
            month12View.updateView(isActive: false)
            year1View.updateView(isActive: false)
        }
        
        if view === month12View {
            view.updateView(isActive: true)
            month1View.updateView(isActive: false)
            year1View.updateView(isActive: false)
        }
        
        if view === year1View {
            view.updateView(isActive: true)
            month1View.updateView(isActive: false)
            month12View.updateView(isActive: false)
        }
    }
    
    func deactivateView(is value: Bool) {
        if value {
            month12View.updateView(isActive: value)
            month1View.updateView(isActive: false)
            year1View.updateView(isActive: false)
        } else {
            month1View.updateView(isActive: value)
            month12View.updateView(isActive: value)
            year1View.updateView(isActive: value)
        }
    }
    
    private func setupView() {
        translatesAutoresizingMaskIntoConstraints = false
        month1View.delegate = self
        month12View.delegate = self
        year1View.delegate = self
        
        month1View.updateView(isActive: false)
        month12View.updateView(isActive: true)
        year1View.updateView(isActive: false)
        
        addSubview(month1View)
        addSubview(month12View)
        addSubview(year1View)
    }
    
    private func makeConstraints() {
        NSLayoutConstraint.activate([
            month1View.topAnchor.constraint(equalTo: topAnchor),
            month1View.leadingAnchor.constraint(equalTo: leadingAnchor),
            month1View.bottomAnchor.constraint(equalTo: bottomAnchor),
            month1View.widthAnchor.constraint(equalToConstant: (UIScreen.main.bounds.width - 40 - 34) / 3),
            
            month12View.topAnchor.constraint(equalTo: topAnchor),
            month12View.leadingAnchor.constraint(equalTo: month1View.trailingAnchor, constant: 17),
            month12View.bottomAnchor.constraint(equalTo: bottomAnchor),
            month12View.widthAnchor.constraint(equalToConstant: (UIScreen.main.bounds.width - 40 - 34) / 3),
            
            year1View.topAnchor.constraint(equalTo: topAnchor),
            year1View.leadingAnchor.constraint(equalTo: month12View.trailingAnchor, constant: 17),
            year1View.trailingAnchor.constraint(equalTo: trailingAnchor),
            year1View.bottomAnchor.constraint(equalTo: bottomAnchor),
            year1View.widthAnchor.constraint(equalToConstant: (UIScreen.main.bounds.width - 40 - 34) / 3)
        ])
    }
}
