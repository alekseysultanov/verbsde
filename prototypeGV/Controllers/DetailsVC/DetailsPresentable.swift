//
//  DetailsVCPresentable.swift
//  prototypeGV
//
//  Created by admin on 28.06.20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import Foundation

extension DetailsVC: UserPresentable {
    
    func addSubView() {
        createStack()
        view.addSubview(commonStack)
        view.addSubview(tableView)
    }
    
    func makeConstraints() {
        commonStack.translatesAutoresizingMaskIntoConstraints = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        viewUnderTable.translatesAutoresizingMaskIntoConstraints = false
        commonStack.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        commonStack.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 27).isActive = true
        commonStack.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        commonStackHeightWithCasus = commonStack.heightAnchor.constraint(equalToConstant: 222)
        commonStackHeightWithoutCasus = commonStack.heightAnchor.constraint(equalToConstant: 148)
        
        tableView.topAnchor.constraint(equalTo: commonStack.bottomAnchor, constant: 0).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
    }
}
