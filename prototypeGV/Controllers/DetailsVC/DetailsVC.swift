//
//  DetailsVC.swift
//  prototypeGV
//
//  Created by admin on 27.12.19.
//  Copyright © 2019 Max Polinkovsky. All rights reserved.
//

import UIKit

class DetailsVC: UIViewController {
    //    MARK: titleLabel
    
    var currentVerb: Verb!
    var translatesArray: [String] = []
    var commonStackHeightWithCasus: NSLayoutConstraint!
    var commonStackHeightWithoutCasus: NSLayoutConstraint!
    var bgView = UIView() //цвет подложки tableView
    
    private lazy var cancelButton: UIBarButtonItem = {
        let button = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(cancel))
        return button
    }()
    
    private lazy var prateritumLine: UIImageView = {
        let image = UIImageView(image: UIImage(named: "line"))
        image.heightAnchor.constraint(equalToConstant: 1).isActive = true
        return image
    }()
    
    private lazy var perfectLine: UIImageView = {
        let image = UIImageView(image: UIImage(named: "line"))
        image.heightAnchor.constraint(equalToConstant: 1).isActive = true
        return image
    }()
    
    private lazy var stackPerfect: UIStackView = {
        let stack = UIStackView()
        stack.spacing = 0
        stack.axis = .vertical
        stack.distribution = .fillProportionally
        stack.alignment = .fill
        stack.contentMode = .center
        stack.heightAnchor.constraint(equalToConstant: 74).isActive = true
        return stack
    }()
    
    private lazy var stackPrateritum: UIStackView = {
        let stack = UIStackView()
        stack.spacing = 0
        stack.axis = .vertical
        stack.distribution = .fillProportionally
        stack.alignment = .fill
        stack.contentMode = .center
        stack.heightAnchor.constraint(equalToConstant: 74).isActive = true
        return stack
    }()
    
    private lazy var stackCasus: UIStackView = {
        let stack = UIStackView()
        stack.spacing = 0
        stack.axis = .vertical
        stack.distribution = .fillProportionally
        stack.alignment = .fill
        stack.contentMode = .center
        return stack
    }()
    
    private lazy var stackCasusLabels: UIStackView = {
        let stack = UIStackView()
        stack.spacing = 52
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.alignment = .fill
        stack.contentMode = .scaleToFill
        stack.heightAnchor.constraint(equalToConstant: 74).isActive = true
        return stack
    }()
    
    lazy var commonStack: UIStackView = {
        let stack = UIStackView()
        stack.spacing = 0
        stack.axis = .vertical
        stack.distribution = .fillProportionally
        stack.alignment = .leading
        stack.contentMode = .scaleToFill
        return stack
    }()
    
    lazy var perfectTitle: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 14)
        label.alpha = 0.5
        label.text = perfect
        return label
    }()
    
    private lazy var perfectLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 20)
        return label
    }()
    
    private lazy var preteriteTitle: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 14)
        label.alpha = 0.5
        label.text = preterite
        return label
    }()
    
    private lazy var preteriteLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 20)
        return label
    }()
    
    private lazy var casusTitle: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 14)
        label.heightAnchor.constraint(equalToConstant: 37).isActive = true
        label.alpha = 0.5
        label.text = casus
        return label
    }()
    
    private lazy var casusLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 20)
        return label
    }()
    
    private lazy var prepositionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 20)
        return label
    }()
    
    lazy var viewUnderTable: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: UITableView.Style.plain)
        tableView.separatorStyle = .singleLine
        tableView.estimatedRowHeight = 54
        tableView.rowHeight = UITableView.automaticDimension
        tableView.showsVerticalScrollIndicator = false
        tableView.dataSource = self
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        tableView.backgroundView = bgView
        tableView.tableFooterView = UIView()
        setup()
        makeNavigationBar()
        setupUI()
        loadDarkTheme()
    }
    
    func setupUI() {
        guard let verb = currentVerb else { return }
        preteriteLabel.text = verb.prateritum
        perfectLabel.text = "\(verb.hilfsVerb) \(verb.perfect)"
        translatesArray = Array(verb.getTranslate())
        casusLabel.text = "\(verb.casus) \(verb.preposition)"
//        if verb.casus == Casus.empty.description || verb.preposition == Preposition.empty.description  {
//            stackCasus.heightAnchor.constraint(equalToConstant: 0).isActive = true
//            commonStackHeightWithCasus.isActive = false
//            commonStackHeightWithoutCasus.isActive = true
//        } else {
            commonStackHeightWithCasus.isActive = true
            commonStackHeightWithoutCasus.isActive = false
//        }
        makeTableHeaderView()
    }
    
    private func makeNavigationBar() {
        navigationItem.leftBarButtonItem = cancelButton
        title = currentVerb.infinitiv
    }
    
    func createStack() {
        commonStack.addArrangedSubview(stackPrateritum)
        commonStack.addArrangedSubview(stackPerfect)
        commonStack.addArrangedSubview(stackCasus)
        stackPrateritum.addArrangedSubview(preteriteLabel)
        stackPrateritum.addArrangedSubview(preteriteTitle)
        stackPrateritum.addArrangedSubview(prateritumLine)
        stackPerfect.addArrangedSubview(perfectLabel)
        stackPerfect.addArrangedSubview(perfectTitle)
        stackPerfect.addArrangedSubview(perfectLine)
        stackCasusLabels.addArrangedSubview(casusLabel)
        stackCasusLabels.addArrangedSubview(prepositionLabel)
        stackCasus.addArrangedSubview(stackCasusLabels)
        stackCasus.addArrangedSubview(casusTitle)
    }
    
    @objc func cancel() {
        navigationController?.popViewController(animated: true)
    }
   
    func loadDarkTheme(){
        let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
        view.backgroundColor = userCurretChoiceIs ? darkGrey : white
        tableView.separatorColor  = userCurretChoiceIs ? black : .gray
        bgView.backgroundColor = userCurretChoiceIs ? darkGrey : veryLightPinkTwo15
        cancelButton.tintColor = userCurretChoiceIs ? white : black
        perfectTitle.textColor = userCurretChoiceIs ? white : black85
        perfectLabel.textColor = userCurretChoiceIs ? white : black85
        preteriteLabel.textColor = userCurretChoiceIs ? white : black85
        preteriteTitle.textColor = userCurretChoiceIs ? white : black85
        casusLabel.textColor = userCurretChoiceIs ? white : black85
        casusTitle.textColor = userCurretChoiceIs ? white : black85
        prepositionLabel.textColor = userCurretChoiceIs ? white : black85
        perfectLine.backgroundColor = userCurretChoiceIs ? black : black10
        prateritumLine.backgroundColor = userCurretChoiceIs ? black : black10
    }
}

