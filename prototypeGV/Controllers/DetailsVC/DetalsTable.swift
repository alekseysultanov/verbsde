//
//  DetalsTable.swift
//  prototypeGV
//
//  Created by admin on 28.06.20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

extension DetailsVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return translatesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
        cell.textLabel?.text = translatesArray[indexPath.row].delSpaces()
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        cell.textLabel?.textAlignment = .left
        cell.textLabel!.translatesAutoresizingMaskIntoConstraints = false
        cell.textLabel?.centerYAnchor.constraint(equalTo: cell.centerYAnchor).isActive = true
        cell.textLabel?.leftAnchor.constraint(equalTo: cell.leftAnchor, constant: 27).isActive = true
        cell.textLabel?.font = UIFont(name: "TeXGyreAdventor-Bold", size: 20)
        cell.textLabel?.textColor = userCurretChoiceIs ? white : black85
        return cell
    }
    
    
    func makeTableHeaderView() {
        let headerTableView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 60))
        let label = UILabel()
        let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = userCurretChoiceIs ? white : black85
        label.text = translates
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 14)
        tableView.tableHeaderView = headerTableView
        headerTableView.addSubview(label)
        tableView.isScrollEnabled = false
        label.heightAnchor.constraint(equalToConstant: 22).isActive = true
        label.centerYAnchor.constraint(equalTo: headerTableView.centerYAnchor).isActive = true
        label.leftAnchor.constraint(equalTo: headerTableView.leftAnchor, constant: 27).isActive = true
        tableView.topAnchor.constraint(equalTo: headerTableView.bottomAnchor, constant: 0).isActive = true
        tableView.tableHeaderView = headerTableView
        tableView.rowHeight = 54
    }
}
