//
//  BageView.swift
//  prototypeGV
//
//  Created by Vladimir Ereskin on 8/2/20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

final class BageView: UIView {
    
    let countLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
    }
    
    private func setupView() {
        translatesAutoresizingMaskIntoConstraints = false
        countLabel.translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = white
        countLabel.textAlignment = .center
        
        addSubview(countLabel)
    }
    
    private func makeConstraints() {
        NSLayoutConstraint.activate([
            countLabel.topAnchor.constraint(equalTo: topAnchor),
            countLabel.leftAnchor.constraint(equalTo: leftAnchor),
            countLabel.rightAnchor.constraint(equalTo: rightAnchor),
            countLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
