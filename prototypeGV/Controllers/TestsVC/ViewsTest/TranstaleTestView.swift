//
//  TranstaleTestView.swift
//  prototypeGV
//
//  Created by  Artem Valeryevich on 03.02.2020.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

class TranstaleTestView: UIView, TestContentView {
    
    weak var delegate: TestVC?

    /// Массив правильных ответов
    private var accurateAnswer: [String] = []
    
    /// Счетчик для Brainstorm
    private var countBrain = 0
    
    private  let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
    /// Слово для ответа
    private var verb = Verb() {
        didSet {
            switch Bool.random() {
            case true:
                accurateAnswer = Array(verb.getTranslate())
                askedVerb.text = verb.infinitiv
            case false:
                accurateAnswer = [verb.infinitiv]
                askedVerb.text = verb.getTranslate().randomElement()
            }
        }
    }
    
    /// Текст слова, которое нужно перевести
    private let askedVerb: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 32)
        label.backgroundColor = .clear
        //        label.textColor = black
        label.alpha = 0
        label.text = ""
        return label
    }()
    
    /// Количество отвеченных слов из общего числа слов
    private let countLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        label.textColor = black
        label.textAlignment = .center
        return label
    }()
    
    /// Правильный перевод слова
    private let trueAnswerLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        label.textAlignment = .center
        label.alpha = 0
        return label
    }()
    
    /// Поле для ввода перевода
    private lazy var answerTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
//        textField.textColor = black
        textField.textAlignment = .center
        textField.borderStyle = .none
        textField.delegate = self
        textField.alpha = 0
        textField.returnKeyType =  .next
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        textField.smartInsertDeleteType = .no
        textField.spellCheckingType = .no
        textField.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        textField.text = ""
        textField.clearButtonMode = .whileEditing
        textField.clearsOnBeginEditing = true
        textField.layer.cornerRadius = 12
        textField.layer.masksToBounds = true
        return textField
    }()
    
    private lazy var goNextButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(goNext, for: .normal)
        button.titleLabel!.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        button.setTitleColor(azure, for: .normal)
        button.setImage(UIImage(named: "back3"), for: .normal)
        button.backgroundColor = .clear
        button.semanticContentAttribute = .forceRightToLeft
        button.addTarget(self, action: #selector(updateViewNextVerb), for: .touchUpInside)
        button.isHidden = false
        return button
    }()
    
    private lazy var iForgotButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(iForgotAnswer, for: .normal)
        button.titleLabel!.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        button.setTitleColor(azure, for: .normal)
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(iForgotTap), for: .touchUpInside)
        button.isHidden = false
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        //        backgroundColor = veryLightPinkTwo15
        setup()
        loadDarkTheme()
    }
    
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    // MARK: - TestContentViewProtocol methods
    
    func saveMiddleProgress(_ verb: Verb, answer: Bool) {
        delegate?.saveMiddleProgress(verb, answer: answer)
    }
    
    func showResultVC() {
        delegate?.showResultVC(withNextButton: false)
    }
    
    @objc func updateViewNextVerb() {
        goNextButton.isHidden = true
        iForgotButton.isHidden = false
        goNextButton.adjustsImageWhenHighlighted = false
        self.trueAnswerLabel.alpha = 0
        
        if let testType = delegate?.testType, testType == .brainstorm, countBrain == 1 {
            delegate?.changeTestView()
        } else if let rightVerb = delegate?.getNewVerb() {
            countBrain = 1
            answerTextField.text = ""
            verb = rightVerb
            countLabel.text = delegate?.getCountAnswers()
            animationIn()
        } else {
            animationOut()
        }
    }
    
    func continueRepeatTest() {
        updateViewNextVerb()
    }
    
    // MARK: - User methods
    
    private func animationIn() {
        
        UIView.animate(withDuration: Animation.duration02, animations: {
            self.answerTextField.alpha = 1
            self.askedVerb.alpha = 1
            self.answerTextField.backgroundColor = self.userCurretChoiceIs ? charcoalGrey : black10
            self.answerTextField.textColor = self.userCurretChoiceIs ? white : black
        })
    }
    
    private func animationOut() {
        UIView.animate(withDuration: Animation.duration12, animations: {
            self.answerTextField.alpha = 0
            self.askedVerb.alpha = 0
        }) { (_) in
            self.answerTextField.alpha = 1
            self.askedVerb.alpha = 1
            self.showResultVC()
        }
    }
    
    @objc func answerTextFieldCheck() {
        guard let userAnswer = answerTextField.text?.lowercased() else { return }
        trueAnswerLabel.text = accurateAnswer.first ?? ""
        UIView.animate(withDuration: Animation.duration04, animations: {
            if self.accurateAnswer.contains(userAnswer) {
                self.saveMiddleProgress(self.verb, answer: true)
                self.answerTextField.backgroundColor = darkLimeGreen
                self.answerTextField.textColor = white
            } else {
                self.saveMiddleProgress(self.verb, answer: false)
                self.answerTextField.backgroundColor = vermillion
                self.answerTextField.textColor = white
                
                UIView.animate(withDuration: Animation.duration04, animations: {
                    self.trueAnswerLabel.alpha = 1
                })
            }
        }, completion: { _ in
            self.goNextButton.isHidden = false
            self.iForgotButton.isHidden = true
        })
    }
    
    @objc func iForgotTap( ) {
        UIView.animate(withDuration: Animation.duration04, animations: {
            self.saveMiddleProgress(self.verb, answer: false)
            self.answerTextField.text = self.accurateAnswer.first ?? ""
            self.answerTextField.backgroundColor = vermillion
            self.answerTextField.textColor = white
        }, completion: { _ in
            self.iForgotButton.isHidden = true
            self.goNextButton.isHidden = false
        })
    }
    
    private func loadDarkTheme(){
        backgroundColor = userCurretChoiceIs ? black : veryLightPinkTwo15
        askedVerb.textColor = userCurretChoiceIs ? white : black
        countLabel.textColor = userCurretChoiceIs ? white : black
        trueAnswerLabel.textColor  = userCurretChoiceIs ? white : battleshipGrey
    }
}

extension TranstaleTestView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        answerTextFieldCheck()
        return true
    }
}

extension TranstaleTestView: UserPresentable {
    func addSubView() {
        addSubview(countLabel)
        addSubview(askedVerb)
        addSubview(trueAnswerLabel)
        addSubview(answerTextField)
        addSubview(iForgotButton)
        addSubview(goNextButton)
    }
    
    func makeConstraints() {
        NSLayoutConstraint.activate([
            countLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            countLabel.topAnchor.constraint(equalTo: topAnchor, constant: 128),
            countLabel.heightAnchor.constraint(equalToConstant: 25),
            
            askedVerb.centerXAnchor.constraint(equalTo: centerXAnchor),
            askedVerb.topAnchor.constraint(equalTo: topAnchor, constant: 168),
            
            trueAnswerLabel.topAnchor.constraint(equalTo: askedVerb.bottomAnchor, constant: 10),
            trueAnswerLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            trueAnswerLabel.heightAnchor.constraint(equalToConstant: 27),
            
            answerTextField.centerYAnchor.constraint(equalTo: centerYAnchor),
            answerTextField.centerXAnchor.constraint(equalTo: centerXAnchor),
            answerTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            answerTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            answerTextField.heightAnchor.constraint(equalToConstant: 56),
            
            goNextButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -63),
            goNextButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
            goNextButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
            goNextButton.heightAnchor.constraint(equalToConstant: 27),
            
            iForgotButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -63),
            iForgotButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
            iForgotButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
            iForgotButton.heightAnchor.constraint(equalToConstant: 27)
        ])
    }
}
