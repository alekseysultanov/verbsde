
import UIKit

/// Вью теста Options
class OptionsTestView: UIView, TestContentView {
    
    weak var delegate: TestVC?
    
    /// Правильное слово
    private var rightVerb = Verb()
    
    /// Тип отображения infinitiv/translate
    private var isWordType = false
    
    /// Счетчик для Brainstorm
    private var countBrain = 0
    
    /// Кнопки выбора слова
    private var answerButtons: [AnswerButton] = (1...3).map{ _ in AnswerButton() }
    private let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
    /// Стэк кнопок для ответа
    lazy var stackButtons: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.spacing = 10
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.alignment = .fill
        stack.contentMode = .scaleToFill
        return stack
    }()
    
    /// Отображаемое слово
    lazy var askedVerbLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 32)
        label.backgroundColor = .clear
        label.text = ""
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    /// Количество отработанных слов
    lazy var countLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        label.textAlignment = .center
        return label
    }()
    
    private lazy var goNextButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(goNext, for: .normal)
        button.titleLabel!.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        button.setTitleColor(azure, for: .normal)
        button.setImage(UIImage(named: "back3"), for: .normal)
        button.backgroundColor = .clear
        button.semanticContentAttribute = .forceRightToLeft
        button.addTarget(self, action: #selector(updateViewNextVerb), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    private lazy var iForgotButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(iForgotAnswer, for: .normal)
        button.titleLabel!.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        button.setTitleColor(azure, for: .normal)
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(iForgotTap), for: .touchUpInside)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        setup()
        loadDarkTheme()
    }
    
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    func saveMiddleProgress(_ verb: Verb, answer: Bool) {
        delegate?.saveMiddleProgress(rightVerb, answer: answer)
    }
    
    func showResultVC() {
        delegate?.showResultVC(withNextButton: false)
    }
    
    @objc func updateViewNextVerb() {
        answerButtonsEnable(is: true)
        goNextButton.isHidden = true
        iForgotButton.isHidden = false
        goNextButton.adjustsImageWhenHighlighted = false
        self.iForgotButton.isEnabled = true
        
        if let testType = delegate?.testType, testType == .brainstorm, countBrain == 1 {
            delegate?.changeTestView()
        } else if let rightVerb = delegate?.getNewVerb() {
            countBrain = 1
            isWordType = Bool.random()
            self.rightVerb = rightVerb
            updateAnswerButton()
            updateLabel()
        } else {
            showResultVC()
        }
    }
    
    func continueRepeatTest() {
        updateViewNextVerb()
    }
    
    /// Настройка кнопок для выбора слов
    private func setupAnswerButtonStack() {
        answerButtons.shuffle()
        answerButtons.forEach {
            $0.backgroundColor = userCurretChoiceIs ? charcoalGrey : black10
            $0.setTitleColor(userCurretChoiceIs ? white : black85, for: .normal)
            $0.layer.cornerRadius = 12
            $0.titleLabel?.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
            $0.addTarget(self, action: #selector(answerTap), for: .touchUpInside)
            stackButtons.addArrangedSubview($0)
        }
    }
    
    /// Обновление информации на кнопках выбора слов
    private func updateAnswerButton() {
        for index in 0..<answerButtons.count {
            answerButtons[index].setTitle(" ", for: .normal)
            answerButtons[index].setImage(UIImage(named: ""), for: .normal)
        }
        
        setupDataFor(answerButton: answerButtons.first, verbs: [rightVerb])
        guard let wrongVerbsArray = delegate?.getRandomVerbs(count: answerButtons.count, except: rightVerb) else { return }
        for index in 1..<answerButtons.count {
            setupDataFor(answerButton: answerButtons[index],
                         verbs: wrongVerbsArray)
        }
        
        setupAnswerButtonStack()
    }
    
    /// Установка значений для кнопок ответа
    /// - Parameters:
    ///   - answerButton: кнопка ответа
    ///   - verb: слово для которого она конфигурируется
    private func setupDataFor(answerButton: AnswerButton?, verbs: [Verb]) {
        guard let answerButton = answerButton else { return }
        
        if isWordType {
            var cycle = true
            var infinitiv = ""
            var verb = Verb()
            
            while cycle {
                verb = verbs.randomElement() ?? Verb()
                infinitiv = verb.infinitiv
                
                var contains = false
                for index in 0..<answerButtons.count {
                    if answerButtons[index].titleLabel?.text == infinitiv {
                        contains = true
                    }
                }
                
                if !contains {
                    cycle = false
                }
            }
            
            answerButton.verb = verb
            answerButton.setTitle(answerButton.verb.infinitiv, for: .normal)
        } else {
            var cycle = true
            var translate = ""
            var verb = Verb()
            
            while cycle {
                verb = verbs.randomElement() ?? Verb()
                translate = verb.getTranslate().randomElement() ?? ""
                
                var contains = false
                for index in 0..<answerButtons.count {
                    if answerButtons[index].titleLabel?.text == translate {
                        contains = true
                    }
                }
                
                if !contains {
                    cycle = false
                }
            }
            
            answerButton.verb = verb
            answerButton.setTitle(translate, for: .normal)
        }
    }
    
    /// Обновление информации на метках
    private func updateLabel() {
        askedVerbLabel.text = isWordType ? rightVerb.getTranslate().randomElement() ?? "" : rightVerb.infinitiv
        countLabel.text = delegate?.getCountAnswers()
    }
    
    private func answerButtonsEnable(is value: Bool) {
        answerButtons.forEach { button in
            button.isEnabled = value
        }
    }
    
    @objc func iForgotTap(_ sender: AnswerButton) {
        self.saveMiddleProgress(rightVerb, answer: false)
        self.iForgotButton.isEnabled = false
        UIView.animate(withDuration: Animation.duration04, delay: Animation.delay03, animations: {
        }) { [weak self] (_) in
            UIView.animate(withDuration: Animation.duration04, animations: {
                guard let anwerButtons = self?.answerButtons.enumerated() else { return }
                for idx in anwerButtons {
                    if idx.element.verb == self?.rightVerb {
                        idx.element.setTitleColor(self!.userCurretChoiceIs ? white : white, for: .normal)
                        idx.element.backgroundColor = darkLimeGreen
                    }
                }
            }) { [weak self] (_) in
                self?.iForgotButton.isHidden = true
                self?.goNextButton.isHidden = false
            }
        }
    }
    
    /// Нажатие на одну из кнопок выбора слова
    /// - Parameter sender: нажатая кнопка
    @objc func answerTap(_ sender: AnswerButton) {
        answerButtonsEnable(is: false)
        if checkVerb(rightVerb: rightVerb, answerString: sender.titleLabel?.text ?? "") {
            self.saveMiddleProgress(sender.verb, answer: true)
            
            UIView.animate(withDuration: Animation.duration04, delay: Animation.delay03, animations: {
                sender.setTitleColor(white, for: .normal)
                sender.backgroundColor = darkLimeGreen
            }) { [weak self] (_) in
                self?.goNextButton.isHidden = false
                self?.iForgotButton.isHidden = true
            }
        } else {
            self.saveMiddleProgress(sender.verb, answer: false)
            
            UIView.animate(withDuration: Animation.duration04, delay: Animation.delay03, animations: {
                sender.setTitleColor(white, for: .normal)
                sender.backgroundColor = vermillion
            }) { [weak self] (_) in
                UIView.animate(withDuration: Animation.duration04, animations: {
                    guard let anwerButtons = self?.answerButtons.enumerated() else { return }
                    for idx in anwerButtons {
                        if idx.element.verb == self?.rightVerb {
                            idx.element.setTitleColor(self!.userCurretChoiceIs ? white : black85, for: .normal)
                            idx.element.backgroundColor = self!.userCurretChoiceIs ? charcoalGrey : veryLightPinkThree
                            if #available(iOS 13.0, *) {
                                idx.element.setImage(UIImage(named: "tick")?.withTintColor(self!.userCurretChoiceIs ? white : black85), for: .normal)
                            } else {
                                idx.element.setImage(UIImage(named: "tick"), for: .normal)
                            }
                            idx.element.semanticContentAttribute = .forceRightToLeft
                            idx.element.contentHorizontalAlignment = .center
                            idx.element.contentMode = .right
                            idx.element.imageEdgeInsets.left = 20
                        }
                    }
                }) { [weak self] (_) in
                    self?.goNextButton.isHidden = false
                    self?.iForgotButton.isHidden = true
                    
                }
            }
        }
    }
    
    private func checkVerb(rightVerb: Verb, answerString: String) -> Bool {
        var value = false
        if !isWordType {
            if rightVerb.getTranslate().contains(answerString) {
                value = true
            }
        } else {
            value = answerString == rightVerb.infinitiv
        }
        return value
    }
    
    private func loadDarkTheme(){
        backgroundColor = userCurretChoiceIs ? black : veryLightPinkTwo15
        askedVerbLabel.textColor = userCurretChoiceIs ? white : black
        countLabel.textColor = userCurretChoiceIs ? white : black
    }
}

extension OptionsTestView: UserPresentable {
    func addSubView() {
        addSubview(countLabel)
        addSubview(askedVerbLabel)
        addSubview(stackButtons)
        addSubview(iForgotButton)
        addSubview(goNextButton)
    }
    
    func makeConstraints() {
        NSLayoutConstraint.activate([
            countLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            countLabel.topAnchor.constraint(equalTo: topAnchor, constant: 128),
            countLabel.heightAnchor.constraint(equalToConstant: 25),
            
            askedVerbLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            askedVerbLabel.topAnchor.constraint(equalTo: countLabel.bottomAnchor, constant: 40),
            
            stackButtons.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -120),
            stackButtons.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
            stackButtons.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
            stackButtons.heightAnchor.constraint(equalToConstant: 188),
            
            goNextButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -63),
            goNextButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
            goNextButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
            goNextButton.heightAnchor.constraint(equalToConstant: 27),
            
            iForgotButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -63),
            iForgotButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
            iForgotButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
            iForgotButton.heightAnchor.constraint(equalToConstant: 27)
        ])
    }
}
