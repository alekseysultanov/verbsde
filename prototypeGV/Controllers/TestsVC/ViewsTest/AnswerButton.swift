//
//  AnswerButton.swift
//  prototypeGV
//
//  Created by Vladimir Ereskin on 3/14/20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

/// Кнопка для ответа в тесте Options
class AnswerButton: UIButton {
    var verb = Verb()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(_ verb: Verb) {
        self.init(frame: .zero)
        self.verb = verb
    }
    
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}
