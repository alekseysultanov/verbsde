//
//  KeysTestView.swift
//  prototypeGV
//
//  Created by  Artem Valeryevich on 03.02.2020.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//


import UIKit

struct AnswerKey {
    let letter: String
    var count: Int
}

class KeysTestView: UIView, TestContentView {
    
    weak var delegate: TestVC?
    private let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
    //MARK: - Properties
    
    /// Отвечаемое слово
    private var verb = Verb() {
        didSet {
            switch Bool.random() {
            case true:
                accurateAnswer = verb.getTranslate().randomElement() ?? ""
                wordTranslateLabel.text = verb.infinitiv
            case false:
                accurateAnswer = verb.infinitiv
                wordTranslateLabel.text = verb.getTranslate().randomElement()
            }
        }
    }
    
    /// Размер кнопки буквы
    private var frameButton: CGFloat = 0
    
    /// Отступ между кнопками букв
    private var padding: CGFloat = 0//10//5
    /// Длинна кнопок
    private var widthKeySize: Int = 0
    private var heightKeySize: Int = 0
    ///высота от вопросительного слова  до keys
    private var heightConstraint: CGFloat = 0
    private var heightButtonConstraint: CGFloat = 0
    private var heightOFCollectionViewConstraint: CGFloat = 0
    private var countLabelHeightConstraint: CGFloat = 0
    
    /// Массив букв в отвечаемом слове
    var arrayLetterVerb = [AnswerKey]()
    var letterVerb: AnswerKey?
    
    /// Правильный ответ
    private var accurateAnswer = ""
    
    /// Счетчик для Brainstorm
    private var countBrain = 0
    
    /// Количество отвеченных слов из общего числа слов
    private let countLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        label.textAlignment = .center
        return label
    }()
    
    lazy var clearLastCharButton: UIButton = {
        let smallSquare = CGSize(width: 15, height: 15)
        let button = UIButton(frame: CGRect(origin: .zero, size: smallSquare))
        button.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            button.setImage(UIImage(named: "back")!.withTintColor(userCurretChoiceIs ? white : black85), for: .normal)
        } else {
            button.setImage(UIImage(named: "back"), for: .normal)
        }
        button.alpha = 1
        button.addTarget(self, action: #selector(tapBackButton), for: .touchUpInside)
        return button
    }()
    
    /// Полученный результат ответа
    lazy var resultLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
//        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 32)
        return label
    }()
    
    lazy var resultView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 12
        return view
    }()
    
    /// Перевод
    lazy var wordTranslateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 20)
        label.layer.cornerRadius = 12
        label.textAlignment = .center
        return label
    }()
    
    /// Кнопки для ответа
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: widthKeySize, height: heightKeySize)
        layout.minimumLineSpacing = padding
        layout.minimumInteritemSpacing = padding
        layout.scrollDirection = .vertical
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.showsHorizontalScrollIndicator = false
        view.register(LetterCollectionViewCell.self,
                      forCellWithReuseIdentifier: "cell")
        view.dataSource = self
        view.clipsToBounds = false
        return view
    }()
    
    lazy var goNextButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(goNext, for: .normal)
        button.titleLabel!.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        button.setTitleColor(azure, for: .normal)
        button.setImage(UIImage(named: "back3"), for: .normal)
        button.backgroundColor = .clear
        button.semanticContentAttribute = .forceRightToLeft
        button.addTarget(self, action: #selector(updateViewNextVerb), for: .touchUpInside)
        button.isHidden = false
        return button
    }()
    
    lazy var iForgotButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(iForgotAnswer, for: .normal)
        button.titleLabel!.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        button.setTitleColor(azure, for: .normal)
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(iForgotTap), for: .touchUpInside)
        button.isHidden = false
        return button
    }()
    
    //MARK: - init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        constraints()
        setup()
        loadDarkTheme()
        deleteLastChar()
    }
    
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    // MARK: - TestContentViewProtocol methods
    
    func saveMiddleProgress(_ verb: Verb, answer: Bool) {
        delegate?.saveMiddleProgress(verb, answer: answer)
    }
    
    func showResultVC() {
        delegate?.showResultVC(withNextButton: false)
    }
    
    private func constraints() {
        var heightScreen: CGFloat = 0
        var widthScreen: CGFloat = 0
        
        let bounds = UIScreen.main.bounds
        heightScreen = bounds.size.height //get the height of screen
        widthScreen = bounds.size.width //get the width of screen
        
        //once you have the sizes, you can validate them, for instance..
        
        //for iPhone 5/5s/SE
        if heightScreen == 568.0 && widthScreen == 320.0 {
            padding = 10
            heightConstraint = 31
            widthKeySize = 44
            heightKeySize = widthKeySize
            heightButtonConstraint = -43
            heightOFCollectionViewConstraint = -30
            countLabelHeightConstraint = 120
            resultLabel.font = UIFont(name: "TeXGyreAdventor-Bold", size: 28)
            
            
            // for iPhone 6/6s/7/7s
        } else if heightScreen == 667.0 && widthScreen == 375.0 {
            padding = 5
            heightConstraint = 31
            widthKeySize = 56
            heightKeySize = widthKeySize
            heightButtonConstraint = -63
            heightOFCollectionViewConstraint = -30
            countLabelHeightConstraint = 136
            resultLabel.font = UIFont(name: "TeXGyreAdventor-Bold", size: 32)
            //// for iPhone 6s Plus/ 7s Plus ...
        } else {
            padding = 10
            heightConstraint = 97
            widthKeySize = 72
            heightKeySize = 56
            heightButtonConstraint = -63
            heightOFCollectionViewConstraint = -30
            countLabelHeightConstraint = 136
            resultLabel.font = UIFont(name: "TeXGyreAdventor-Bold", size: 32)
        }
    }
    
    @objc func updateViewNextVerb() {
        goNextButton.isHidden = true
        goNextButton.adjustsImageWhenHighlighted = false
        iForgotButton.isHidden = false
        collectionView.isHidden = false
        clearLastCharButton.alpha = 1
        resultView.backgroundColor = userCurretChoiceIs ? charcoalGrey : black10
        
        if let testType = delegate?.testType, testType == .brainstorm, countBrain == 1 {
            delegate?.changeTestView()
        } else if let rightVerb = delegate?.getNewVerb() {
            countBrain = 1
            resultLabel.text = ""
            self.resultLabel.textColor = userCurretChoiceIs ? white : black
            verb = rightVerb
            creatTextToButton()
            countLabel.text = delegate?.getCountAnswers()
        } else {
            showResultVC()
        }
    }
    /// Кнопка быстрого ответа
    @objc func iForgotTap() {
        self.resultLabel.text = self.accurateAnswer
        saveMiddleProgress(verb, answer: false)
        UIView.animate(withDuration: Animation.duration04, delay: Animation.delay03, animations: {
            self.collectionView.isHidden = true
            self.resultView.backgroundColor = vermillion
            self.resultLabel.textColor = white
        }) { [weak self] (_) in
            self?.iForgotButton.isHidden = true
            self?.goNextButton.isHidden = false
            self?.clearLastCharButton.alpha = 0
        }
    }
    // удаление символа из Label
    @objc func tapBackButton() {
        deleteLastChar()
        
    }
    
    func continueRepeatTest() {
        updateViewNextVerb()
    }
    
    // MARK: - User methods
    
    /// Создание массива букв для кнопок
    private func creatTextToButton() {
        var text = [String]()
        for idx in accurateAnswer {
            text.append(idx.description)
        }
        text.shuffle()
        
        let dups = Dictionary(grouping: text, by: { $0 })
        
        arrayLetterVerb = []
        dups.forEach { (key, value) in
            arrayLetterVerb.append(AnswerKey(letter: key, count: value.count))
        }
        arrayLetterVerb.shuffle()
        
        collectionView.reloadData()
    }
    //    Удаление символа из label
    func deleteLastChar() {
        guard var result = resultLabel.text else { return }
        let last = String(result.removeLast())
        resultLabel.text = result
        
        if let indexRow = arrayLetterVerb.firstIndex(where: { $0.letter == last }), let cell = collectionView.cellForItem(at: IndexPath(row: indexRow, section: 0)) as? LetterCollectionViewCell {
            
            cell.letterView.alpha = 1
            cell.answerKey.count += 1
            cell.showBage()
        }
    }
    
    /// Анимация результата
    /// - Parameter answer: правильно или нет ответил
    func animateResult(answer: Bool) {
        saveMiddleProgress(verb, answer: answer)
        
        UIView.animate(withDuration: Animation.duration04, delay: Animation.delay03, animations: {
            if answer {
                self.resultView.backgroundColor = darkLimeGreen
                self.resultLabel.textColor = white
                self.clearLastCharButton.alpha = 0
            } else {
                self.resultView.backgroundColor = vermillion
                self.resultLabel.text = self.accurateAnswer
                self.resultLabel.textColor = white
                self.clearLastCharButton.alpha = 0
            }
        }) { [weak self] (_) in
            self?.goNextButton.isHidden = false
            self?.iForgotButton.isHidden = true
        }
    }
    
    /// Добавление буквы в результирующий Label
    /// - Parameter title: добавляемая буква
    private func resultAdd(title: String) {
        if let text = resultLabel.text {
            resultLabel.text = text + title
        } else {
            resultLabel.text = title
        }
    }
    
    private func loadDarkTheme(){
        backgroundColor = userCurretChoiceIs ? black : veryLightPinkTwo15
        countLabel.textColor = userCurretChoiceIs ? white : black
        wordTranslateLabel.textColor = userCurretChoiceIs ? white : black
        resultLabel.backgroundColor = userCurretChoiceIs ? .clear : veryLightPinkTwo15
        resultLabel.textColor = userCurretChoiceIs ? white : black
        collectionView.backgroundColor = userCurretChoiceIs ? .clear : veryLightPinkTwo15
        clearLastCharButton.tintColor = userCurretChoiceIs ? white : black
    }
}

// MARK: - UserPresentable

extension KeysTestView: UserPresentable {
    
    func addSubView() {
        addSubview(countLabel)
        addSubview(collectionView)
        addSubview(resultView)
        resultView.addSubview(resultLabel)
        addSubview(wordTranslateLabel)
        addSubview(iForgotButton)
        addSubview(goNextButton)
        addSubview(clearLastCharButton)
    }
    
    func makeConstraints() {
        NSLayoutConstraint.activate([
            
            countLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            countLabel.topAnchor.constraint(equalTo: topAnchor, constant: countLabelHeightConstraint),
            countLabel.heightAnchor.constraint(equalToConstant: 25),
            
            resultView.topAnchor.constraint(equalTo: countLabel.bottomAnchor, constant:  30),
            resultView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            resultView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            resultView.heightAnchor.constraint(equalToConstant: 56),
            
            resultLabel.topAnchor.constraint(equalTo: resultView.topAnchor, constant: 0),
            resultLabel.bottomAnchor.constraint(equalTo: resultView.bottomAnchor,constant: -6),
            resultLabel.centerXAnchor.constraint(equalTo: resultView.centerXAnchor),
            resultLabel.heightAnchor.constraint(equalToConstant: 50),
            
            wordTranslateLabel.topAnchor.constraint(equalTo: resultView.bottomAnchor, constant:  25),
            wordTranslateLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            wordTranslateLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            wordTranslateLabel.heightAnchor.constraint(equalToConstant: 40),
            
            collectionView.topAnchor.constraint(equalTo: wordTranslateLabel.bottomAnchor, constant: heightConstraint),
            collectionView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -28),
            collectionView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 28),
            collectionView.bottomAnchor.constraint(equalTo: goNextButton.topAnchor, constant: heightOFCollectionViewConstraint),
            
            goNextButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: heightButtonConstraint),
            goNextButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
            goNextButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
            goNextButton.heightAnchor.constraint(equalToConstant: 27),
            
            iForgotButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: heightButtonConstraint),
            iForgotButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
            iForgotButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
            iForgotButton.heightAnchor.constraint(equalToConstant: 27),
            
            clearLastCharButton.rightAnchor.constraint(equalTo: rightAnchor, constant:  -20),
            clearLastCharButton.centerYAnchor.constraint(equalTo: resultLabel.centerYAnchor)
        ])
    }
}

extension KeysTestView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayLetterVerb.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? LetterCollectionViewCell else { return UICollectionViewCell() }
        
        cell.delegate = self
        cell.bind(with: arrayLetterVerb[indexPath.row])
        return cell
    }
}

extension KeysTestView: LetterCollectionViewCellProtocol {
    func letterTap(text: String) {
        self.resultAdd(title: text)
        if resultLabel.text?.count == accurateAnswer.count {
            animateResult(answer: accurateAnswer == resultLabel.text )
        }
    }
}
