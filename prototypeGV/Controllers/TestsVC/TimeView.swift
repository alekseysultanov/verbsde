//
//  TimeView.swift
//  prototypeGV
//
//  Created by Vladimir Ereskin on 5/11/20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

final class TimeView: UIView {
    
    private let textLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = azure
        label.font = UIFont.boldSystemFont(ofSize: 24)
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        setupView()
        makeConstraints()
    }
    
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
    }
    
    func setText(_ text: String) {
        textLabel.text = text
    }
    
    func setColorTimeForAnswer(timeForAnswer: Int) {
        if timeForAnswer < 20 {
            textLabel.textColor = vermillion
        } else {
            textLabel.textColor = azure
        }
    }
    
    private func setupView() {
        addSubview(textLabel)
        clipsToBounds = true
    }
    
    private func makeConstraints() {
        NSLayoutConstraint.activate([
            textLabel.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            textLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            textLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            textLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)
        ])
    }
    
    func loadDarkTheme(){
        let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
        backgroundColor = userCurretChoiceIs ? veryLightPinkTwo : black10
    }
}
