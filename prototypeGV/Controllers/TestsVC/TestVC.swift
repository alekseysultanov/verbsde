//
//  TestVC.swift
//  prototypeGV
//
//  Created by Vladimir Ereskin on 3/12/20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

/// Протокол Вью отображаемого тест
protocol TestContentView: UIView {
    var delegate: TestVC? { get set }
    
    // сохранение промежуточного результата
    func saveMiddleProgress(_ verb: Verb, answer: Bool)
    
    // показать результирующий экран
    func showResultVC()
    
    /// Обновить экран для следующего слова если оно есть
    func updateViewNextVerb()
    
    /// Обновить Вью для прохождения теста сначала
    func continueRepeatTest()
}

class TestVC: UIViewController {
    
    var testType: TestType
    
    /// Массив всех слов для ответа
    private var verbArray: [Verb] = []
    
    /// Массив оставшихся слов для ответа
    private var verbOutputArray: [Verb] = []
    
    /// Массив правильно отвеченных слов
    private var rightArray: [Verb] = []
    
    /// Массив неправильно отвеченных слов
    private var wrongArray: [Verb] = []
    
    /// Тема по которой проходит тестирование
    let selectedTheme: Theme
    
    private var backView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    /// Вью для прохождения теста
    private var contentView: TestContentView = OptionsTestView()
    
    /// Следующее Вью для теста Brainstorm
    private var contentViewNext: TestContentView = OptionsTestView()
    
    /// Переворачиваем вью
    private var isFlipped: Bool = false
    
    /// Кнопка закрытия
    lazy var cancelButton: UIBarButtonItem = {
        let button = UIBarButtonItem(image: UIImage(named: "close"), style: .plain, target: self, action: #selector(cancelAction(_:)))
        return button
    }()
    
    private let timerView = TimeView()
    private var timer: Timer?
    private var timeForAnswer = 0
    
    init(verbs: [Verb], type: TestType, theme: Theme) {
        self.verbArray = verbs
        self.verbOutputArray = verbs
        self.testType = type
        self.selectedTheme = theme
        super.init(nibName: nil, bundle: nil)
        
        makePassingTestView(type: type)
    }
    
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        navigationItem.leftBarButtonItem = cancelButton
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupTimer()
        loadDarkTheme()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
    }
    
    /// Показать экран с результатами
    func showResultVC(withNextButton: Bool) {
        let resultVC = ResultsVC()
        resultVC.delegate = self
        resultVC.continueTestShow = withNextButton
        resultVC.modalPresentationStyle = .fullScreen
        present(resultVC, animated: true, completion: nil)
    }
    
    /// Сохранить в базу истрорию ответа
    /// - Parameters:
    ///   - verb: отвечаемое слово
    ///   - answer: правильно/неправильно
    func saveMiddleProgress(_ verb: Verb, answer: Bool) {
        let date = Date()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        let dateString = dateFormatter.string(from: date)
        
        answer ? rightArray.append(verb) : wrongArray.append(verb)
        
        let historyVerb = HistoryVerb(date: dateString, answer: answer)
        
        try! realm.write {
            verb.historyVerb.append(historyVerb)
        }
        
        historyVerb.saveToFirebaseStep(to: verb, fixed: selectedTheme.getThemePath())
    }
    
    /// Получение следующего слова для ответа, если оно есть
    func getNewVerb() -> Verb? {
        guard !verbOutputArray.isEmpty else { return nil }
        
        var outputArray: [Verb] = []
        
        if !getVerbsForProgressLevel(.one).isEmpty {
            outputArray = getVerbsForProgressLevel(.one)
        } else if !getVerbsForProgressLevel(.two).isEmpty {
            outputArray = getVerbsForProgressLevel(.two)
        } else if !getVerbsForProgressLevel(.three).isEmpty {
            outputArray = getVerbsForProgressLevel(.three)
        } else {
            outputArray = getVerbsForProgressLevel(.four)
        }
        
        guard let verb = outputArray.randomElement(), let index = outputArray.lastIndex(of: verb) else { return nil }
        
        return verbOutputArray.remove(at: index)
    }
    
    /// Получения случайного неправильного слова
    /// - Parameter count: количество необходимых слов
    func getRandomVerbs(count: Int, except: Verb) -> [Verb] {
        var arrayForRandom = verbArray
        var randomVerbsArray: [Verb] = []
        
        if let index = arrayForRandom.firstIndex(of: except) {
            arrayForRandom.remove(at: index)
        }
        
        guard !arrayForRandom.isEmpty else { return [] }
        
        for _ in 0..<min(arrayForRandom.count, count) {
            guard let indexRemoveVerb = (0..<arrayForRandom.count).randomElement() else { return [] }
            randomVerbsArray.append(arrayForRandom.remove(at: indexRemoveVerb))
        }
        return randomVerbsArray
    }
    
    /// Получение информации количества отработанных слов из общего числа
    func getCountAnswers() -> String {
        showMiddleResult()
        let currentAllCount = getAllCount()
        return "\(verbArray.count - verbOutputArray.count) / \(currentAllCount)"
    }
    
    func showMiddleResult() {
        let answerCount = verbArray.count - verbOutputArray.count
        if (answerCount != 1) && ((answerCount - 1) % countTestVerbs == 0) {
            showResultVC(withNextButton: true)
        }
    }
    
    /// Получить массив неправильно отвеченных слов
    func getWrongArray() -> [Verb] {
        return wrongArray
    }
    
    /// Получить массив правильно отвеченных слов
    func getRightArray() -> [Verb] {
        return rightArray
    }
    
    /// Получить весь массив слов
    func getAllVerbsArray() -> [Verb] {
        return verbArray
    }
    
    /// Количество пройденных слов
    func getProgressVerbCount() -> Int {
        return verbOutputArray.count
    }
    
    /// Обновить Вью для прохождения теста сначала
    func continueRepeatTest() {
        contentView.continueRepeatTest()
    }
    
    /// Обнулить массивы с результатами
    func clearResultArrays() {
        verbOutputArray = verbArray
        rightArray.removeAll()
        wrongArray.removeAll()
    }
    
    /// Сменить Вью теста
    func changeTestView() {
        
        isFlipped = !isFlipped
        
        if isFlipped {
            contentViewNext = randomView()
            backView.addSubview(contentViewNext)
            makeConstraintToNext(contentViewNext)
            contentViewNext.delegate = self
            contentViewNext.updateViewNextVerb()
        } else {
            contentView = randomView()
            backView.addSubview(contentView)
            makeConstraintToNext(contentView)
            contentView.delegate = self
            contentView.updateViewNextVerb()
        }
        
        
        let fromView = isFlipped ? contentView : contentViewNext
        let toView = isFlipped ? contentViewNext : contentView
        
        UIView.transition(from: fromView, to: toView, duration: 0.5, options: [.curveEaseOut, .transitionFlipFromRight, .showHideTransitionViews])
    }
    
    /// Создать вью для прохождения теста
    private func makePassingTestView(type: TestType) {
        contentViewNext.isHidden = type != .brainstorm
        switch type {
        case .keys:
            title = keys
            contentView = KeysTestView()
            contentView.delegate = self
            contentView.updateViewNextVerb()
        case .option:
            title = option
            contentView = OptionsTestView()
            contentView.delegate = self
            contentView.updateViewNextVerb()
        case .translate:
            title = translate
            contentView = TranstaleTestView()
            contentView.delegate = self
            contentView.updateViewNextVerb()
        case .brainstorm:
            title = brainstorm
            changeTestView()
        }
    }
    
    /// Получить рандомное Вью для теста Brainstorm
    /// - Returns: случайное вью теста
    private func randomView() -> TestContentView {
        let type = TestType(rawValue: Int.random(in: 0...2))
        switch type {
        case .keys:
            return KeysTestView()
        case .option:
            return OptionsTestView()
        case .translate:
            return TranstaleTestView()
        case .brainstorm:
            return KeysTestView()
        case .none:
            return OptionsTestView()
        }
    }
    
    /// Получить количество слов для отображения (xx/20(40...))
    func getAllCount() -> Int {
        let progressCount = (verbArray.count - verbOutputArray.count) / countTestVerbs + 1
        
        if (verbArray.count - verbOutputArray.count) % countTestVerbs == 0 {
            return (progressCount - 1) * countTestVerbs
        }
        
        let count = min(progressCount * countTestVerbs, verbArray.count)
        return count
    }
    
    /// Получить количество слов для отображения (xx/20(40...)) во Вью результатов теста
    func getAllCountForResult() -> Int {
        let progressCount = (verbArray.count - verbOutputArray.count) / countTestVerbs
        
        if (verbArray.count - verbOutputArray.count) % countTestVerbs == 0 {
            return (progressCount - 1) * countTestVerbs
        }
        
        let count = min(progressCount * countTestVerbs, verbArray.count)
        
        if (count + countTestVerbs) > verbArray.count {
            return verbArray.count
        }
        
        if (progressCount * countTestVerbs) == (rightArray.count + wrongArray.count) {
            return progressCount * countTestVerbs
        }
        return count + countTestVerbs
    }
    
    /// Закрытие экрана с показом результатов
    /// - Parameter sender: navigationItem
    @objc func cancelAction(_ sender: Any) {
        let answerCount = rightArray.count + wrongArray.count
        if answerCount == 0 {
            navigationController?.popViewController(animated: true)
        } else {
            showResultVC(withNextButton: false)
        }
    }
    
    private func makeConstraintToNext(_ view: UIView) {
        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: backView.topAnchor),
            view.rightAnchor.constraint(equalTo: backView.rightAnchor),
            view.leftAnchor.constraint(equalTo: backView.leftAnchor),
            view.bottomAnchor.constraint(equalTo: backView.bottomAnchor)
        ])
    }
    
    private func setupTimer() {
        if let isTimer = defaults.object(forKey: "timer") as? Bool, isTimer {
            if verbOutputArray.count >= countTestVerbs {
                timeForAnswer = secondsForAnswer * countTestVerbs
            } else {
                timeForAnswer = secondsForAnswer * verbOutputArray.count
            }
            timerView.setText(String(timeForAnswer))
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        } else {
            timerView.isHidden = true
        }
        timerView.loadDarkTheme()
    }
    
    @objc private func updateTime() {
        timeForAnswer -= 1
        timerView.setText(String(timeForAnswer))
        timerView.setColorTimeForAnswer(timeForAnswer: timeForAnswer)
        if timeForAnswer <= 20 {
            UIView.animate(withDuration: Animation.duration04,
                           delay: Animation.duration02,
                           options: [.repeat, .autoreverse],
                           animations: { self.timerView.alpha = 0 }
            )
        }
        
        if timeForAnswer == 0 {
            timer?.invalidate()
            showResultVC(withNextButton: false)
        }
    }
    
    /// Получить массив глаголов в зависимости от прогресса
    /// - Parameter level: уровень прогресса слова
    /// - Returns: массив глаголов
    private func getVerbsForProgressLevel(_ level: ProgressLevel) -> [Verb] {
        switch level {
        case .one:
            return verbOutputArray.filter { $0.getProgress() < 0.3 }
        case .two:
            return verbOutputArray.filter { $0.getProgress() >= 0.3 && $0.getProgress() < 0.7 }
        case .three:
            return verbOutputArray.filter { $0.getProgress() >= 0.7 && $0.getProgress() < 1 }
        case .four:
            return verbOutputArray.filter { $0.getProgress() >= 1 }
        }
    }
    
    func loadDarkTheme(){
        let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
        cancelButton.tintColor = userCurretChoiceIs ? white : black
    }
}

enum ProgressLevel {
    case one, two, three, four
}

extension TestVC: UserPresentable {
    func addSubView() {
        backView.addSubview(contentViewNext)
        backView.addSubview(contentView)
        view.addSubview(backView)
        view.addSubview(timerView)
    }
    
    func makeConstraints() {
        NSLayoutConstraint.activate([
            backView.topAnchor.constraint(equalTo: view.topAnchor),
            backView.rightAnchor.constraint(equalTo: view.rightAnchor),
            backView.leftAnchor.constraint(equalTo: view.leftAnchor),
            backView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            contentView.topAnchor.constraint(equalTo: backView.topAnchor),
            contentView.rightAnchor.constraint(equalTo: backView.rightAnchor),
            contentView.leftAnchor.constraint(equalTo: backView.leftAnchor),
            contentView.bottomAnchor.constraint(equalTo: backView.bottomAnchor),
            
            contentViewNext.topAnchor.constraint(equalTo: backView.topAnchor),
            contentViewNext.rightAnchor.constraint(equalTo: backView.rightAnchor),
            contentViewNext.leftAnchor.constraint(equalTo: backView.leftAnchor),
            contentViewNext.bottomAnchor.constraint(equalTo: backView.bottomAnchor),
            
            timerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            timerView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20),
            timerView.heightAnchor.constraint(equalToConstant: 50),
            timerView.widthAnchor.constraint(greaterThanOrEqualToConstant: 50)
        ])
    }
}
