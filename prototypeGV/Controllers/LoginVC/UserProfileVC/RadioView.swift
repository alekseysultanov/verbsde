//
//  RadioView.swift
//  prototypeGV
//
//  Created by Vladimir Ereskin on 8/19/20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

final class RadioView: UIControl {
    
    private enum Constants {
        static let size: CGFloat = 18
        static let dotSize: CGFloat = 7
    }
    
    let textLabel = UILabel()
    
    var isActive = false {
        didSet {
            dotView.backgroundColor = isActive ? black : white
        }
    }
    
    private let dotView = UIView()
    private let dotBorderView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        translatesAutoresizingMaskIntoConstraints = false
        dotView.translatesAutoresizingMaskIntoConstraints = false
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        dotBorderView.translatesAutoresizingMaskIntoConstraints = false
        
        textLabel.numberOfLines = 1
        
        dotBorderView.layer.cornerRadius = Constants.size / 2
        dotBorderView.layer.borderWidth = 2
        dotBorderView.layer.borderColor = black.cgColor
        dotBorderView.isUserInteractionEnabled = false
        
        dotView.layer.cornerRadius = Constants.dotSize / 2
        dotView.backgroundColor = white
        dotView.isUserInteractionEnabled = false
        
        addSubview(dotBorderView)
        dotBorderView.addSubview(dotView)
        addSubview(textLabel)
    }
    
    private func makeConstraints() {
        NSLayoutConstraint.activate([
            dotView.heightAnchor.constraint(equalToConstant: Constants.dotSize),
            dotView.widthAnchor.constraint(equalToConstant: Constants.dotSize),
            dotView.centerYAnchor.constraint(equalTo: dotBorderView.centerYAnchor),
            dotView.centerXAnchor.constraint(equalTo: dotBorderView.centerXAnchor),
            
            dotBorderView.leadingAnchor.constraint(equalTo: leadingAnchor),
            dotBorderView.topAnchor.constraint(equalTo: topAnchor),
            dotBorderView.bottomAnchor.constraint(equalTo: bottomAnchor),
            dotBorderView.heightAnchor.constraint(equalToConstant: Constants.size),
            dotBorderView.widthAnchor.constraint(equalToConstant: Constants.size),
            
            textLabel.leadingAnchor.constraint(equalTo: dotBorderView.trailingAnchor, constant: 15),
            textLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            textLabel.topAnchor.constraint(equalTo: topAnchor),
            textLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
