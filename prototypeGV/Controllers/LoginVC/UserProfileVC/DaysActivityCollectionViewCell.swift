//
//  DaysActivityCollectionViewCell.swift
//  prototypeGV
//
//  Created by Vladimir Ereskin on 3/7/20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

class DaysActivityCollectionViewCell: UICollectionViewCell {
    
    private struct Constants {
        static let progressViewWidth: CGFloat = 6
    }
    
    private let countWordLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.font = UIFont(name: "TeXGyreAdventor-Regular", size: 14)
        return label
    }()
    
    private let progressView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        view.layer.cornerRadius = Constants.progressViewWidth / 2
        view.clipsToBounds = true
        return view
    }()
    
    private let dayLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.font = UIFont(name: "TeXGyreAdventor-Regular", size: 12)
        label.textColor = .gray
        return label
    }()
    
    private var heightProgressViewConstraint: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        heightProgressViewConstraint = progressView.heightAnchor.constraint(equalToConstant: 0)
        setupViews()
        makeConstraints()
    }
    
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    func update(with model: DaysActivityCellModel) {
        countWordLabel.text = "\(model.countWord)"
        dayLabel.text = "\(model.numberDay)"
        
        var progress: Float = 0
        if model.countWord >= 10 {
            progress = 1
        } else {
            progress = Float(model.countWord) / 10.0
        }
        
        progressView.backgroundColor = ProgressColor.by(progress: progress)
        heightProgressViewConstraint.constant = CGFloat(progress * 100)
    }
    
    private func setupViews() {
        contentView.addSubview(dayLabel)
        contentView.addSubview(progressView)
        contentView.addSubview(countWordLabel)
    }
    
    private func makeConstraints() {
        NSLayoutConstraint.activate([
            heightProgressViewConstraint,
            
            countWordLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            countWordLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            
            progressView.topAnchor.constraint(equalTo: countWordLabel.bottomAnchor, constant: 16),
            progressView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            progressView.widthAnchor.constraint(equalToConstant: Constants.progressViewWidth),
            
            dayLabel.topAnchor.constraint(equalTo: progressView.bottomAnchor, constant: 16),
            dayLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            dayLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            dayLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
}
