//
//  UserProfileVC.swift
//  prototypeGV
//
//  Created by Vladimir Ereskin on 2/29/20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseStorage
import RealmSwift

//enum TypeRangeTime {
//    case week, month
//}

final class UserProfileVC: UIViewController {
    
    //MARK: - Constants
    
    private struct Constants {
        static let buttonSize: CGFloat = 32
        static let offset: CGFloat = 20
        static let iconSize: CGFloat = 80
        static let identifireCellDays: String = String(describing: DaysActivityCell.self)
        static let identifireCellWeek: String = String(describing: ThemeCell.self)
    }
    
    //MARK: - Properties
    
    private var user = UserInfo()
    
    private let imagePickerController = UIImagePickerController()
    
    private let iconImageView: UIImageView = {
        let image = UIImageView(image: UIImage(named: "account"))
        image.translatesAutoresizingMaskIntoConstraints = false
        image.isUserInteractionEnabled = true
        image.contentMode = .scaleAspectFill
        image.layer.cornerRadius = image.frame.size.width / 2
        image.clipsToBounds = true
        image.layer.cornerRadius = Constants.iconSize * 0.5
        return image
    }()
    //MARK: спрятал кнопку
    /// Кнопка выхода
    private let logoutButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "logout"), for: .normal)
        button.addTarget(self, action: #selector(logoutUser), for: .touchUpInside)
        return button
    }()
    
    /// Кнопка закрытия текущего контроллера
    private let exitButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(exitFromVC), for: .touchUpInside)
        return button
    }()
    
    private let userEmailLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 20)
        label.textAlignment = .center
        return label
    }()
    
    private lazy var timer: UISwitch = {
        let timerSwitch =  UISwitch()
        timerSwitch.translatesAutoresizingMaskIntoConstraints = false
        timerSwitch.isOn = false
        timerSwitch.set(width: 37, height: 20)
        timerSwitch.onTintColor = azure
        timerSwitch.addTarget(self, action: #selector(timerChange), for: .valueChanged)
        return timerSwitch
    }()
    
    private lazy var timerLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Test timer".localized()
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        return label
    }()
    
    private lazy var darkThemeSwitcher: UISwitch = {
        var timerSwitch =  UISwitch()
        timerSwitch.translatesAutoresizingMaskIntoConstraints = false
        timerSwitch.isOn = false
        timerSwitch.set(width: 37, height: 20)
        timerSwitch.onTintColor = azure
        timerSwitch.addTarget(self, action: #selector(getThemeChoice), for: .valueChanged)
        return timerSwitch
    }()
    
    private lazy var translateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Word translation".localized()
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        return label
    }()
    
    lazy var segmentedControl: UISegmentedControl = {
        let control = UISegmentedControl()
        control.translatesAutoresizingMaskIntoConstraints = false
        control.insertSegment(withTitle: "RU", at: 0, animated: false)
        control.insertSegment(withTitle: "EN", at: 1, animated: false)
        control.selectedSegmentIndex = 0
        control.addTarget(self, action: #selector(sortSelection), for: .valueChanged)
        
        return control
    }()
    
    private lazy var themeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Dark Theme".localized()
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        return label
    }()
    
    //    let radioButtonView = RadioButtonView()
    
    let addPurchase: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Subscribe to Easy+".localized(), for: .normal)
        button.layer.cornerRadius = 12
        button.backgroundColor = azure
        button.setTitleColor(white, for: .normal)
        button.setTitleColor(.gray, for: .highlighted)
        button.titleLabel?.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        button.addTarget(self, action: #selector(addPurchases), for: .touchUpInside)
        return button
    }()
    
    let restorePurchase: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Restore Purchase".localized(), for: .normal)
//        button.layer.cornerRadius = 12
        button.backgroundColor = .clear
        button.setTitleColor(azure, for: .normal)
        button.setTitleColor(.gray, for: .highlighted)
        button.contentHorizontalAlignment = .left
        button.titleLabel?.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        button.addTarget(self, action: #selector(restorePurchases), for: .touchUpInside)
        return button
    }()
    
    private let privacyButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(terms, for: .normal)
        button.setTitleColor(azure, for: .normal)
        button.titleLabel?.font = UIFont(name: "TeXGyreAdventor-Bold", size: 16)
        button.addTarget(self, action: #selector(readPrivacy), for: .touchUpInside)
        return button
    }()
    
    let activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.hidesWhenStopped = true
        return indicator
    }()
    
    var data: UserProfileModel?
    
    //MARK: - Life circle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setup()
        iconViewConfigurate()
        isOtherIphone()
        loadDarkTheme()
        setupLocale()
        purchasedButtonIs()
        subscriptionPurchased()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //        radioButtonView.setupLocale()
        
        DataManager.getUserInfo { [ weak self ] userInfo in
            guard let userInfo = userInfo else { return }
            self?.user = userInfo
            updateData()
        }
        
        if let isTimer = defaults.object(forKey: "timer") as? Bool {
            timer.isOn = isTimer
        }
        
        if let isDark = defaults.object(forKey: "isDarkTheme") as? Bool {
            darkThemeSwitcher.isOn = isDark
        }
    }
    
    // MARK: - User methods
    
    private func iconViewConfigurate() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(iconTap))
        iconImageView.addGestureRecognizer(tapGesture)
    }
    
    // MARK: - Upload photo
    private func upload(image: UIImage) {
        
        guard let icon = image.jpegData(compressionQuality: 0.3) else { return }
        
        DataManager.saveUserIcon(icon, userInfo: user)
    }
    
    func clearPurchases() {
        defaults.set(nil, forKey: Products.oneMonth.rawValue)
//        defaults.set(nil, forKey: Products.12Month.rawValue)
        defaults.set(nil, forKey: Products.oneYear.rawValue)
    }
    
    /// Разлогинивание пользователя
    @objc private func logoutUser() {
        do {
            try Auth.auth().signOut()
            let vc = LoginVC()
            clearPurchases()
            defaults.setValue(nil, forKey: "userUid")
            dismiss(animated: false, completion: nil)
            navigationController?.present(vc, animated: true, completion: nil)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    /// Закрытие данного экрана
    @objc private func exitFromVC() {
        dismiss(animated: true, completion: nil)
    }
    
    /// Нажание на изображение  пользователя
    @objc private func iconTap() {
        addUsersPhoto()
    }
    
    @objc func sortSelection() {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            defaults.set("ru", forKey: "Locale")
        case 1:
            defaults.set("en", forKey: "Locale")
        default:
            break
        }
        setupLocale()
    }
    
    func setupLocale() {
        let locale = defaults.object(forKey: "Locale") as? String
        if locale == "ru" {
            segmentedControl.selectedSegmentIndex = 0
        }
        if locale == "en" {
            segmentedControl.selectedSegmentIndex = 1
        }
    }
    
    /// Изменение значения отображения статистики за месяц/день
    /// - Parameter sender: UISegmentedControl
    //    @objc func daysDisplayTypeChange(_ sender: UISegmentedControl) {
    //
    //    }
    
    // MARK: ImagePicker sourcetype
    private func addUsersPhoto() {
        imagePickerController.delegate = self
        imagePickerController.sourceType = .photoLibrary
        present(imagePickerController, animated: true, completion: nil)
    }
    // MARK: через UserDefaults.standard записываем значение переключателя для TestVC
    @objc private func timerChange() {
        defaults.set(timer.isOn, forKey: "timer")
    }
    
    @objc func addPurchases() {
        if let count = IAPManager.shared.products?.count, count < 3 {
            let alert = UIAlertController(title: nil, message: purchasesArentAvailable, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "ОК", style: .destructive, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        } else {
            let dvc = SubscriptionVC()
            dvc.modalPresentationStyle = .fullScreen
            present(dvc, animated: true, completion: nil)
        }
    }
    
    @objc func restorePurchases() {
    
        activityIndicator.startAnimating()
      
        IAPManager.shared.restorePurchases(success: {
            let alert = UIAlertController(title: nil, message: purchaseIsRestored, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            self.activityIndicator.stopAnimating()
            self.present(alert, animated: true, completion: nil)
        }) { error in
            let alert = UIAlertController(title: nil, message: subscriptionCouldtBeRestored, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            self.activityIndicator.stopAnimating()
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    @objc func getThemeChoice() {
        defaults.set(darkThemeSwitcher.isOn, forKey: "isDarkTheme")
        loadDarkTheme()
    }
    
    @objc private func readPrivacy() {
        let dvc = PrivacyVC()
        dvc.modalPresentationStyle = .fullScreen
        present(dvc, animated: true, completion: nil)
    }
    
    private func updateData() {
        data = UserProfileModel(email: user.email, dayActivity: [], themes: [])
        userEmailLabel.text = user.email
        guard let data = user.imageData, !data.isEmpty else { return }
        iconImageView.image = UIImage(data: data)
    }
    
    private func isOtherIphone() {
        var heightScreen: CGFloat = 0
        var widthScreen: CGFloat = 0
        
        let bounds = UIScreen.main.bounds
        heightScreen = bounds.size.height //get the height of screen
        widthScreen = bounds.size.width //get the width of screen
        
        if heightScreen == 568.0 && widthScreen == 320.0 {
            NSLayoutConstraint.activate([
                timerLabel.topAnchor.constraint(equalTo: userEmailLabel.bottomAnchor, constant: 30),
                addPurchase.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -60),
                segmentedControl.widthAnchor.constraint(equalToConstant: 100)
            ])
        } else {
                NSLayoutConstraint.activate([
                timerLabel.topAnchor.constraint(equalTo: userEmailLabel.bottomAnchor, constant: 60),
                segmentedControl.widthAnchor.constraint(equalToConstant: 130),
                addPurchase.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -120)
            ])
        }
    }
    
    private func loadDarkTheme() {
        let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
        let titleTextAttributesSelected = userCurretChoiceIs ? [NSAttributedString.Key.foregroundColor: black] : [NSAttributedString.Key.foregroundColor: black]
        let titleTextAttributesNormal = userCurretChoiceIs ? [NSAttributedString.Key.foregroundColor: white] : [NSAttributedString.Key.foregroundColor: black]
        
        view.backgroundColor = userCurretChoiceIs ? darkGrey : white
        iconImageView.backgroundColor = userCurretChoiceIs ? white : .clear
        exitButton.setImage(UIImage(named: userCurretChoiceIs ? "closeWhite" : "close"), for: .normal)
        userEmailLabel.textColor = userCurretChoiceIs ? white : black
        timerLabel.textColor = userCurretChoiceIs ? white : black
        themeLabel.textColor = userCurretChoiceIs ? white : black
        translateLabel.textColor = userCurretChoiceIs ? white : black
        exitButton.tintColor = userCurretChoiceIs ? white : black
        segmentedControl.tintColor = userCurretChoiceIs ? white : black85
        segmentedControl.backgroundColor = userCurretChoiceIs ? black : veryLightPinkTwo15
        segmentedControl.setTitleTextAttributes(titleTextAttributesSelected, for: .selected)
        segmentedControl.setTitleTextAttributes(titleTextAttributesNormal, for: .normal)
    }
    private func subscriptionPurchased() {
        defaults.set(IAPManager.shared.getSubscriptionStatus(), forKey: "subscription")
        purchasedButtonIs()
    }
    
    func purchasedButtonIs() {
        let current = defaults.bool(forKey: "subscription")
        addPurchase.alpha = current ? 0 : 1
    }
}

// MARK: - UIImagePickerControllerDelegate
extension UserProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
        iconImageView.image = image
        upload(image: image)
    }
}

extension UserProfileVC: UserPresentable {
    
    func addSubView() {
        view.addSubview(iconImageView)
        view.addSubview(exitButton)
        view.addSubview(logoutButton)
        view.addSubview(userEmailLabel)
        view.addSubview(timerLabel)
        view.addSubview(timer)
        view.addSubview(darkThemeSwitcher)
        view.addSubview(themeLabel)
        view.addSubview(addPurchase)
        view.addSubview(restorePurchase)
        view.addSubview(segmentedControl)
        view.addSubview(translateLabel)
        view.addSubview(privacyButton)
        view.addSubview(activityIndicator)
    }
    
    func makeConstraints() {
        NSLayoutConstraint.activate([
            logoutButton.widthAnchor.constraint(equalToConstant: Constants.buttonSize),
            logoutButton.heightAnchor.constraint(equalTo: logoutButton.widthAnchor),
            logoutButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: Constants.offset),
            logoutButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Constants.offset),
            
            exitButton.widthAnchor.constraint(equalToConstant: Constants.buttonSize),
            exitButton.heightAnchor.constraint(equalTo: logoutButton.widthAnchor),
            exitButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: Constants.offset),
            exitButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Constants.offset),
            
            iconImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 40),
            iconImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            iconImageView.heightAnchor.constraint(equalToConstant: Constants.iconSize),
            iconImageView.widthAnchor.constraint(equalToConstant: Constants.iconSize),
            
            userEmailLabel.topAnchor.constraint(equalTo: iconImageView.bottomAnchor, constant: 16),
            userEmailLabel.heightAnchor.constraint(equalToConstant: 31),
            userEmailLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -Constants.offset),
            userEmailLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: Constants.offset),
            
            timerLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 27),
            timerLabel.centerYAnchor.constraint(lessThanOrEqualTo: timer.centerYAnchor),
            
            timer.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -29),
            timer.topAnchor.constraint(equalTo: timerLabel.topAnchor),
            
            themeLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 27),
            themeLabel.topAnchor.constraint(equalTo: timerLabel.bottomAnchor, constant: 24),
            themeLabel.centerYAnchor.constraint(lessThanOrEqualTo: darkThemeSwitcher.centerYAnchor),
            
            darkThemeSwitcher.topAnchor.constraint(equalTo: timer.bottomAnchor, constant: 20),
            darkThemeSwitcher.rightAnchor.constraint(equalTo: timer.rightAnchor),
            
            segmentedControl.leftAnchor.constraint(equalTo: translateLabel.rightAnchor, constant: 10),
            segmentedControl.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -27),
            segmentedControl.centerYAnchor.constraint(equalTo: translateLabel.centerYAnchor),
            
            translateLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 27),
            translateLabel.topAnchor.constraint(equalTo: themeLabel.bottomAnchor, constant: 24),
            
            restorePurchase.topAnchor.constraint(equalTo: translateLabel.bottomAnchor, constant: 24),
            restorePurchase.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 27),
            restorePurchase.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            restorePurchase.heightAnchor.constraint(equalToConstant: 28),
            
            addPurchase.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            addPurchase.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            addPurchase.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            addPurchase.heightAnchor.constraint(equalToConstant: 56),
            
            privacyButton.topAnchor.constraint(equalTo: addPurchase.bottomAnchor, constant: 10),
            privacyButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            privacyButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            privacyButton.heightAnchor.constraint(equalToConstant: 25),
            privacyButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -36),
            
            activityIndicator.widthAnchor.constraint(equalToConstant: 56),
            activityIndicator.heightAnchor.constraint(equalToConstant: 56),
            activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
}
