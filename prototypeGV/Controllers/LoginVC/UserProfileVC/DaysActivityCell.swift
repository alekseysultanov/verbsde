//
//  DaysActivityCell.swift
//  prototypeGV
//
//  Created by Vladimir Ereskin on 3/7/20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

class DaysActivityCell: UITableViewCell {
    
    private struct Constants {
        static let identifireCell: String = String(describing: DaysActivityCollectionViewCell.self)
        static let daysActivityHeight: CGFloat = 180
    }
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 20, height: Constants.daysActivityHeight)
        layout.minimumLineSpacing = 16
        layout.minimumInteritemSpacing = 16
        layout.scrollDirection = .horizontal
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.showsHorizontalScrollIndicator = false
        view.backgroundColor = .white
        view.dataSource = self
        return view
    }()
    
    private var dataModel: [DaysActivityCellModel] = []
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
        makeConstraints()
    }
    
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    func update(with model: [DaysActivityCellModel]) {
        dataModel = model
        collectionView.reloadData()
    }
    
    private func setupViews() {
        contentView.addSubview(collectionView)
        
        collectionView.register(DaysActivityCollectionViewCell.self, forCellWithReuseIdentifier: Constants.identifireCell)
    }
    
    private func makeConstraints() {
        NSLayoutConstraint.activate([
            collectionView.heightAnchor.constraint(equalToConstant: Constants.daysActivityHeight),
            collectionView.topAnchor.constraint(equalTo: contentView.topAnchor),
            collectionView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            collectionView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            collectionView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
}

extension DaysActivityCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.identifireCell, for: indexPath) as? DaysActivityCollectionViewCell else { return UICollectionViewCell() }
        
        cell.update(with: dataModel[indexPath.row])
        
        return cell
    }
}
