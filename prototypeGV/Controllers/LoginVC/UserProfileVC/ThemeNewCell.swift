//
//  ThemeNewCell.swift
//  prototypeGV
//
//  Created by Vladimir Ereskin on 3/8/20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

class ThemeNewCell: UITableViewCell {
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = black
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 20)
        return label
    }()
    
    let currentResultLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = black
        label.font = UIFont(name: "TeXGyreAdventor-Regular", size: 14)
        return label
    }()
    
    let progressView: UIProgressView = {
        let progress = UIProgressView()
        progress.translatesAutoresizingMaskIntoConstraints = false
        progress.progress = 0.5
        progress.contentMode = .scaleToFill
        return progress
    }()
    
    let learnedView: UIImageView = {
        let image = UIImageView(image: UIImage(named: "done"))
        image.translatesAutoresizingMaskIntoConstraints = false
        image.alpha = 0
        return image
    }()
    
    let scoreLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor(red: 109 / 255,
                                  green: 212 / 255,
                                  blue: 0,
                                  alpha: 1)
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 20)
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: cellIdentifier)
        setup()
    }
    
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    func update(theme: Theme) {
        let progress = theme.getProgress()
        
        nameLabel.text = theme.name
        progressView.setProgressBar(progress: progressView)
        progressView.tintColor = ProgressColor.by(progress: progress)
        progressView.progress = progress
        currentResultLabel.text = "\(theme.getRightVerbCount())/\(theme.verb.count)"
        if progress == 1.0 {
            learnedView.alpha = 1
        }
        scoreLabel.text = "+20"
    }
}

extension ThemeNewCell: UserPresentable {
    
    func addSubView() {
        contentView.addSubview(nameLabel)
        contentView.addSubview(currentResultLabel)
        contentView.addSubview(progressView)
        contentView.addSubview(learnedView)
        contentView.addSubview(scoreLabel)
    }
    
    func makeConstraints() {
        NSLayoutConstraint.activate([
            nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
            nameLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            
            learnedView.centerYAnchor.constraint(equalTo: nameLabel.centerYAnchor),
            learnedView.leftAnchor.constraint(equalTo: nameLabel.rightAnchor, constant: 10),
            learnedView.heightAnchor.constraint(equalToConstant: 20),
            learnedView.widthAnchor.constraint(equalToConstant: 20),
            
            scoreLabel.centerYAnchor.constraint(equalTo: nameLabel.centerYAnchor),
            scoreLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            
            progressView.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 20),
            progressView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            progressView.rightAnchor.constraint(equalTo: currentResultLabel.leftAnchor, constant: -20),
            progressView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16),
            progressView.heightAnchor.constraint(equalToConstant: 8),
            
            currentResultLabel.centerYAnchor.constraint(equalTo: progressView.centerYAnchor, constant: -2),
            currentResultLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor)
        ])
    }
}

