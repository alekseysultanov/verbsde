//
//  RadioButtonView.swift
//  prototypeGV
//
//  Created by Vladimir Ereskin on 8/19/20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

final class RadioButtonView: UIView {
    let ruView = RadioView()
    let enView = RadioView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupLocale() {
        let locale = defaults.object(forKey: "Locale") as? String
        if locale == "ru" {
            ruView.isActive = true
        }
        
        if locale == "en" {
            enView.isActive = true
        }
    }
    
    private func setupView() {
        translatesAutoresizingMaskIntoConstraints = false
        
        ruView.textLabel.text = "Russian"
        enView.textLabel.text = "English"
        
        addSubview(ruView)
        addSubview(enView)
        
        ruView.addTarget(self, action: #selector(selectLangRU), for: .touchUpInside)
        enView.addTarget(self, action: #selector(selectLangEN), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        NSLayoutConstraint.activate([
            ruView.topAnchor.constraint(equalTo: topAnchor),
            ruView.leadingAnchor.constraint(equalTo: leadingAnchor),
            ruView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            enView.topAnchor.constraint(equalTo: ruView.bottomAnchor, constant: 16),
            enView.leadingAnchor.constraint(equalTo: leadingAnchor),
            enView.trailingAnchor.constraint(equalTo: trailingAnchor),
            enView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    @objc private func selectLangRU() {
        ruView.isActive = true
        enView.isActive = false
        
        defaults.set("ru", forKey: "Locale")
    }
    
    @objc private func selectLangEN() {
        ruView.isActive = false
        enView.isActive = true
        
        defaults.set("en", forKey: "Locale")
    }
}
