//
//  CustomSegmentControl.swift
//  prototypeGV
//
//  Created by Vladimir Ereskin on 3/8/20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

//import UIKit
//
//protocol CustomSegmentControlProtocol: AnyObject {
//    func updateDays(for type: TypeRangeTime)
//}
//
//class CustomSegmentControl: UIView {
//    weak var delegate: CustomSegmentControlProtocol?
//    
//    let leftButton: UIButton = {
//        let button = UIButton()
//        button.translatesAutoresizingMaskIntoConstraints = false
//        button.setTitleColor(.black, for: .normal)
//        button.titleLabel?.font = UIFont(name: "TeXGyreAdventor-Bold", size: 13)
//        button.setTitle("Weekly", for: .normal)
//        button.addTarget(self, action: #selector(buttonLeftPressed), for: .touchUpInside)
//        return button
//    }()
//    let rightButton: UIButton = {
//        let button = UIButton()
//        button.translatesAutoresizingMaskIntoConstraints = false
//        button.setTitleColor(.black, for: .normal)
//        button.titleLabel?.font = UIFont(name: "TeXGyreAdventor-Regular", size: 13)
//        button.setTitle("Monthly", for: .normal)
//        button.addTarget(self, action: #selector(buttonRightPressed), for: .touchUpInside)
//        return button
//    }()
//    
//    private let centerView: UIView = {
//        let view = UIView()
//        view.translatesAutoresizingMaskIntoConstraints = false
//        return view
//    }()
//    
//    private let selectedView: UIView = {
//        let view = UIView()
//        view.translatesAutoresizingMaskIntoConstraints = false
//        view.layer.cornerRadius = 28 / 2
//        view.clipsToBounds = true
//        view.backgroundColor = .white
//        return view
//    }()
//    
//    private var activeIndex = 0
//    
//    private var leftSelectedConstraint = NSLayoutConstraint()
//    private var rightSelectedConstraint = NSLayoutConstraint()
//    
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        translatesAutoresizingMaskIntoConstraints = false
//        layer.cornerRadius = 32 / 2
//        clipsToBounds = true
//        backgroundColor = UIColor(red: 118 / 255,
//                                  green: 118 / 255,
//                                  blue: 128 / 255,
//                                  alpha: 0.12)
//        
//        leftSelectedConstraint = selectedView.leftAnchor.constraint(equalTo: leftAnchor, constant: 3)
//        rightSelectedConstraint = selectedView.rightAnchor.constraint(equalTo: centerView.leftAnchor, constant: -3)
//        
//        setupViews()
//        makeConstraints()
//    }
//    
//    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
//    
//    private func setupViews() {
//        addSubview(centerView)
//        addSubview(selectedView)
//        addSubview(leftButton)
//        addSubview(rightButton)
//    }
//    
//    private func makeConstraints() {
//        NSLayoutConstraint.activate([
//            heightAnchor.constraint(equalToConstant: 32),
//            
//            centerView.topAnchor.constraint(equalTo: topAnchor),
//            centerView.bottomAnchor.constraint(equalTo: bottomAnchor),
//            centerView.centerXAnchor.constraint(equalTo: centerXAnchor),
//            centerView.widthAnchor.constraint(equalToConstant: 1),
//            
//            selectedView.topAnchor.constraint(equalTo: topAnchor, constant: 3),
//            leftSelectedConstraint,
//            rightSelectedConstraint,
//            selectedView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -3),
//            
//            leftButton.topAnchor.constraint(equalTo: topAnchor, constant: 3),
//            leftButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 3),
//            leftButton.rightAnchor.constraint(equalTo: centerView.leftAnchor, constant: -3),
//            leftButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -3),
//            
//            rightButton.topAnchor.constraint(equalTo: topAnchor, constant: 3),
//            rightButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -3),
//            rightButton.leftAnchor.constraint(equalTo: centerView.rightAnchor, constant: 3),
//            rightButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -3),
//        ])
//    }
//    
//    @objc private func buttonLeftPressed() {
//        guard activeIndex != 0 else { return }
//        activeIndex = 0
//        leftButton.titleLabel?.font = UIFont(name: "TeXGyreAdventor-Bold", size: 13)
//        rightButton.titleLabel?.font = UIFont(name: "TeXGyreAdventor-Regular", size: 13)
//        leftSelectedConstraint.constant -= leftButton.frame.width + 6
//        rightSelectedConstraint.constant -= leftButton.frame.width + 6
//        UIView.animate(withDuration: 0.2) {
//            self.layoutIfNeeded()
//        }
//        delegate?.updateDays(for: .week)
//    }
//    
//    @objc private func buttonRightPressed() {
//        guard activeIndex != 1 else { return }
//        activeIndex = 1
//        leftButton.titleLabel?.font = UIFont(name: "TeXGyreAdventor-Regular", size: 13)
//        rightButton.titleLabel?.font = UIFont(name: "TeXGyreAdventor-Bold", size: 13)
//        leftSelectedConstraint.constant += leftButton.frame.width + 6
//        rightSelectedConstraint.constant += leftButton.frame.width + 6
//        UIView.animate(withDuration: 0.2) {
//            self.layoutIfNeeded()
//        }
//        delegate?.updateDays(for: .month)
//    }
//}
