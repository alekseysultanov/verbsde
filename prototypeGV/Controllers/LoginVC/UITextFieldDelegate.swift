//
//  UITextFieldDelegate.swift
//  prototypeGV
//
//  Created by admin on 28.06.20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

extension LoginVC: UITextFieldDelegate {
    //hidden keyboard with Done tapped
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
