//
//  UserPresentable.swift
//  prototypeGV
//
//  Created by admin on 28.06.20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

extension LoginVC: UserPresentable {
    
    func addSubView() {
        view.addSubview(icon)
        view.addSubview(nameLabel)
        view.addSubview(stackTextField)
        view.addSubview(forgotPassButton)
        view.addSubview(loginButton)
        view.addSubview(idLabel)
        view.addSubview(registerButton)
        view.addSubview(activityIndicator)
        view.addSubview(appleLogInButton)
        createStack()
    }
    
    func makeConstraints() {
        
        icon.topAnchor.constraint(equalTo: view.topAnchor, constant: 87).isActive = true
        icon.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        icon.heightAnchor.constraint(equalToConstant: 110).isActive = true
        icon.widthAnchor.constraint(equalToConstant: 110).isActive = true
        
        nameLabel.topAnchor.constraint(equalTo: icon.bottomAnchor, constant: 0).isActive = true
        nameLabel.widthAnchor.constraint(equalToConstant: 80).isActive = true
        nameLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        nameLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        appleLogInButton.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 10).isActive = true
        appleLogInButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        stackTextField.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        stackTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8).isActive = true
        stackTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
        stackTextField.heightAnchor.constraint(equalToConstant: 122).isActive = true
        
        loginButton.topAnchor.constraint(equalTo: stackTextField.bottomAnchor, constant: 30).isActive = true
        loginButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8).isActive = true
        loginButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
        loginButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loginButton.heightAnchor.constraint(equalToConstant: 56).isActive = true
        
        idLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        idLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
        idLabel.bottomAnchor.constraint(equalTo: registerButton.topAnchor, constant: -10).isActive = true
        
        registerButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        registerButton.widthAnchor.constraint(equalToConstant: 120).isActive = true
        registerButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        forgotPassButton.topAnchor.constraint(equalTo: registerButton.bottomAnchor, constant: 20).isActive = true
        forgotPassButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        forgotPassButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        NSLayoutConstraint.activate([
            activityIndicator.widthAnchor.constraint(equalToConstant: 56),
            activityIndicator.heightAnchor.constraint(equalToConstant: 56),
            activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
}
