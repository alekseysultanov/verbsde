//
//  LoginVC.swift
//  prototypeGV
//
//  Created by admin on 01.01.20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseStorage
import Firebase
import AuthenticationServices
import RealmSwift

class LoginVC: UIViewController {
    
    //MARK: - Properties
    
    weak var delegate: ThemeVC?
    
    /// Иконка приложения
    let icon: UIImageView = {
        let image = UIImageView(image: UIImage(named: "icnEasy"))
        image.translatesAutoresizingMaskIntoConstraints = false
        image.layer.cornerRadius = image.frame.size.width / 2
        image.clipsToBounds = true
        return image
    }()
    
    /// Название приложения
    let nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 32)
        label.textAlignment = .center
        label.text = "Easy!"
        return label
    }()
    //    MARK: appleLoginButton
    var appleLogInButton : ASAuthorizationAppleIDButton = {
        let button = ASAuthorizationAppleIDButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(handleLogInWithAppleID), for: .touchUpInside)
        return button
    }()
    
    /// Поле ввода логина
    lazy var loginTextField: UITextField = {
        let textField = InputTextField(text: "Email".localized(), image: UIImage(named: "icnEmail"), isSecureText: false)
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.delegate = self
        textField.textContentType = .emailAddress
        textField.clearButtonMode = .whileEditing
        textField.clearsOnBeginEditing = true
        textField.clearButtonMode = .whileEditing
        textField.clearsOnBeginEditing = true
        textField.layer.cornerRadius = 12
        textField.keyboardType = .emailAddress
        return textField
    }()
    
    /// Поде ввода пароля
    lazy var passTextField: UITextField = {
        let textField = InputTextField(text: password, image: UIImage(named: "icnPass"), isSecureText: true)
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.delegate = self
        textField.textContentType = .password
        textField.clearButtonMode = .whileEditing
        textField.clearsOnBeginEditing = true
        textField.layer.cornerRadius = 12
        return textField
    }()
    
    let forgotPassButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(iForgotPassword, for: .normal)
        button.setTitleColor(azure, for: .normal)
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont(name: "TeXGyreAdventor-Bold", size: 16)
        button.addTarget(self, action: #selector(forgotPass), for: .touchUpInside)
        return button
    }()
    
    /// Кнопка авторизации/регистрации
    let loginButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Login".localized(), for: .normal)
        button.layer.cornerRadius = 12
        button.backgroundColor = azure
        button.setTitleColor(white, for: .normal)
        button.setTitleColor(.gray, for: .highlighted)
        button.titleLabel?.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        button.addTarget(self, action: #selector(login), for: .touchUpInside)
        return button
    }()
    
    /// Метка сообщения об отсутствии ID
    let idLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 16)
        label.text = "Don’t have an ID?".localized()
        return label
    }()
    
    /// Кнопка регистрации
    let registerButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Register".localized(), for: .normal)
        button.setTitleColor(azure, for: .normal)
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont(name: "TeXGyreAdventor-Bold", size: 16)
        button.addTarget(self, action: #selector(register), for: .touchUpInside)
        return button
    }()
    
    /// Вью для объединения полей ввода логина/пароля
    let stackTextField: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.spacing = 10
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.alignment = .fill
        stack.contentMode = .scaleToFill
        return stack
    }()
    
    let activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.hidesWhenStopped = true
        return indicator
    }()
    
    //MARK: - Life circle
    override func viewWillAppear(_ animated: Bool) {
        loginTextField.transform = CGAffineTransform(scaleX: 0.00001, y: 0.000001)
        passTextField.transform = CGAffineTransform(scaleX: 0.00001, y: 0.000001)
        loginButton.transform = CGAffineTransform(scaleX: 0.00001, y: 0.000001)
        loadDarkTheme()
        isOtherIphone()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        UIView.animate(withDuration: Animation.duration07, delay: Animation.delay03, options: [.curveEaseInOut], animations: {
            self.loginTextField.transform = CGAffineTransform.identity
        }) { (_) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                let userDefaults = defaults
                userDefaults.set(false, forKey: "showAnimation")
            }
        }
        UIView.animate(withDuration: Animation.duration07, delay: Animation.delay04, options: [.curveEaseInOut], animations: {
            self.passTextField.transform = CGAffineTransform.identity
        }) { (_) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                let userDefaults = defaults
                userDefaults.set(false, forKey: "showAnimation")
            }
        }
        UIView.animate(withDuration: Animation.duration07, delay: Animation.delay05, options: [.curveEaseInOut], animations: {
            self.loginButton.transform = CGAffineTransform.identity
        }) { (_) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                let userDefaults = defaults
                userDefaults.set(false, forKey: "showAnimation")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        icon.alpha = 1
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil);
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: UIResponder.keyboardWillHideNotification, object: nil);
        setup()
    }
    
    //MARK: - User methods
    
    @objc private func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y = -150 // Move view 150 points upward
    }
    
    @objc private func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0 // Move view to original position
    }
    
    func createStack() {
        stackTextField.addArrangedSubview(loginTextField)
        stackTextField.addArrangedSubview(passTextField)
    }
    
    @objc func handleLogInWithAppleID() {
        let request = ASAuthorizationAppleIDProvider().createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let controller = ASAuthorizationController(authorizationRequests: [request])
        
        controller.delegate = self
        controller.presentationContextProvider = self
        
        controller.performRequests()
    }
    
    private func isOtherIphone() {
        var heightScreen: CGFloat = 0
        var widthScreen: CGFloat = 0
        
        let bounds = UIScreen.main.bounds
        heightScreen = bounds.size.height //get the height of screen
        widthScreen = bounds.size.width //get the width of screen
        
        if heightScreen == 568.0 && widthScreen == 320.0 {
            NSLayoutConstraint.activate([
                icon.topAnchor.constraint(equalTo: view.topAnchor, constant: 47),
                forgotPassButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20)
            ])
        } else {
            NSLayoutConstraint.activate([
                icon.topAnchor.constraint(equalTo: view.topAnchor, constant: 87),
                forgotPassButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -70)
            ])
        }
    }
    
    @objc private func login() {
        guard let login = loginTextField.text, !login.isEmpty else { return }
        guard let password = passTextField.text, !password.isEmpty else { return }
        
        activityIndicator.startAnimating()
        if loginButton.titleLabel?.text == "Login".localized() {
            Auth.auth().signIn(withEmail: login, password: password) { [weak self] (result, error) in
                if error == nil {
                    if result != nil {
                        let userDefaults = defaults
                        userDefaults.setValue(result?.user.uid, forKey: "userUid")
                        
                        DataManager.getFirebaseDB { isValue in
                            if !isValue {
                                DataManager.userInfoSave(UserInfo(name: login,
                                                                  email: login,
                                                                  imageData: nil,
                                                                  verbFixVersion: -1))
                            } else {
                                self?.activityIndicator.stopAnimating()
                                self?.dismiss(animated: true, completion: nil)
                            }
                        }
                    }
                } else {
                    let alert = UIAlertController(title: nil, message: "Email address is badly formatted or password is invalid".localized(), preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
                    alert.addAction(cancelAction)
                    self?.present(alert, animated: true, completion: nil)
                }
            }
        }
        
        if loginButton.titleLabel?.text == "Register".localized() {
            Auth.auth().createUser(withEmail: loginTextField.text!, password: passTextField.text!) { [weak self] (result, error) in
                if error == nil {
                    let userDefaults = defaults
                    userDefaults.setValue(result?.user.uid, forKey: "userUid")
                    DataManager.clearVerbUser()
                    DataManager.clearThemeUser()
                    DataManager.userInfoSave(
                        UserInfo(name: self?.loginTextField.text ?? "",
                                 email: self?.loginTextField.text ?? "",
                                 imageData: nil,
                                 verbFixVersion: -1)
                    )
                    
                    self?.delegate?.updateBD()
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                        self?.activityIndicator.stopAnimating()
                        self?.dismiss(animated: true, completion: nil)
                    }
                    
                    Auth.auth().currentUser?.sendEmailVerification { (error) in
                        if error == nil {
                            //                                let alert = UIAlertController(title: nil, message: "Email address is veryfied".localized(), preferredStyle: .alert)
                            //                                let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .destructive, handler: nil)
                            //                                alert.addAction(cancelAction)
                            //                                self?.present(alert, animated: true, completion: nil)
                        }
                    }
                } else {
                    self?.activityIndicator.stopAnimating()
                    let alert = UIAlertController(title: nil, message: "User is already registered or password is short".localized(), preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .destructive, handler: nil)
                    alert.addAction(cancelAction)
                    self?.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    @objc private func register() {
        if loginButton.titleLabel?.text == "Login".localized() {
            loginButton.setTitle("Register".localized(), for: .normal)
            idLabel.text = "Already registered?".localized()
            registerButton.setTitle("Login".localized(), for: .normal)
            forgotPassButton.alpha = 0
        } else {
            loginButton.setTitle("Login".localized(), for: .normal)
            idLabel.text = "Don’t have an ID?".localized()
            registerButton.setTitle("Register".localized(), for: .normal)
            forgotPassButton.alpha = 1
        }
    }
    
    @objc private func forgotPass() {
        Auth.auth().sendPasswordReset(withEmail: loginTextField.text!) { (error) in
            if error == nil {
                let alert = UIAlertController(title: nil, message: "An email with password has been sent to your address".localized(), preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .destructive, handler: nil)
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
            } else {
                let alert = UIAlertController(title: nil, message: "Check email".localized(), preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .destructive, handler: nil)
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    private func loadDarkTheme(){
        let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
        view.backgroundColor = userCurretChoiceIs ? black : white
        nameLabel.textColor = userCurretChoiceIs ? white : black85
        loginTextField.textColor = userCurretChoiceIs ? black : black85
        passTextField.textColor = userCurretChoiceIs ? black : black85
        loginTextField.backgroundColor = userCurretChoiceIs ? veryLightPinkTwo : black10
        passTextField.backgroundColor = userCurretChoiceIs ? veryLightPinkTwo : black10
        idLabel.textColor = userCurretChoiceIs ? white : black85
        activityIndicator.color = userCurretChoiceIs ? azure : black85
    }
}

extension LoginVC: ASAuthorizationControllerDelegate {
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            let userIdentifier = appleIDCredential.user
    
            defaults.set(userIdentifier, forKey: "userIdentifier1")
            
            //Save the UserIdentifier somewhere in your server/database
            let vc = ThemeVC()
            vc.userID = userIdentifier
            self.present(UINavigationController(rootViewController: vc), animated: true)
            break
        default:
            break
        }
    }
}

extension LoginVC: ASAuthorizationControllerPresentationContextProviding {
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
           return self.view.window!
    }
}
