//
//  ThemeNewCell.swift
//  prototypeGV
//
//  Created by Vladimir Ereskin on 3/8/20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

class ThemeCell: UITableViewCell {
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let currentResultLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "TeXGyreAdventor-Regular", size: 14)
        return label
    }()
    
    let progressView: UIProgressView = {
        let progress = UIProgressView()
        progress.translatesAutoresizingMaskIntoConstraints = false
        progress.progress = 0.0
        progress.backgroundColor = veryLightPinkTwo
        progress.contentMode = .scaleToFill
        return progress
    }()
    
    let learnedView: UIImageView = {
        let image = UIImageView(image: UIImage(named: "done"))
        image.translatesAutoresizingMaskIntoConstraints = false
        image.alpha = 0
        return image
    }()
    
    let blockView: UIImageView = {
        let image = UIImageView(image: UIImage(named: "icnPass"))
        image.translatesAutoresizingMaskIntoConstraints = false
        image.isHidden = true
        return image
    }()
    
    let scoreLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = black85
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 20)
        return label
    }()
    
    let separatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var isNavigateTo = true
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: cellIdentifier)
        setup()
    }
    
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    func update(theme: Theme, countLearned: Int) {
        let progress = theme.getProgress()
        
        nameLabel.text = theme.name.localized()
        progressView.setProgressBar(progress: progressView)
        progressView.tintColor = ProgressColor.by(progress: progress)
        progressView.progress = progress
        currentResultLabel.text = "\(theme.getRightVerbCount())/\(theme.getVerbCount())"
        if progress == 1.0 {
            learnedView.alpha = 1
        } else {
            learnedView.alpha = 0
        }
        
        if countLearned > 0 {
            scoreLabel.textColor = darkLimeGreen
            scoreLabel.text = "+\(countLearned)"
        } else if countLearned < 0 {
            scoreLabel.textColor = vermillion
            scoreLabel.text = "\(countLearned)"
        } else {
            scoreLabel.textColor = veryLightPinkTwo
            scoreLabel.text = "\(countLearned)"
        }
    }
    
    func loadDarkTheme() {
        let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
        nameLabel.textColor = userCurretChoiceIs ? white : black85
        currentResultLabel.textColor = userCurretChoiceIs ? white : black85
        scoreLabel.textColor = userCurretChoiceIs ? darkLimeGreen : darkLimeGreen
        separatorView.backgroundColor = userCurretChoiceIs ? black : .gray
        separatorView.alpha = userCurretChoiceIs ? 1.0 : 0.2
    }
    
    func isOtherIphone() {
        var heightScreen: CGFloat = 0
        var widthScreen: CGFloat = 0
                   
        let bounds = UIScreen.main.bounds
        heightScreen = bounds.size.height //get the height of screen
        widthScreen = bounds.size.width //get the width of screen
        
        if heightScreen == 568.0 && widthScreen == 320.0 {
         nameLabel.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        } else {
         nameLabel.font = UIFont(name: "TeXGyreAdventor-Bold", size: 20)
        }
    }
    
    func blockCell(is value: Bool) {
        isNavigateTo = !value
        if value {
            nameLabel.alpha = 0.5
            currentResultLabel.alpha = 0.5
            progressView.alpha = 0.5
            learnedView.isHidden = true
            blockView.isHidden = false
            scoreLabel.isHidden = true
        } else {
            nameLabel.alpha = 1
            currentResultLabel.alpha = 1
            progressView.alpha = 1
            blockView.isHidden = true
            scoreLabel.isHidden = false
        }
    }
}

extension ThemeCell: UserPresentable {
    
    func addSubView() {
        contentView.addSubview(nameLabel)
        contentView.addSubview(currentResultLabel)
        contentView.addSubview(progressView)
        contentView.addSubview(learnedView)
        contentView.addSubview(scoreLabel)
        contentView.addSubview(separatorView)
        contentView.addSubview(blockView)
    }
    
    func makeConstraints() {
        NSLayoutConstraint.activate([
            nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            nameLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 27),
            
            learnedView.centerYAnchor.constraint(equalTo: nameLabel.centerYAnchor),
            learnedView.leftAnchor.constraint(equalTo: nameLabel.rightAnchor, constant: 15),
            learnedView.heightAnchor.constraint(equalToConstant: 22),
            learnedView.widthAnchor.constraint(equalToConstant: 22),
            
            blockView.centerYAnchor.constraint(equalTo: nameLabel.centerYAnchor),
            blockView.rightAnchor.constraint(equalTo: contentView.rightAnchor,constant: -27),
            blockView.heightAnchor.constraint(equalToConstant: 22),
            blockView.widthAnchor.constraint(equalToConstant: 22),
            
            scoreLabel.centerYAnchor.constraint(equalTo: nameLabel.centerYAnchor),
            scoreLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor,constant: -27),
            
            progressView.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 16),
            progressView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 27),
            progressView.rightAnchor.constraint(equalTo: currentResultLabel.leftAnchor, constant: -13),
            progressView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -15),
            progressView.heightAnchor.constraint(equalToConstant: 6),
            
            currentResultLabel.centerYAnchor.constraint(equalTo: progressView.centerYAnchor, constant: -2),
            currentResultLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -27),
            
            separatorView.heightAnchor.constraint(equalToConstant: 1),
            separatorView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            separatorView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            separatorView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
}

