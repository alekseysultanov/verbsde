//
//  ExtensionThemeTVC.swift
//  prototypeGV
//
//  Created by admin on 28.06.20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

extension ThemeVC: UITableViewDataSource, UITableViewDelegate  {
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userInfo.themes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ThemeCell else { return UITableViewCell() }
        let theme = userInfo.themes[indexPath.row]
        let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
        cell.selectionStyle = .none
        cell.update(theme: theme, countLearned: theme.learnCount)
        cell.loadDarkTheme()
        cell.isOtherIphone()
        cell.backgroundColor = userCurretChoiceIs ? darkGrey : white
        //        cell.blockCell(is: !IAPManager.shared.getSubscriptionStatus())
        
        switch theme.name {
        case "Strong with haben":
            cell.blockCell(is: !IAPManager.shared.getSubscriptionStatus())
        case "Strong with sein":
            cell.blockCell(is: !IAPManager.shared.getSubscriptionStatus())
        default:
            break
        }
        
        return cell
    }
    
    // MARK: - Table view Delegate
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dvc = VerbVC()
        if let indexPath = tableView.indexPathForSelectedRow {
            let cell = tableView.cellForRow(at: indexPath) as? ThemeCell
            guard let isNavigate = cell?.isNavigateTo, isNavigate else { return }
            dvc.selectedTheme = userInfo.themes[indexPath.row]
            title = ""
            navigationController?.pushViewController(dvc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 78
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let theme = userInfo.themes[indexPath.row]
        guard !theme.fixed else { return nil }
        let editAction = UIContextualAction(style: .normal, title:  "Edit".localized(), handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            tableView.setEditing(true, animated: true)
            self.alertForAddAndUpdateTheme(theme, indexPath: indexPath)
            success(true)
        })
        editAction.backgroundColor = brightOrange
        
        let deleteAction = UIContextualAction(style: .normal, title:  "Delete".localized(), handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            DataManager.deleteTheme(indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            success(true)
        })
        deleteAction.backgroundColor = vermillion
        
        return UISwipeActionsConfiguration(actions: [editAction, deleteAction])
    }
}
