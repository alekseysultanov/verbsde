//
//  ExtensionsThemeTVC.swift
//  prototypeGV
//
//  Created by Max Polinkovsky on 22.09.19.
//  Copyright © 2019 Max Polinkovsky. All rights reserved.
//

import UIKit

extension ThemeVC {
    
    @objc func alertForAddAndUpdateTheme(_ theme: Theme!, indexPath: IndexPath) {
        
        var title = newTheme
        var doneTitle = save
        if theme != nil {
            title = edit
            doneTitle = update
        }
        
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        var alertTextField = UITextField()
        let saveAction = UIAlertAction(title: doneTitle, style: .default) { action in
            guard let text = alertTextField.text , !text.isEmpty else { return }
            let name = text.delSpaces()
            if theme != nil {
                try! realm.write {
                    theme?.name = name
                }
                self.tableView.reloadRows(at: [indexPath], with: .automatic)
            } else {
                DataManager.addTheme(name: name, fixed: false)
                self.tableView.insertRows(at: [indexPath], with: .bottom)
            }
        }
        
        let cancelAction = UIAlertAction(title: cancel, style: .destructive, handler: nil)
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        alert.addTextField { textField in
            alertTextField = textField
            alertTextField.placeholder = ""
            
            if theme != nil {
                alertTextField.text = theme.name.delSpaces()
            }
        }
        present(alert, animated: true, completion: nil) // Вызываем алёрт контроллер
    }
}
