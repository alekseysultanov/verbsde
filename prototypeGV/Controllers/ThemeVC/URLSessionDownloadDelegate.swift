//
//  URLSessionDownloadDelegate.swift
//  prototypeGV
//
//  Created by admin on 28.06.20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

extension ThemeVC: URLSessionDownloadDelegate {
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        // create destination URL with the original pdf name
        guard let url = downloadTask.originalRequest?.url else { return }
        let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let destinationURL = documentsPath.appendingPathComponent(url.lastPathComponent)
        // delete original copy
        try? FileManager.default.removeItem(at: destinationURL)
        // copy from temp to Document
        do {
            try FileManager.default.copyItem(at: location, to: destinationURL)
            _ = destinationURL
            let str = String(destinationURL.absoluteString)
            self.readFile(bundle: str)
        } catch let error {
            print("Copy Error: \(error.localizedDescription)")
        }
    }
}

