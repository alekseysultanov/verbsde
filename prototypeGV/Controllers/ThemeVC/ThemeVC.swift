//
//  ThemeTVC.swift
//  prototypGV
//
//  Created by Max Polinkovsky on 02.05.2019.
//  Copyright © 2019 Max Polinkovsky. All rights reserved.
//

import UIKit
import RealmSwift
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class ThemeVC: UIViewController {
    
    //MARK: - Properties
    
    var userInfo = UserInfo() {
        didSet {
            try! realm.write {
                userInfo.themes.sort { $0.name < $1.name }
            }
        }
    }
    
    private var isDataBaseEmpty = false
    var url: String?
    var version: Double?
    var timer = Timer()
    var userID: String?
    
    lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    
    lazy var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.hidesWhenStopped = true
        return indicator
    }()
    
    lazy var tableView: UITableView = {
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        table.separatorStyle = .none
        table.delegate = self
        table.dataSource = self
        table.register(ThemeCell.self, forCellReuseIdentifier: cellIdentifier)
        return table
    }()
    
    lazy var iconImage: UIImageView = {
        let view = UIImageView(image: UIImage(named: "icnEasy"))
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var unlockAll: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Unlock all content"
        label.font = UIFont(name: "TeXGyreAdventor-Bold", size: 20)
        return label
    }()
    
    /// Кнопка профиля в навигейшнБаре
    private lazy var userProfileButton: UIBarButtonItem = {
        let button = UIBarButtonItem(image: UIImage(named: "account"),
                                     style: .plain,
                                     target: self,
                                     action: #selector(goToUserProfile))
        return button
    }()
    
    /// Кнопка добавления новой темы
    private lazy var addNewThemeButton: UIBarButtonItem = {
        let button = UIBarButtonItem(image: UIImage(named: "add"),
                                     style: .plain,
                                     target: self,
                                     action: #selector(addNewTheme(_:)))
        return button
    }()
    
    let addPurchase: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Subscribe to Easy+".localized(), for: .normal)
        button.layer.cornerRadius = 12
        button.backgroundColor = azure
        button.setTitleColor(white, for: .normal)
        button.setTitleColor(.gray, for: .highlighted)
        button.titleLabel?.font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
        button.addTarget(self, action: #selector(addPurchases), for: .touchUpInside)
        return button
    }()
    
    lazy var updateButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.alpha = 0
        button.setTitle("update".localized(), for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor.gray.withAlphaComponent(0.8)
        button.addTarget(self, action: #selector(updateUserData), for: .touchUpInside)
        button.layer.cornerRadius = 10
        button.layer.masksToBounds = true
        return button
    }()
    
    private var pullControl = UIRefreshControl()
    
    //MARK: - Life circle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setup()
        makeNavigationBar()
        authUser()
        makeTableHeaderView()
        
        DataManager.checkVersionFirebase { [ weak self ] url, version in
            if let url = url {
                self?.url = url
                self?.version = version
                self?.showUpdateButton(is: false)
            } else {
                self?.showUpdateButton(is: true)
            }
        }
        
        pullControl.attributedTitle = NSAttributedString(string: "Update database".localized())
        pullControl.addTarget(self, action: #selector(refreshListData(_:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            tableView.refreshControl = pullControl
        } else {
            tableView.addSubview(pullControl)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = verbs
        
        timer = Timer.scheduledTimer(withTimeInterval: 600, repeats: true) { [ weak self ] _ in
            self?.updateCheckInternetConnection()
        }
        
        DataManager.getUserInfo(completion: { [ weak self ] info in
            guard let user = info else { return }
            self?.userInfo = user
            self?.tableView.reloadData()
        })
        loadDarkTheme()
        purchasedButtonIs()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer.invalidate()
    }
    
    //MARK: - User methods
    
    private func makeNavigationBar() {
        navigationItem.leftBarButtonItem = userProfileButton
        navigationItem.rightBarButtonItem = addNewThemeButton
    }
    
    private func authUser() {
        // MARK: автовход
        Auth.auth().addStateDidChangeListener({ [weak self] (auth, user) in
            guard user != nil, defaults.object(forKey: "userUid") as? String != nil else {
                let dvc = LoginVC()
                dvc.delegate = self
                dvc.modalPresentationStyle = .fullScreen
                self?.navigationController?.present(dvc, animated: false, completion: nil)
                return
            }
            
            DataManager.getFirebaseDB { value in
                if value {
                    DataManager.getUserInfo(completion: { [ weak self ] info in
                        guard let user = info else {
                            let dvc = LoginVC()
                            dvc.modalPresentationStyle = .fullScreen
                            self?.navigationController?.present(dvc, animated: false, completion: nil)
                            return
                        }
                        self?.userInfo = user
                        self?.tableView.reloadData()
                    })
                }
            }
        })
    }
    
    @objc private func goToUserProfile() {
        let userProfileVC = UserProfileVC()
        userProfileVC.modalPresentationStyle = .fullScreen
        navigationController?.present(userProfileVC, animated: true, completion: nil)
    }
    
    func readFile(bundle: String?) {
        guard let bundleURL = bundle else {return}
        
        // Убираем функцию из другого потока
        DispatchQueue.main.async  {
            // По завершению парсинга файла мы обновляем таблицу
            ConverterDB.conversion(files: bundleURL) {
                DataManager.getUserInfo { info in
                    guard let user = info else { return }
                    self.userInfo = user
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func getDownloadFile(url: String) {
        guard let url = URL(string: url) else { return }
        let urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue())
        let downloadTask = urlSession.downloadTask(with: url)
        downloadTask.resume()
    }
    
    @objc func addNewTheme(_ sender: Any) {
        alertForAddAndUpdateTheme(nil, indexPath: IndexPath(row: self.userInfo.themes.count, section: 0))
    }
    
    func updateCheckInternetConnection() {
        NetworkConnection.checkInternet(completion: { [ weak self ] isConnected in
            if !isConnected {
                let alert = UIAlertController(title: internet, message: noInternetConnection, preferredStyle: .alert)
                let cancelButton = UIAlertAction(title: cancel, style: .default) { _ in
                    self?.timer.invalidate()
                }
                alert.addAction(cancelButton)
                self?.present(alert, animated: true)
            }
        })
    }
    
    func purchasedButtonIs() {
        let current = defaults.bool(forKey: "subscription")
        addPurchase.alpha = current ? 0 : 1
        iconImage.alpha = current ? 0 : 1
        unlockAll.alpha = current ? 0 : 1
        
    }
    
    @objc func addPurchases() {
        if let count = IAPManager.shared.products?.count, count < 3 {
            let alert = UIAlertController(title: nil, message: purchasesArentAvailable, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "ОК", style: .destructive, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        } else {
            let dvc = SubscriptionVC()
            dvc.modalPresentationStyle = .fullScreen
            present(dvc, animated: true, completion: nil)
        }
    }
    
    @objc func updateUserData() {
        if let url = url, let version = version {
            getDownloadFile(url: url)
            DataManager.saveVerbFixVersion(version, userInfo: userInfo)
            
            UIView.animate(withDuration: 0.5) {
                self.updateButton.alpha = 0
            }
        }
    }
    
    func showUpdateButton(is hidden: Bool) {
        UIView.animate(withDuration: 0.5) {
            self.updateButton.alpha = hidden ? 0 : 1
        }
    }
    
    @objc private func refreshListData(_ sender: Any) {
        self.pullControl.endRefreshing()
        self.timer = Timer.scheduledTimer(withTimeInterval: 10, repeats: true) { [ weak self ] _ in
            self?.updateCheckInternetConnection()
        }
    }
    
    func updateBD() {
        DataManager.checkVersionFirebase { [ weak self ] url, version in
            if let url = url {
                self?.url = url
                self?.version = version
                self?.updateUserData()
            }
        }
    }
    
    private func makeTableHeaderView() {
        let footerTableView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 600))
        
        tableView.tableFooterView = footerTableView
        
        footerTableView.addSubview(iconImage)
        footerTableView.addSubview(unlockAll)
        footerTableView.addSubview(addPurchase)
        NSLayoutConstraint.activate([
            iconImage.topAnchor.constraint(equalTo: footerTableView.topAnchor, constant: 40),
            iconImage.heightAnchor.constraint(equalToConstant: 111),
            iconImage.widthAnchor.constraint(equalTo: iconImage.heightAnchor),
            iconImage.centerXAnchor.constraint(equalTo: footerTableView.centerXAnchor),
            unlockAll.topAnchor.constraint(equalTo: iconImage.bottomAnchor),
            unlockAll.heightAnchor.constraint(equalToConstant: 30),
            unlockAll.centerXAnchor.constraint(equalTo: footerTableView.centerXAnchor),
            addPurchase.topAnchor.constraint(equalTo: unlockAll.bottomAnchor, constant: 30),
            addPurchase.centerXAnchor.constraint(equalTo: footerTableView.centerXAnchor),
            addPurchase.heightAnchor.constraint(equalToConstant: 56),
            addPurchase.widthAnchor.constraint(equalToConstant: 280)
        ])
    }
    
    private func loadDarkTheme(){
        let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
        view.backgroundColor = userCurretChoiceIs ? black : white
        navigationController?.navigationBar.barTintColor = userCurretChoiceIs ? darkGrey : white
        navigationController?.navigationBar.barStyle = userCurretChoiceIs ? .black : .default
        navigationController?.navigationBar.titleTextAttributes = userCurretChoiceIs ? [.foregroundColor : white] : [.foregroundColor : black]
        userProfileButton.tintColor = userCurretChoiceIs ? white : black
        tableView.backgroundColor = userCurretChoiceIs ? black : veryLightPinkTwo15
        unlockAll.textColor = userCurretChoiceIs ? white : black85
    }
}
