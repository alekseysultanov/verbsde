//
//  TranslateCell.swift
//  prototypeGV
//
//  Created by Vladimir Ereskin on 4/2/20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

protocol TranslateCellProtocol: AnyObject {
    func cellTextDidChange(text: String, index: Int)
}

final class TranslateCell: UITableViewCell, UITextFieldDelegate {
    
    var delegate: TranslateCellProtocol?
    
    lazy var translateTextField: UITextField = {
        let textField = UITextField()
        textField.textInsets = CGPoint(x: 0, y: 0)
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = black85
        textField.textAlignment = .left
        textField.borderStyle = .none
        textField.autocorrectionType = .yes
        textField.autocapitalizationType = .none
        textField.smartInsertDeleteType = .yes
        textField.spellCheckingType = .yes
        textField.font = UIFont(name: "TeXGyreAdventor-Bold", size: 20)
        textField.layer.masksToBounds = true
        textField.text = ""
        textField.delegate = self
        textField.becomeFirstResponder()
        textField.placeholder = addTranslate
        textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        return textField
    }()
    
    func loadDarkTheme() {
        let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
        translateTextField.attributedPlaceholder = NSAttributedString(string:"add translate", attributes: [NSAttributedString.Key.foregroundColor: userCurretChoiceIs ? veryLightPink : black85])
        translateTextField.textColor = userCurretChoiceIs ? white : black85
    }
    
    private var index = 0
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(with text: String, index: Int) {
        translateTextField.text = text
        self.index = index
    }
    
    @objc private func textFieldDidChange() {
        delegate?.cellTextDidChange(text: translateTextField.text ?? "", index: index)
    }
}

extension TranslateCell: UserPresentable {
    func addSubView() {
        contentView.addSubview(translateTextField)
    }
    
    func makeConstraints() {
        NSLayoutConstraint.activate([
            translateTextField.topAnchor.constraint(equalTo: contentView.topAnchor),
            translateTextField.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            translateTextField.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            translateTextField.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 27),
        ])
    }
}

extension UITextField {
    @IBInspectable var textInsets: CGPoint {
        get {
            return CGPoint.zero
        }
        set {
            layer.sublayerTransform = CATransform3DMakeTranslation(newValue.x, newValue.y, 0);
        }
    }
}
