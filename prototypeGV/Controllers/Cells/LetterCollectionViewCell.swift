//
//  LetterCollectionViewCell.swift
//  prototypeGV
//
//  Created by Vladimir Ereskin on 4/6/20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import UIKit

protocol LetterCollectionViewCellProtocol: AnyObject {
    func letterTap(text: String)
}

final class LetterCollectionViewCell: UICollectionViewCell {
    
    private struct Constants {
        static let badgeSize: CGFloat = 20
    }
    
    weak var delegate: LetterCollectionViewCellProtocol?
    
    private let letterLabel = UILabel()
    let letterView = UIView()
    let bageView = BageView()
    
    var answerKey: AnswerKey = AnswerKey(letter: "", count: 0)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        makeConstraints()
    }
    
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    func bind(with letter: AnswerKey) {
        letterView.alpha = 1
        answerKey = letter
        letterLabel.text = letter.letter
        showBage()
    }
    
    private func setupViews() {
        let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
        letterView.translatesAutoresizingMaskIntoConstraints = false
        letterLabel.translatesAutoresizingMaskIntoConstraints = false
        
        letterView.backgroundColor = userCurretChoiceIs ? veryLightPinkTwo : black10
        letterView.layer.cornerRadius = 12
        
        letterLabel.font = UIFont.systemFont(ofSize: 24)
        letterLabel.textAlignment = .center
        
        let tapLetterGesture = UITapGestureRecognizer(target: self, action: #selector(letterTap))
        letterView.addGestureRecognizer(tapLetterGesture)
        
        contentView.addSubview(letterView)
        letterView.addSubview(letterLabel)
        letterView.addSubview(bageView)
    }
    
    private func makeConstraints() {
        NSLayoutConstraint.activate([
            letterView.topAnchor.constraint(equalTo: contentView.topAnchor),
            letterView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            letterView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            letterView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            letterLabel.topAnchor.constraint(equalTo: letterView.topAnchor),
            letterLabel.leftAnchor.constraint(equalTo: letterView.leftAnchor),
            letterLabel.rightAnchor.constraint(equalTo: letterView.rightAnchor),
            letterLabel.bottomAnchor.constraint(equalTo: letterView.bottomAnchor),
            
            bageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant:  -Constants.badgeSize / 3),
            bageView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: Constants.badgeSize / 3),
            bageView.widthAnchor.constraint(equalToConstant: Constants.badgeSize),
            bageView.heightAnchor.constraint(equalToConstant: Constants.badgeSize),
        ])
    }
    
    func showBage() {
        if answerKey.count >= 2 {
            bageView.alpha = 1
            bageView.countLabel.text = "\(answerKey.count)"
        } else {
            bageView.alpha = 0
        }
    }
    
    @objc private func letterTap() {
        let userCurretChoiceIs = defaults.bool(forKey: "isDarkTheme")
        self.letterView.backgroundColor = azure
        delegate?.letterTap(text: letterLabel.text ?? "")
        answerKey.count -= 1
        showBage()
        
        if answerKey.count == 0 {
            UIView.animate(withDuration: Animation.duration04, animations: {
                self.letterView.transform = CGAffineTransform(scaleX: 0.00001, y: 0.000001)
                self.letterView.alpha = 0
                self.letterView.backgroundColor = userCurretChoiceIs ? white : black10
            }, completion: { _ in
                self.letterView.transform = CGAffineTransform(scaleX: 1, y: 1)
            })
        } else {
            UIView.animate(withDuration: Animation.duration04, animations: {
                self.letterView.transform = CGAffineTransform(scaleX: 0.00001, y: 0.000001)
                self.letterView.backgroundColor = azure
                self.bageView.countLabel.text = "\(self.answerKey.count)"
                self.letterView.backgroundColor = userCurretChoiceIs ? white : black10
            }, completion: { _ in
                self.letterView.transform = CGAffineTransform(scaleX: 1, y: 1)
            })
        }
    }
    
    func addLetterToView() {
        if letterView.alpha == 0 {
            self.alpha = 1
        }
    }
}
