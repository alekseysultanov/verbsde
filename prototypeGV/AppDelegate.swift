//
//  AppDelegate.swift
//  prototypGV
//
//  Created by Max Polinkovsky on 30.04.2019.
//  Copyright © 2019 Max Polinkovsky. All rights reserved.
//

import UIKit
import Firebase
import AuthenticationServices

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let navigationController = UINavigationController()
        let mainView = ThemeVC()
        navigationController.navigationBar.barTintColor = .white
        navigationController.viewControllers = [mainView]
        self.window!.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
        
        IAPManager.shared.startWith(
            arrayOfIds: [Products.oneMonth.rawValue,
                         Products.twelveMonths.rawValue,
                         Products.oneYear.rawValue])
        
        if let userIdentifier = defaults.object(forKey: "userIdentifier1") as? String {
            let authorizationProvider = ASAuthorizationAppleIDProvider()
            authorizationProvider.getCredentialState(forUserID: userIdentifier) { (state, error) in
                switch (state) {
                case .authorized:
                    print("Account Found - Signed In")
                    DispatchQueue.main.async {
                        self.window = UIWindow(frame: UIScreen.main.bounds)
                        let navigationController = UINavigationController()
                        let mainView = ThemeVC()
                        navigationController.navigationBar.barTintColor = .white
                        navigationController.viewControllers = [mainView]
                        self.window!.rootViewController = navigationController
                        self.window?.makeKeyAndVisible()
                    }
                    break
                case .revoked:
                    print("No Account Found")
                    fallthrough
                case .notFound:
                    print("No Account Found")
                    //                        DispatchQueue.main.async {
                    //                             self.LoginVC()
                //                        }
                default:
                    break
                }
            }
        }
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}
