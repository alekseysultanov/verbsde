//
//  ExtensionUITextField.swift
//  prototypeGV
//
//  Created by admin on 08.11.19.
//  Copyright © 2019 Max Polinkovsky. All rights reserved.
//

import UIKit

extension String {
    
    func delSpaces() -> String { // тут мы удалем пробелы вначале и конце текста
        
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).replacingOccurrences(of: ", ", with: ",")
    }
    
    func isEmpty() -> Bool { // проверяем пусто или нет
        return self.delSpaces() == "" ? true : false
    }
    
    func localized(withComment comment: String? = nil) -> String {
        return NSLocalizedString(self, comment: comment ?? "")
    }
}
