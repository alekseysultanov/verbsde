//
//  ExtensionsUIColor.swift
//  prototypeGV
//
//  Created by Max Polinkovsky on 26.05.2019.
//  Copyright © 2019 Max Polinkovsky. All rights reserved.
//
import UIKit

extension UIColor {
    
    func add(_ over: UIColor) -> UIColor {
        var b: (R: CGFloat, G: CGFloat, B: CGFloat, A: CGFloat) = (0, 0, 0, 0)
        var f: (R: CGFloat, G: CGFloat, B: CGFloat, A: CGFloat) = (0, 0, 0, 0)
        
        self.getRed(&b.R, green: &b.G, blue: &b.B, alpha: &b.A)
        over.getRed(&f.R, green: &f.G, blue: &f.B, alpha: &f.A)
        
        return UIColor(
            red:   f.A * f.R + (1 - f.A) * b.R,
            green: f.A * f.G + (1 - f.A) * b.G,
            blue:  f.A * f.B + (1 - f.A) * b.B,
            alpha: 1
        )
    }
    
    static func color(light: UIColor, dark: UIColor) -> UIColor {
        if #available(iOS 13, *) {
            return UIColor.init { traitCollection in
                return traitCollection.userInterfaceStyle == .dark ? dark : light
            }
        } else {
            return light
        }
    }
    
    static func hex(_ hexString: String) -> UIColor {
        
        var hexString: String = hexString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if hexString.hasPrefix("#") { hexString.remove(at: hexString.startIndex) }
        
        var rgbValue:UInt64 = 0
        Scanner(string: hexString).scanHexInt64(&rgbValue)
        
        return .init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    struct Pallete {

        static let white = UIColor.color(light: .white, dark: .black)
        static let black = UIColor.color(light: .black, dark: .white)

        static let background = UIColor.color(light: .white, dark: .hex("1b1b1d"))
        static let secondaryBackground = UIColor(named: "secondaryBackground") ?? .black

        static let gray = UIColor.color(light: .lightGray, dark: .hex("8e8e92"))
    }
}
