//
//  ExtensionsResults.swift
//  prototypeGV
//
//  Created by Vladimir Ereskin on 3/15/20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import RealmSwift

extension Results {
    func toArray() -> [Element] {
        return compactMap { $0 }
    }
}
