//
//  ExtensionArray.swift
//  prototypeGV
//
//  Created by admin on 24.07.20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import Foundation

extension Array {
    var last: Any {
        return self[self.endIndex - 1]
    }
}
