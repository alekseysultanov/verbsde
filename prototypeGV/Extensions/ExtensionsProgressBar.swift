//
//  ExtensionsProgressBar.swift
//  prototypeGV
//
//  Created by Max Polinkovsky on 06.10.19.
//  Copyright © 2019 Max Polinkovsky. All rights reserved.
//

import UIKit


extension UIProgressView {
    func setProgressBar(progress: UIProgressView) {
        progress.layer.cornerRadius = 3
        progress.clipsToBounds = true
        progress.layer.sublayers![1].cornerRadius = 3
        progress.subviews[1].clipsToBounds = true
    }
}
