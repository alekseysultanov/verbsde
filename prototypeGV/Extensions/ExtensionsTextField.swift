//
//  ExtensionsTextField.swift
//  prototypeGV
//
//  Created by admin on 11.11.19.
//  Copyright © 2019 Max Polinkovsky. All rights reserved.
//

import UIKit

extension UITextField {
    
    //    func setUpTF(textField: UITextField, color: UIColor, textColor: UIColor ,cornerRadius: CGFloat) {
    //    textField.layer.cornerRadius = cornerRadius
    //    textField.backgroundColor = color
    //    textField.textColor = textColor
    //    }
    
    func setLeftView(image: UIImage) {
        let iconView = UIImageView(frame: CGRect(x: 22, y: 10, width: 13, height: 14)) // set your Own size
        iconView.image = image
        let iconContainerView: UIView = UIView(frame: CGRect(x: 22, y: 0, width: 66, height: 33))
        iconContainerView.addSubview(iconView)
        leftView = iconContainerView
        leftViewMode = .always
        self.tintColor = black
    }
}

/// Класс TextField для ввода Логина/Пароля
final class InputTextField: UITextField {
    init(text: String, image: UIImage?, isSecureText: Bool) {
        super.init(frame: .zero)
        
        if let image = image {
            setLeftView(image: image)
        }
        translatesAutoresizingMaskIntoConstraints = false
        isSecureTextEntry = isSecureText
        textColor = black
        placeholder = text
        background = UIImage(contentsOfFile: "")
        backgroundColor = black10
        textAlignment = .left
        borderStyle = .none
        returnKeyType = .continue
        autocorrectionType = .no
        autocapitalizationType = .none
        smartInsertDeleteType = .no
        spellCheckingType = .no
        font = UIFont(name: "TeXGyreAdventor-Bold", size: 18)
    }
    
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}
