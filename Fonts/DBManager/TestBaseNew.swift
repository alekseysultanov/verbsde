//
//  TestBaseNew.swift
//  prototypeGV
//
//  Created by Max Polinkovsky on 08.09.19.
//  Copyright © 2019 Max Polinkovsky. All rights reserved.
//
import UIKit

class TestBase {
    
    static var themesToBase = [Theme]()
    static let levelName = ["a1", "a2", "b1", "b2", "c1"]
    static let themes = ["future", "family", "nature", "reise", "haus", "work"]
    static let themeImages = ["1", "2", "3", "4", "5", "6", "7"]
    static let verbs = [Verb(infinitiv: "machen", translate: "делать", progress:                         0.0,  isTested: false),
                        Verb(infinitiv: "sehen", translate: "видеть", progress: 0.0, isTested: false),
                        Verb(infinitiv: "arbeiten", translate: "работать", progress: 0.0, isTested:false),
                        Verb(infinitiv: "schissen", translate: "стрелять", progress: 0.0, isTested:false),
                        Verb(infinitiv: "schliesen", translate: "закрывать", progress: 0.0, isTested:false),
                        Verb(infinitiv: "warten", translate: "ждать",
                             progress: 0.0, isTested:false),
                        Verb(infinitiv: "weiterleiten", translate: "передавать", progress: 0.0,  isTested:false),
                        Verb(infinitiv: "leiten", translate: "руководить", progress: 0.9,isTested:false),
                        Verb(infinitiv: "geben", translate: "давать",progress: 0.0, isTested:false),
                        Verb(infinitiv: "bekommen", translate: "получать", progress: 1, isTested:false),
                        Verb(infinitiv: "argern", translate: "злиться",
                             progress: 0.0, isTested:false),
                        Verb(infinitiv: "denken", translate: "думать", progress: 1.0, isTested:false)
//                        Verb(infinitiv: "laufen", translate: "бежать", progress: 0.0, level: levelName[1], isTested:false),
//                        Verb(infinitiv: "schaukeln", translate: "качаться", progress: 0.0, level: levelName[2], isTested:false),
//                        Verb(infinitiv: "merken", translate: "отмечать",
//                             progress: 0.5, level: levelName[3], isTested:false),
//                        Verb(infinitiv: "nehmen", translate: "брать",progress: 0.0, level: levelName[4], isTested:false),
//                        Verb(infinitiv: "lesen", translate: "читать",progress: 0.0, level: levelName[0], isTested:false),
//                        Verb(infinitiv: "hoeren", translate: "слушать", progress: 0.0, level: levelName[1], isTested:false),
//                        Verb(infinitiv: "fahren", translate: "ехать",progress: 0.0, level: levelName[2], isTested:false),
//                       Verb(infinitiv:
//                           "ankommen", translate: "приезжать", progress: 0.0, level: levelName[3], isTested:false)
    ]
    
    static func addTestBase() {
    
        for name in themes {
            let theme = Theme(name: name)
            FileManager.add(theme)
            themesToBase.append(theme)
        }
 
        for (i, verb) in verbs.enumerated() {
            FileManager.add(verb, toTheme: themesToBase[0])
            if (i + 1) % 2 == 0 { FileManager.add(verb, toTheme: themesToBase[1]) }
            if (i + 1) % 5 == 0 { FileManager.add(verb, toTheme: themesToBase[2]) }
            if (i + 1) % 4 == 0 { FileManager.add(verb, toTheme: themesToBase[3]) }
            if (i + 1) % 3 == 0 { FileManager.add(verb, toTheme: themesToBase[4]) }
            if (i + 1) % 6 == 0 { FileManager.add(verb, toTheme: themesToBase[5]) }
        }
    }
}
