//
//  DownloadMyDB.swift
//  prototypeGV
//
//  Created by  Artem Valeryevich on 21.01.2020.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import Foundation
import FirebaseDatabase
import RealmSwift

class DownloadMyDB: UIViewController {
    var database: StructFirebase!
    var ref: DatabaseReference!
    var completion: (() -> Void)?
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        let userUid = defaults.object(forKey: "userUid") as? String
        ref = Database.database().reference().child("users").child(userUid!)
    }
    
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    func download(completion: @escaping (() -> Void)) {
        self.completion = completion
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            let task = StructFirebase(snapshot: snapshot)
            self.database = task
            if let task = task.databaseXlsx, let urlTask = URL(string: task) {
                let urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue())
                let downloadTask = urlSession.downloadTask(with: urlTask)
                downloadTask.resume()
            }
        })
    }
}

extension DownloadMyDB:  URLSessionDownloadDelegate {
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        // create destination URL with the original pdf name
        guard let url = downloadTask.originalRequest?.url else { return }
        let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let destinationURL = documentsPath.appendingPathComponent(url.lastPathComponent)
        // delete original copy
        try? FileManager.default.removeItem(at: destinationURL)
        // copy from temp to Document
        do {
            try FileManager.default.copyItem(at: location, to: destinationURL)
        } catch let error {
            print("Copy Error: \(error.localizedDescription)")
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        if Float(totalBytesWritten) == Float(totalBytesExpectedToWrite) {
            completion?()
        }
    }
}
