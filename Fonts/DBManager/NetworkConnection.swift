//
//  NetworkConnection.swift
//  prototypeGV
//
//  Created by Vladimir Ereskin on 3/30/20.
//  Copyright © 2020 Max Polinkovsky. All rights reserved.
//

import Network
import Foundation

// Проверка интернет соединения
final class NetworkConnection {
    static func checkInternet(completion: @escaping (Bool) -> Void) {
        DispatchQueue.main.async {
            let url = URL(string: "https://www.google.com")!
            var request = URLRequest(url: url)
            request.timeoutInterval = 600
            
            let task = URLSession.shared.dataTask(with: request) {data, response, error in
                DispatchQueue.main.async {
                    if error != nil {
                        completion(false)
                    }
                    else if let httpResponse = response as? HTTPURLResponse {
                        if httpResponse.statusCode == 200 {
                            completion(true)
                        }
                    }
                }
            }
            task.resume()
        }
    }
}
