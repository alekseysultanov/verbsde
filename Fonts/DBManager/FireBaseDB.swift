//
//  FireBaseDB.swift
//  prototypeGV
//
//  Created by admin on 26.12.19.
//  Copyright © 2019 Max Polinkovsky. All rights reserved.
//

import Foundation
import Firebase

struct FirebaseDB {
    let version: String?
    let url: String?
    let ref: DatabaseReference?
    
    init(_version: String, _url : String) {
        self.version = _version
        self.url = _url
        self.ref = nil
    }
    
    init(snapshot: DataSnapshot) {
        let snapshotValue = snapshot.value as! [String: AnyObject]
        version = snapshotValue["version"] as? String
        url = snapshotValue["url"] as? String
        ref = snapshot.ref
    }
}
