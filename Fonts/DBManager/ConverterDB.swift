//
//  ConverterDB.swift
//  prototypeGV
//
//  Created by admin on 21.12.19.
//  Copyright © 2019 Max Polinkovsky. All rights reserved.
//

import UIKit
import CoreXLSX
import FirebaseStorage
import RealmSwift

class ConverterDB {
    
    static var oldTheme: Results<Theme>?
    static var themes: Results<Theme>!
    
//    static let names = [TypeTheme.modal.description, TypeTheme.strongHaben.description, TypeTheme.strongSein.description]
    
    static var themesw = [Theme]()
    
    /// Функция для чтения XLSX файла
    ///  Записывает все в локальную базу Realm
    /// - Parameters:
    ///   - files: ссылка на файл в папке документы
    ///   - completion: сробатывает по завершению
    static func conversion(files: String!, completion: @escaping (() -> Void)) {
        
        let newString: String? = files.replacingOccurrences(of: "file://", with: "")
        guard let path = newString,
            let file = XLSXFile(filepath: path) else { return }
        do {
            for path in try file.parseWorksheetPaths() {
                let ws = try file.parseWorksheet(at: path)
                let sharedStrings = try file.parseSharedStrings()
                var infinitiv = ws.cells(atColumns: [ColumnReference("A")!])
                    .filter { $0.type!.rawValue == "s" }
                    .compactMap { $0.value }
                    .compactMap { Int($0) }
                    .compactMap { sharedStrings.items[$0].text }
                let ru = ws.cells(atColumns: [ColumnReference("B")!])
                    .filter { $0.type!.rawValue == "s" }
                    .compactMap { $0.value }
                    .compactMap { Int($0) }
                    .compactMap { sharedStrings.items[$0].text }
                let en = ws.cells(atColumns: [ColumnReference("C")!])
                .filter { $0.type!.rawValue == "s" }
                .compactMap { $0.value }
                .compactMap { Int($0) }
                .compactMap { sharedStrings.items[$0].text }
                let prateritum = ws.cells(atColumns: [ColumnReference("D")!])
                    .filter { $0.type!.rawValue == "s" }
                    .compactMap { $0.value }
                    .compactMap { Int($0) }
                    .compactMap { sharedStrings.items[$0].text }
                let perfect = ws.cells(atColumns: [ColumnReference("E")!])
                    .filter { $0.type!.rawValue == "s" }
                    .compactMap { $0.value }
                    .compactMap { Int($0) }
                    .compactMap { sharedStrings.items[$0].text }
                let hilfsVerb = ws.cells(atColumns: [ColumnReference("F")!])
                    .filter { $0.type!.rawValue == "s" }
                    .compactMap { $0.value }
                    .compactMap { Int($0) }
                    .compactMap { sharedStrings.items[$0].text }
                let casus = ws.cells(atColumns: [ColumnReference("G")!])
                    .filter { $0.type!.rawValue == "s" }
                    .compactMap { $0.value }
                    .compactMap { Int($0) }
                    .compactMap { sharedStrings.items[$0].text?.replacingOccurrences(of: "-", with: "") }
                let preposition = ws.cells(atColumns: [ColumnReference("H")!])
                    .filter { $0.type!.rawValue == "s" }
                    .compactMap { $0.value }
                    .compactMap { Int($0) }
                    .compactMap { sharedStrings.items[$0].text?.replacingOccurrences(of: "-", with: "") }
                let themeVerb = ws.cells(atColumns: [ColumnReference("I")!])
                    .filter { $0.type!.rawValue == "s" }
                    .compactMap { $0.value }
                    .compactMap { Int($0) }
                    .compactMap { sharedStrings.items[$0].text }
                
                infinitiv = infinitiv.compactMap { $0.delSpaces().lowercased() }
                
                DataManager.getUserInfo(completion: { userInfo in
                    guard let userInfo = userInfo else { return }
                    
                    userInfo.verbFix.forEach { verb in
                        if infinitiv.contains(verb.infinitiv) == false {
                            if let index = userInfo.verbFix.index(of: verb) {
                                try! realm.write {
                                    userInfo.verbFix.remove(at: index)
                                }
                            }
                        }
                    }
                    
                    for i in 1..<themeVerb.count {
                        
                        if let verbIsContain = userInfo.verbFix.filter(NSPredicate(format: "infinitiv = %@", infinitiv[i])).first {
                            try! realm.write ({
                                
                                // +++ Здест добавляем локаль (RU)
                                
                                verbIsContain.translate.removeAll()
                                for translate in ru[i].delSpaces().components(separatedBy: ",") {
                                    verbIsContain.appendTranslateTo(locale: "ru", value: translate.delSpaces().lowercased())
                                }
                                
                                // --- Конец
                                
                                // +++ Здест добавляем локаль (EN)
                                
                                verbIsContain.translate.removeAll()
                                for translate in en[i].delSpaces().components(separatedBy: ",") {
                                    verbIsContain.appendTranslateTo(locale: "en", value: translate.delSpaces().lowercased())
                                }
                                
                                // --- Конец
                                
                                verbIsContain.perfect = perfect[i].delSpaces().lowercased()
                                verbIsContain.prateritum = prateritum[i].delSpaces().lowercased()
                                verbIsContain.casus = casus[i].delSpaces().lowercased()
                                verbIsContain.preposition = preposition[i].delSpaces().lowercased()
                                verbIsContain.hilfsVerb = hilfsVerb[i].delSpaces().lowercased()
                            })
                            verbIsContain.saveToFirebaseStep(fixed: "verbFix")
                        } else {
                            var translatesArray: [Translates] = [] // в этот массив добавляем новую локаль
                            
                            translatesArray.append(Translates(locale: "ru", translates: ru[i].delSpaces().components(separatedBy: ",")))
                            translatesArray.append(Translates(locale: "en", translates: en[i].delSpaces().components(separatedBy: ",")))
                            
                            let verb = Verb(infinitiv: infinitiv[i].delSpaces(),
                                            translate: translatesArray,
                                            prateritum: prateritum[i].delSpaces(),
                                            hilfsVerb: hilfsVerb[i].delSpaces(),
                                            perfect: perfect[i].delSpaces(),
                                            casus: casus[i].delSpaces(),
                                            preposition: preposition[i].delSpaces(),
                                            theme: themeVerb[i].delSpaces())
                            try! realm.write {
                                userInfo.verbFix.append(verb)
                            }
                            verb.saveToFirebaseStep(fixed: "verbFix")
                        }
                        DataManager.addTheme(name: themeVerb[i], fixed: true)
                    }
                    completion()
                })
            }
        } catch {
        }
    }
}
