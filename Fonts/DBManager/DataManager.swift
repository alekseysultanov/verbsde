//
//  DataManager.swift
//  prototypGV
//
//  Created by Max Polinkovsky on 30.04.2019.
//  Copyright © 2019 Max Polinkovsky. All rights reserved.
//

import RealmSwift
import FirebaseDatabase

var realm = try! Realm()

class DataManager {
    
    static let refFirebase = Database.database().reference()
    
    /// Получение текущего пользователя
    static func getUserInfo(completion: (UserInfo?) -> Void) {
        guard let userInfo = realm.objects(UserInfo.self).toArray().first else {
            completion(nil)
            return
        }
        completion(userInfo)
    }
    
    /// Добавить тему в Базы данных если она является новой
    static func addTheme(name: String, fixed: Bool) {
        DataManager.getUserInfo { userInfo in
            guard let themes = userInfo?.themes else { return }
            let temesArray = Array(themes)
            
            if temesArray.first(where: { $0.name == name }) == nil {
                let theme = Theme(name: name, fixed: fixed, date: "", oldCount: 0, learnCount: 0)
                try! realm.write {
                    userInfo?.themes.append(theme)
                }
                theme.saveToFirebase(to: name)
            }
        }
    }
    
    /// Добавления нового пользовательского слова
    /// - Parameters:
    ///   - verb: слово
    ///   - toTheme: тема
    static func add(_ verb: Verb) {
        try! realm.write {
            DataManager.getUserInfo { userInfo in
                guard let userInfo = userInfo else { return }
                userInfo.verbUser.append(verb)
            }
        }
    }
    
    /// Удалить тему из Баз по индексу
    static func deleteTheme(_ index: Int) {
        DataManager.getUserInfo { user in
            let theme = user?.themes[index]
            
            theme?.removeFromFirebase()
            
            try! realm.write {
                user?.themes.remove(at: index)
            }
        }
    }
    
    static func delete(_ verb: Verb, fixed: String) {
        verb.removeFromFirebaseStep(fixed: fixed)
        
        try! realm.write {
            realm.delete(verb)
        }
    }
    
    static func userInfoSave(_ userInfo: UserInfo) {
        try! realm.write {
            realm.delete(realm.objects(UserInfo.self))
        }
        try! realm.write {
            realm.add(userInfo)
        }
        
        userInfoSaveToFirebaseFor(user: userInfo)
    }
    
    static func saveUserImageFirebase(user: UserInfo) {
        guard let userUid = defaults.object(forKey: "userUid") as? String else { return }
        DataManager.refFirebase.child("users").child("\(userUid)").child("userInfo").updateChildValues([
            "imageData": self.convertImageToBase64(data: user.imageData)
        ])
    }
    
    static func userInfoSaveToFirebaseStep() {
        DataManager.getUserInfo { userInfo in
            guard let userInfo = userInfo else { return }
            guard let userUid = defaults.object(forKey: "userUid") as? String else { return }
            
            
            DataManager.refFirebase.child("users").child("\(userUid)").child("userInfo").updateChildValues([
                "email": userInfo.email,
                "name": userInfo.name,
                "verbFixVersion": userInfo.verbFixVersion,
                "imageData": self.convertImageToBase64(data: userInfo.imageData)
            ])
        }
    }
    
    static func userInfoSaveVersionBD(user: UserInfo) {
        guard let userUid = defaults.object(forKey: "userUid") as? String else { return }
        
        DataManager.refFirebase.child("users").child("\(userUid)").child("userInfo").updateChildValues([
            "verbFixVersion": user.verbFixVersion
        ])
    }
    
    static func userInfoSaveToFirebaseFor(user: UserInfo) {
        guard let userUid = defaults.object(forKey: "userUid") as? String else { return }
        
        var themes: [String: [AnyHashable: Any]] = [:]
        user.themes.forEach { theme in
            themes["\(theme.name)"] = [
                "name": theme.name,
                "fixed": theme.fixed,
                "date": theme.date,
                "oldCount": theme.oldCount,
                "learnCount": theme.learnCount,
            ]
        }
        
        
        DataManager.refFirebase.child("users").child("\(userUid)").child("userInfo").updateChildValues([
            "email": user.email,
            "name": user.name,
            "verbFixVersion": user.verbFixVersion,
            "imageData": self.convertImageToBase64(data: user.imageData),
            "themes": themes
        ])
    }
    
    /// Очистить локальную базу данных
    static func clearDB() {
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    /// Очистить слова из фиксированных тем
    static func clearVerbFix() {
        DataManager.getUserInfo { userInfo in
            guard let userInfo = userInfo else { return }
            try! realm.write {
                userInfo.verbFix.removeAll()
            }
        }
    }
    
    /// Очистить слова из пользовательских тем
    static func clearVerbUser() {
        DataManager.getUserInfo { userInfo in
            guard let userInfo = userInfo else { return }
            try! realm.write {
                userInfo.verbUser.removeAll()
            }
        }
    }
    
    /// Очистить фиксированные темы
    static func clearThemeFix() {
        try! realm.write {
            DataManager.getUserInfo { userInfo in
                guard let userInfo = userInfo else { return }
                let themes = userInfo.themes.filter(NSPredicate(format: "fixed = %d", true))
                themes.forEach { theme in
                    if let index = userInfo.themes.index(of: theme) {
                        userInfo.themes.remove(at: index)
                    }
                }
            }
        }
    }
    
    /// Очистить пользовательские темы
    static func clearThemeUser() {
        try! realm.write {
            DataManager.getUserInfo { userInfo in
                guard let userInfo = userInfo else { return }
                let themes = userInfo.themes.filter(NSPredicate(format: "fixed = %d", false))
                themes.forEach { theme in
                    if let index = userInfo.themes.index(of: theme) {
                        userInfo.themes.remove(at: index)
                    }
                }
            }
        }
    }
    
    /// Получить базу данных из Firebase
    static func getFirebaseDB(completion: @escaping ((Bool) -> Void)) {
        guard let userUid = defaults.object(forKey: "userUid") as? String else {
            completion(false)
            return
        }
        let ref = Database.database().reference().child("users").child(userUid).child("userInfo")
        
        ref.observeSingleEvent(of: .value, with: { snapshot in
            guard ((snapshot.value as? [String: AnyObject]) != nil) else {
                completion(false)
                return
            }
            let userInfoFB = UserInfoFirebase(snapshot: snapshot)
            DataManager.userInfoSave(userInfoFB.convertToUserInfoRealm())
            completion(true)
        })
    }
    
    /// проверить локальную версию файла с версией в Firebase
    static func checkVersionFirebase(completion: @escaping ((String?, Double?) -> Void)) {
        let ref = Database.database().reference().child("DataBase")
        
        ref.observe(.value) { (snapeshot) in
            
            let firebaseDB = FirebaseDB(snapshot: snapeshot)
            
            guard let version = Double(firebaseDB.version ?? "-1") else {
                completion(firebaseDB.url, 0)
                return
            }
            
            DataManager.getUserInfo { userInfo in
                guard let userInfo = userInfo else {
                    completion(nil, nil)
                    return
                }
                guard userInfo.verbFixVersion != version else {
                    completion(nil, nil)
                    return
                }
                
                completion(firebaseDB.url, version)
            }
        }
    }
    
    // Сохранить версию файла в Firebase
    static func saveVerbFixVersion(_ version: Double, userInfo: UserInfo) {
        DataManager.getUserInfo { userInfoN in
            guard let userInfoN = userInfoN else { return }
            
            try! realm.write {
                userInfoN.verbFixVersion = version
            }
            DataManager.userInfoSaveVersionBD(user: userInfoN)
        }
    }
    
    /// Сохранить иконку пользователя в Firebase
    static func saveUserIcon(_ icon: Data, userInfo: UserInfo) {
        try! realm.write {
            userInfo.imageData = icon
        }
        DataManager.saveUserImageFirebase(user: userInfo)
    }
    
    /// Конвертировать картинку в данные для выгрузки
    static func convertImageToBase64(data: Data?) -> String {
        guard let data = data else { return "" }
        return data.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    /// Конвертировать загруженные данные в картинку
    static func convertBase64ToImage(imageString: String) -> Data {
        let imageData = Data(base64Encoded: imageString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
        return imageData
    }
    
    static func saveLernThemeToFirebase(date: String, oldCount: Int, learnCount: Int, theme: String) {
        guard let userUid = defaults.object(forKey: "userUid") as? String else { return }
        
        DataManager.refFirebase.child("users").child("\(userUid)").child("userInfo").child("themes").child(theme).updateChildValues([
            "oldCount": oldCount,
            "learnCount": learnCount,
            "date": date
        ])
    }
}
